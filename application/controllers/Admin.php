<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('Admin_model');
		$this->load->model('Auth_model');
		$this->Auth_model->auth($this->session->userdata());

	}

	public function index()
	{
		$this->data['val'] = 'pankaj';
		$bc = array(array('link' => base_url(), 'page' => 'Admin'), array('link' => '#', 'page' => 'Dashboard'));
        $meta = array('page_title' => 'Dashboard', 'bc' => $bc);
        $this->page_construct('admin/index', $meta, $this->data);
	}

	/*Static Page*/

	public function static_page(){
		$this->data['static_page'] = $this->Admin_model->getStaticPageAll();
		$bc = array(array('link' => base_url(), 'page' => 'Home'), array('link' => '#', 'page' => 'Static Page'));
        $meta = array('page_title' => 'Static Page', 'bc' => $bc);
        $this->page_construct('admin/static_page/index', $meta, $this->data);
	}

	public function add_static_page(){
		$this->form_validation->set_rules('name', lang('name'), 'required');

        if ($this->form_validation->run() == true) {
        	$data = array( 
			'name' => $this->input->post('name'),
			'label' => $this->input->post('label'),
			'content' => $this->input->post('content'),
			'url_text' => $this->input->post('url'),
			'link' => $this->input->post('links') 
			);
        	if($this->Admin_model->validateName($data)){
        		if($this->Admin_model->validateLabel($data)){
	        		if($this->Admin_model->addStaticPage($data)){
						$this->session->set_flashdata('message', 'You have successfully added the static page');
					}	
        		}else{
        			$this->session->set_flashdata('error', 'The static page label you have entered already exists');
				}	
        	}else{
        			$this->session->set_flashdata('error', 'The static page name you have entered already exists');
				}
		}
		$this->data['val'] = 'pankaj';
		$bc = array(array('link' => base_url(), 'page' => 'Add Static Page'), array('link' => '#', 'page' => 'Add Static Page'));
        $meta = array('page_title' => 'Add Static Page', 'bc' => $bc);
        $this->page_construct('admin/static_page/add', $meta, $this->data);
	}

	public function edit_static_page($id = NULL){
		if($this->input->post()){
			$data = array( 
			'name' => $this->input->post('name'),
			'label' => $this->input->post('label'),
			'content' => $this->input->post('content'),
			'url_text' => $this->input->post('url'),
			'link' => $this->input->post('links') 
			);
			if($this->Admin_model->addUpdatePage($data, $id)){
				$this->session->set_flashdata('message', 'You have successfully updated the static page');
				redirect('Admin/static_page','refresh');
			}
		}
		$this->data['static_page'] = $this->Admin_model->getStaticPageById($id);
		$bc = array(array('link' => base_url(), 'page' => 'Edit Static Page'), array('link' => '#', 'page' => 'Edit Static Page'));
        $meta = array('page_title' => 'Edit Static Page', 'bc' => $bc);
        $this->page_construct('admin/static_page/edit', $meta, $this->data);
	}

	public function view_static_page($id, $name){
		$id = $this->uri->segment(3); 
		$name = $this->uri->segment(4);
		$static_page = $this->Admin_model->getStaticPageById($id);
		$this->data['static_page'] = $static_page;
		$bc = array(array('link' => base_url(), 'page' => 'Static Page'), array('link' => '#', 'page' => $name));
        $meta = array('page_title' => $name, 'bc' => $bc);
        $this->page_construct('admin/static_page/view', $meta, $this->data);
	}

	public function delete_static_page($id = NULL){
		if($this->input->get('id')){
			$id = $this->input->get('id');
		}
		if($this->Admin_model->deleteStaticPage($id)){
			$this->session->set_flashdata('message', 'You have successfully deleted the static page');
			redirect(base_url('Admin/static_page'), 'refresh');
		}
	}

	/*Defaults Page*/
	

	public function defaults_page(){
		$this->load->view('admin/comming_soon');
	}

	public function add_defaults_page(){
		$this->load->view('admin/comming_soon');
	}

	public function edit_defaults_page($id = NULL){
		$this->load->view('admin/comming_soon');
	}

	public function delete_defaults_page($id = NULL){
		$this->load->view('admin/comming_soon');
	}

	/*package Page*/
	

	public function package_page(){
   //print_r($this->Admin_model->get_package_value());exit();
		$this->data['result']=$this->Admin_model->get_package_value();
		$this->data['val'] = 'pankaj';
		$bc = array(array('link' => base_url(), 'page' => 'Home'), array('link' => '#', 'page' => 'package_data'));
        $meta = array('page_title' => 'package_data', 'bc' => $bc);
        $this->page_construct('admin/package/index',$meta, $this->data);

		
	}

    public function add_package_page(){
        $this->data['currency']=$this->Admin_model->getCurrency();
		$this->data['val'] = 'pankaj';
		$bc = array(array('link' => base_url(), 'page' => 'Home'), array('link' => '#', 'page' => 'Add Packages'));
        $meta = array('page_title' => 'Add_package', 'bc' => $bc);
        $this->page_construct('admin/package/add', $meta, $this->data);
	}

    public function add_package_page_action(){
       
     $data = array(
            'package_id'       => $this->input->post('package_id'),
            'package_name'       => $this->input->post('package_name'),
			'package_details'    => $this->input->post('package_details'),
			'monthly_fee'      => $this->input->post('monthly_fee'),
			'annualy_fee'       => $this->input->post('annualy_fee'),
			'monthly_currency_name'      => $this->input->post('monthly_currency_name'),
			'annual_currency_name' => $this->input->post('annual_currency_name'),
            'status'=>$this->input->post('status_active').''.$this->input->post('status_inactive')
           );
       
		
		$data['date_created'] =date('Y-m-d H:i:s',now());
		if ($this->db->insert('fb_package_content', $data)) {
			$this->session->set_flashdata('message', 'You have successfully added package');
			 redirect('Admin/package_page'); exit();
		} else {

         echo 'not inserted';
			}
    	}

	public function edit_package_page($id = NULL){
        $this->data['currency']=$this->Admin_model->getCurrency();
		$this->data['info'] = $this->Admin_model->getPackageInfoById($id);
		$this->data['val'] = 'pankaj';
		$bc = array(array('link' => base_url(), 'page' => 'Home'), array('link' => '#', 'page' => 'Edit Packages'));
        $meta = array('page_title' => 'Edit_package', 'bc' => $bc);
        $this->page_construct('admin/package/edit', $meta, $this->data);
		

	}
	public function edit_package_action(){

		$data = array(

			'package_name'       => $this->input->post('package_name'),
			'package_details'    => $this->input->post('package_details'),
			'monthly_fee'      => $this->input->post('monthly_fee'),
			'annualy_fee'       => $this->input->post('annualy_fee'),
			'monthly_currency_name'      => $this->input->post('monthly_currency_name'),
			'annual_currency_name' => $this->input->post('annual_currency_name'),
			'status' => $this->input->post('status')
			
        );
        $data['date_created'] =date('Y-m-d H:i:s',now());
        $return_value = $this->Admin_model->Updatepackage($data, $this->input->post('data_id'));
		if ($return_value == 1) {
			$this->session->set_flashdata('message', 'You have successfully updated the Package Details');
			redirect('admin/package_page');
		} else {
			echo 'not updated';
		}
	}

	public function delete_package_page($id = NULL){
		if (isset($id) && !empty($id)) {
			if ($this->db->delete('fb_package_content', array('id' => $id))) {
				$this->session->set_flashdata('message', 'You have successfully deleted package');
			    redirect(base_url('admin/package_page'), 'refresh');
			}
		}
		
	}

}

/* End of file Admin.php */
/* Location: ./application/controllers/Admin.php */

