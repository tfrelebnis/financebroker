<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Finance_broker extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Broker_model');
		$this->load->helper(array('form', 'url'));
		$this->load->model('Auth_model');
		//$this->Auth_model->auth($this->session->userdata());
	}

	public function index()
	{
		$test = 'Pankaj';
		$this->data['val'] = 'pankaj';
		$bc = array(array('link' => base_url(), 'page' => 'Finance_Broker'), array('link' => '#', 'page' => 'Finance_Broker'));
        $meta = array('page_title' => 'Finance_Broker', 'bc' => $bc);
        $this->page_construct('finance_broker/index', $meta, $this->data);
	}

	public function add(){
		
	}

	public function view(){
		
	}

	public function edit(){
		
	}

	public function delete(){
		
	} 

	public function profile($id = ''){
		if(isset($id) && !empty($id)){

			$this->data['profile'] = $this->Broker_model->getFinanceProfileById($id);
			if(isset($this->data) && !empty($this->data)){
				$bc = array(array('link' => base_url(), 'page' => 'Admin'), array('link' => '#', 'page' => 'Admin'));
				$meta = array('page_title' => 'Admin', 'bc' => $bc);
				$this->page_profile('front/customer/profile', $meta, $this->data);
			}


		}else{
			$this->session->set_flashdata('warning', 'no coustomer profile found');
			
			redirect('Home','refresh');
		}
	}

}

/* End of file Admin.php */
/* Location: ./application/controllers/Admin.php */