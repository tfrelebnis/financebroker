<?php if (!defined('BASEPATH')) {exit('No direct script access allowed');
}

class Paypal extends CI_Controller {
	function __construct() {
		parent::__construct();
		
	}

	function success($user_id) {

		//get the transaction data
		$paypalInfo = $this->input->post();
		if($this->db->insert('payment', $paypalInfo)){
			if($this->db->update('user', array('payment_status' => '1'), array('id' => $user_id))){
				redirect(base_url('Home/broker_profile/'.$user_id),'refresh');
			}
		}
		/*$this->session->set_flashdata('successMsg', 'Payment Added');
		redirect(base_url('welcome'), 'refresh');
		*/
		/*$this->db->insert('papal_detail', $this->input->post());
		$data['item_number']   = $paypalInfo['item_number'];
		$data['txn_id']        = $paypalInfo["txn_id"];
		$data['payment_amt']   = $paypalInfo["amt"];
		$data['currency_code'] = $paypalInfo["cc"];
		$data['status']        = $paypalInfo["st"];
		$data['email']        = $paypalInfo["payer_email"];
		$this->session->set_flashdata('successMsg', 'Payment Added');*/
		//pass the transaction data to view
		/*$this->load->view('includes/header');
		$this->load->view('order_confirmation_page', $data);
		$this->load->view('includes/footer');*/
		//$this->load->view('paypal/success', $data);
	}

	function cancel($user_id) {
		if($this->db->update('user', array('payment_status' => '2'), array('id' => $user_id))){
			redirect(base_url('Home/broker_profile/'.$user_id),'refresh');
		}
	}

	function ipn() {
		//paypal return transaction details array
		$paypalInfo = $this->input->post();
		/*$this->session->set_flashdata('successMsg', 'Payment Added');
		redirect(base_url('welcome'), 'refresh');*/

		$data['user_id']        = $paypalInfo['custom'];
		$data['product_id']     = $paypalInfo["item_number"];
		$data['txn_id']         = $paypalInfo["txn_id"];
		$data['payment_gross']  = $paypalInfo["mc_gross"];
		$data['currency_code']  = $paypalInfo["mc_currency"];
		$data['payer_email']    = $paypalInfo["payer_email"];
		$data['payment_status'] = $paypalInfo["payment_status"];

		$paypalURL = $this->paypal_lib->paypal_url;
		$result    = $this->paypal_lib->curlPost($paypalURL, $paypalInfo);

		//check whether the payment is verified
		if (preg_match("/VERIFIED/i", $result)) {
			//insert the transaction data into the database
				print($data); exit;
			$this->db->insert('payment', $data);
			
		}
	}
}