<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Customer extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Customer_model');
	}

	public function index()
	{
		$test = 'Pankaj';
		$this->data['val'] = 'pankaj';
		$bc = array(array('link' => base_url(), 'page' => 'Customer'), array('link' => '#', 'page' => 'Customer'));
        $meta = array('page_title' => 'Customer', 'bc' => $bc);
        $this->page_construct('customer/index', $meta, $this->data);
	}

	public function add(){
		if($this->input->post('g-recaptcha-response')){
			$secret = '6LcdkyUUAAAAAIFug0zT9aQ1Y09TC4IYH9Wnd4iZ';
			$verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
        	$responseData = json_decode($verifyResponse);
        	if($responseData->success){
        		$this->session->set_flashdata('warning', 'Click all fields agains');
			}
			$data = array(
				'type' => '3',
				'last_name' => $this->input->post('clast_name'),
				'username' => $this->input->post('cemail_address'),
				'first_name' => $this->input->post('cfirst_name'),
				'state' => $this->input->post('cstate'),
				'suburb' => $this->input->post('csuburb'),
				'household_income' => $this->input->post('chouse_hold_income'),
				'age' => $this->input->post('cage'),
				'address' => $this->input->post('cemail_address'),
				'password' => $this->input->post('cpassword'),
				'status' => '1'
				);
			
			if(isset($data) && !empty($data)){
				if($this->Customer_model->getCustomerByEmail($data['address'])){
					$this->session->set_flashdata('error', 'This email address is already register');
					redirect(base_url('Home'),'refresh');
				}
				if($data['password'] != $this->input->post('crpassword')){
					$this->session->set_flashdata('error', 'Password and password retype could not matched');
					redirect(base_url('Home'),'refresh');
				}
				if($this->Customer_model->addCustomer($data)){
						$id = $this->db->insert_id();
						$this->sendMail($data['address']);
						$data = $this->Customer_model->getUserbyId($id);
						$this->session->set_userdata('username', $data->username);
						$this->session->set_userdata('login', 'TRUE');
						$this->session->set_userdata('profile_id', $id);
						$this->session->set_userdata('last_name', $data->last_name);
						$this->session->set_flashdata('message', 'You have register successfuly.');

						$this->session->set_userdata('type', $data->type);
						$this->session->set_userdata('name', $data->first_name);
						$this->setUrl($data);

						$this->session->set_flashdata('message', 'You have registerd successfully');
						redirect(base_url('Customer/home/'.$id), 'refresh');
				}
			}
		}else{
			$this->session->set_flashdata('error', 'Captcha could not matched');
			redirect(base_url('home'),'refresh');
		}
	}

	public function setUrl($data){
		if(isset($data) && !empty($data)){
			if($data->type == 2){

				$msg = $this->Customer_model->getNoOfUnreadChatsById($data->id);
				$this->session->set_userdata('notification', $msg); 
				$this->session->set_userdata('user_type', $data->type);
				$this->session->set_userdata('profile_id', $data->id);
				$this->session->set_userdata('profile', base_url('Home/broker_profile/'.$data->id));
				$this->session->set_userdata('change_password', base_url('Home/change_password/'.$data->id));
				$this->session->set_userdata('change_subscription', base_url('Home/change_subscription/'.$data->id));
				$this->session->set_userdata('cancel_subscription', base_url('Home/cancel_subscription/'.$data->id));
				$this->session->set_userdata('deactivate_account', base_url('Home/deactivate_acc/'.$data->id));
				$this->session->set_flashdata('message', 'You have logged in as financial broker successfuly');
				redirect(base_url('Home/broker_profile/'.$data->id),'refresh');
			}elseif($data->type == 3){
				$msg = $this->Customer_model->getNoOfUnreadChatsById($data->id);
				$this->session->set_userdata('notification', $msg); 
				$this->session->set_userdata('profile_id', $data->id);
				$this->session->set_userdata('user_type', $data->type);
				$this->session->set_userdata('profile', base_url('Customer/profile/'.$data->id));
				$this->session->set_userdata('change_password', base_url('Home/change_password/'.$data->id));
				$this->session->set_userdata('dashboard', base_url('Customer/home/'.$data->id));
				$this->session->set_flashdata('message', 'You have logged in as customer successfuly');
				redirect(base_url('Customer/home/'.$data->id),'refresh');
			}else{
				$this->session->set_userdata('profile', base_url('home'));
				$this->session->set_flashdata('warning', 'Sorry your detail did not matched to any profile');
				redirect(base_url('home'),'refresh');
			}
		}
	}

	public function profile($id){
                $session_id = $this->session->userdata('profile_id');
				if($id != $session_id){
					redirect(base_url('Home/broker_profile/'.$session_id),'refresh');
				}
				
				$config['upload_path'] = './uploads/profilepics/';
				$config['allowed_types'] = 'gif|jpg|png|jpeg';
				$config['max_size'] = 2500000;
				$this->load->library('upload', $config);

				$this->upload->do_upload('profile_img_path');
				$data_upload_files = $this->upload->data();
              
				$image = $data_upload_files['file_name']; 
               if($this->input->post()){
                $data = array(
						'last_name' => $this->input->post('last_name'),
						'first_name' => $this->input->post('first_name'),
						'state' => $this->input->post('state'),
						'suburb' => $this->input->post('suburb'),
						'household_income' => $this->input->post('household_income'),
						'age' => $this->input->post('cage'),
						'post_code' => $this->input->post('post_code'),
                        'home' => $this->input->post('home'),
                        'mobile' => $this->input->post('mobile'),
                        'family_members' => $this->input->post('family_members'),
                        'phone_code' => $this->input->post('phone_code'),
                        'profile_img_path' => $image 
						);
				    
					
					$this->upload->initialize($config);
					
						
					     if($this->Customer_model->addCustomerDetails($data, $id)){
					         $this->session->set_flashdata('message', 'user details updated successfuly');
				            }

				}
		
						$this->data['state'] = $this->Customer_model->getState();
						$this->data['profile'] = $this->Customer_model->getCustomerById($id);
						$this->data['phone_code'] = $this->getPhoneCode();
						$this->data['favouritcustomer'] = $this->Customer_model->favouritCustomer($id);
						$this->data['val'] = "val";
						//$this->data['customer_profile'] = $this->Home_model->getCustomerProfileById($id);


						$bc = array(array('link' => base_url(), 'page' => 'Admin'), array('link' => '#', 'page' => 'Admin'));
						$meta = array('page_title' => 'Admin', 'bc' => $bc);
						$this->page_profile('front/customer/profile', $meta, $this->data);
			
   }

   public function broker_profile($id){
   		if($id == ""){
   			$id = $this->session->userdata('profile_id');
   			redirect(base_url('Customer/profile/'.$id),'refresh');
   		}
   		$this->data['id'] = $id;
   		$this->data['broker'] = $this->Customer_model->getUserById($id);
   		$customer = $this->Customer_model->getUserByUsername($this->session->userdata('username'));
   		$this->data['favourit'] = $this->Customer_model->getFavouritByBrokerId($id, $customer->id);
   		$this->data['customer'] = $customer;
   		$broker_profile = $this->Customer_model->getBrokerProfileByIdAndData($id);
		$this->data['broker_profile']= $broker_profile;
		$this->data['state'] = $this->Customer_model->getStateById($broker_profile->state);
		
   		$bc = array(array('link' => base_url(), 'page' => 'Admin'), array('link' => '#', 'page' => 'Admin'));
		$meta = array('page_title' => 'Admin', 'bc' => $bc);
		$this->page_profile('front1/broker_profile', $meta, $this->data);
   }

	public function view(){
		
	}

	public function edit(){
		
	}

	public function delete(){
		
	}


	public function home($id){
		$session_id = $this->session->userdata('profile_id');
		if($id != $session_id){
			redirect(base_url('Home/broker_profile/'.$session_id),'refresh');
		}
		
		//$this->session->set_userdata('id', $this->uri->segment(3));
		$this->data['broker'] = $this->Customer_model->getBrokerAll();
		$this->data['customer_id'] = $id;
		$bc = array(array('link' => base_url(), 'page' => 'Admin'), array('link' => '#', 'page' => 'Admin'));
        $meta = array('page_title' => 'Admin', 'bc' => $bc);
        $this->page_profile('front1/customer_home', $meta, $this->data);
	}

	public function validateEmail(){
		if($this->input->post()){
			$email = $this->input->post('email');
			$result = $this->Customer_model->validateEmail($email);
		}
		echo json_encode($result);

	}

	public function sendMail($email_id){
			$uname = explode('@', $email_id);
			$to = "p.elebnis@gmail.com, ".$email_id;
			$subject = $uname[0]." Finance broker registration";

			$message = '<html>
			<head>
			<title>Registration Verification</title>
			</head>
			<body>
			<p>
			Hello <br> <b>'.$uname[0].'</b> 

			</p>
			You have successfuly registered as customer on <a href="http://workspace.elebnis.com/financebroker/" title="finance broker">finance broker</a>
			Call : +XX-XXXX-XXXX-XX
			<br>
			To verify your account
			</body>
			</html>';

			// Always set content-type when sending HTML email
			$headers = "MIME-Version: 1.0" . "\r\n";
			$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

			// More headers
			$headers .= 'From: <workspace@elebnis.com>' . "\r\n";
			$headers .= 'Cc: pankaj@elebnis.com' . "\r\n";

			mail($to,$subject,$message,$headers);
			
		mail($to,$subject,$message,$headers);
	}
	//=============== 21 june 2017 start by sushant ===========//
	public function search_broker_by_name($keyword='', $sort=''){
		$c_id = $this->session->userdata('id');
		if(isset($sort) && !empty($sort)){
			$zipcode = $this->Customer_model->getZipcodeById($c_id);
			$point = $this->getLatAndLong($zipcode);

				// var_dump($point['lat']); exit();
				// if($sort == '1'){
				// 	$return_value = $this->Customer_model->ajax_get_broker_details_by_keyword($keyword);
				// 	//print_r($return_value); exit();
				// 	foreach ($return_value as $value) {
				// 		print_r($value->post_code);
				// 	}
				// 	exit();
				// }
		}else{
			$return_value['broker'] = $this->Customer_model->ajax_get_broker_details_by_keyword($keyword);
			// print_r($return_value); exit();
			$this->load->view('front1/ajax_pages/ajax_search_broker', $return_value);
		}
	}
	public function getLatAndLong($zipcode){
		$url = "http://maps.googleapis.com/maps/api/geocode/json?address=".$zipcode."&sensor=false";
		$details=file_get_contents($url);
		$result = json_decode($details,true);
		$lat=$result['results'][0]['geometry']['location']['lat'];
		$lng=$result['results'][0]['geometry']['location']['lng'];
		$point = array('lat'=>$lat, 'lng'=>$lng);
		return $point;

	}

	public function favourit(){
		$broker_id = $this->input->post('broker_id');
		$customer_id = $this->input->post('customer_id');

		$data = $this->Customer_model->addFavourit($broker_id, $customer_id);
		echo json_encode($data);
	}

	//=============== end by sushant ===========//
	//=============== 22 june 2017 start by sushant ===========//
	public function ajax_add_contact_to_broker($bid=''){
		$cid = $this->session->userdata('id');
		if(isset($bid) && !empty($bid)){
			$data = array('broker_id'=>$bid, 'customer_id' => $cid);
			$return_value = $this->Customer_model->add_contact_to_broker($data);
			echo $return_value;
		}
	}
	public function ajax_block_unblock_to_broker($bid='',$status=''){
		$cid = $this->session->userdata('id');
		if(isset($bid) && !empty($bid)){
			$data = array('broker_id'=>$bid, 'customer_id' => $cid);
			$return_value = $this->Customer_model->block_unblock_to_broker($data,$status);
			echo json_encode($return_value);
		}
	}
	public function block_unblock_broker_by_id($cid='', $bid='', $bstatus=''){
		if(isset($cid) && !empty($cid)){
			$data = array('broker_id'=>$bid, 'customer_id' => $cid);
			$return_value = $this->Customer_model->block_unblock_to_broker($data,$bstatus);
			if($return_value != 1 ){
				$this->session->set_flashdata('message', 'Oops Something went wrong');
				redirect(base_url('Customer/broker_profile/'.$bid), 'refresh');
			}else{
				$this->session->set_flashdata('message', 'Blocked broker successfuly');
				redirect(base_url('Customer/broker_profile/'.$bid), 'refresh');
			}
		}
	}
	public function add_contact_to_broker($bid=''){
		$cid = $this->session->userdata('profile_id');
		// echo $cid; exit();
		if(isset($bid) && !empty($bid)){
			$data = array('broker_id'=>$bid, 'customer_id' => $cid);
			$return_value = $this->Customer_model->add_contact_to_broker($data);
			if($return_value == 1){
				$this->session->set_flashdata('message', 'Requested to Connnect');
				redirect(base_url('Customer/broker_profile/'.$bid), 'refresh');
			}else{
				$this->session->set_flashdata('message', 'Oops Something went wrong');
				redirect(base_url('Customer/broker_profile/'.$bid), 'refresh');
			}
		}
	}
	public function cancel_request($bid=''){
		$cid = $this->session->userdata('profile_id');
		if(isset($bid) && !empty($bid)){
			$return_value = $this->Customer_model->cancel_request($bid,$cid);
			if($return_value == 1){
				$this->session->set_flashdata('message', 'Request Cancelled successfully');
				redirect(base_url('Customer/broker_profile/'.$bid), 'refresh');
			}else{
				$this->session->set_flashdata('message', 'Oops Something went wrong');
				redirect(base_url('Customer/broker_profile/'.$bid), 'refresh');
			}
		}
	}
	//=============== end by sushant ===========//
}

/* End of file Admin.php */
/* Location: ./application/controllers/Admin.php */