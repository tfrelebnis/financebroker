<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Auth_model');
		$this->load->helper(array('form', 'url'));

	}

	public function index()
	{
		if($this->session->userdata('login') == 1){
			redirect(base_url('Welcome/home'),'refresh');
		}
		if($this->input->post()){
			$data = array(
				'username'=> $this->input->post('username'),
				'password'=> $this->input->post('password'),
				'ip_info'=> $this->input->post('ip_info'),
				'user_agent'=> $this->input->post('user_agent')
			);
			if($this->Auth_model->login($data)){
				$this->session->set_userdata('login', '1');
				$this->session->set_flashdata('message', 'logged in successfully');
				redirect('admin','refresh');
			}
		}
		$this->load->view('welcome_message');
	}

	public function logout(){
		$this->session->set_userdata('login', '0');
		redirect(base_url('Welcome/'),'refresh');
	}

	public function home(){
		$this->Auth_model->auth($this->session->userdata());
		$test = 'Pankaj';
		$this->data['val'] = 'pankaj';
		$bc = array(array('link' => base_url(), 'page' => 'Admin'), array('link' => '#', 'page' => 'Admin'));
        $meta = array('page_title' => 'Admin', 'bc' => $bc);
        $this->page_construct('admin/index', $meta, $this->data);
	}
}
