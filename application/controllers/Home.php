<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
		$this->load->model('Home_model');
	}

	public function index()
	{	
		if(isset($_POST) && !empty($_POST)){
		if($this->input->post('g-recaptcha-response')){
			$secret = '6LcdkyUUAAAAAIFug0zT9aQ1Y09TC4IYH9Wnd4iZ';
			 $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
        $responseData = json_decode($verifyResponse);
        	if($responseData->success){
        		$this->session->set_flashdata('warning', 'Click all fields agains');
			}
				$data = array(
					'type' => '2',
					'username' => $this->input->post('email_address'),
					'password' => $this->input->post('password'),
					'last_name' => $this->input->post('last_name'),
					'first_name' => $this->input->post('first_name'),
					'state' => $this->input->post('state'),
					'business_name' => $this->input->post('business_name'),
					'credit_license' => $this->input->post('credit_license'),
					'business_specification' => implode(',', $this->input->post('business_specification')),
					'address' => $this->input->post('email_address'),
					'status' => '1'
					);

				if($this->Home_model->addBroker($data)){
					$insert_id = $this->db->insert_id(); 
					$this->sendMail($data['address']);
					
					$data = $this->Home_model->getUserbyId($insert_id);
					$this->session->set_userdata('username', $data->username);
					$this->session->set_userdata('login', 'TRUE');
					$this->session->set_userdata('profile_id', $insert_id);
					$this->session->set_userdata('last_name', $data->last_name);
					$this->session->set_flashdata('message', 'You have register successfuly.');

					$this->session->set_userdata('type', $data->type);
					$this->session->set_userdata('name', $data->first_name);
					$this->setUrl($data);


					
					redirect(base_url('Home/broker_profile/'.$insert_id), 'refresh');
				}
			
		}else{
			$this->session->set_flashdata('error', 'Captcha could not matched');
		}
	}

		$vals = array(
	     'img_path'  => './captcha/',
	     'img_url'  => base_url().'/captcha/',
	     'img_width' => 150,
	     'img_height' => '50',
	     'expiration' => 7200
	     );
	 	$cap = create_captcha($vals);
		$this->data['captcha'] = $cap;
		$this->data['state'] = $this->Home_model->getStateAll();
		$bc = array(array('link' => base_url(), 'page' => 'Admin'), array('link' => '#', 'page' => 'Admin'));
        $meta = array('page_title' => 'Admin', 'bc' => $bc);
        $this->page_front('front/index', $meta, $this->data);
	}

	public function validateEmail(){
		if($this->input->post()){
			$email = $this->input->post('email');
			$result = $this->Home_model->validateEmail($email);
		}
		echo json_encode($result);
	}

	public function validateBusinessName(){
		if($this->input->post()){
			$bname = $this->input->post('bname');
			$result = $this->Home_model->validateBusinessName($bname);
		}
		echo json_encode($result);

	}

	public function sendMail($email_id){
			$uname = explode('@', $email_id);
			$to = "p.elebnis@gmail.com, ".$email_id;
			$subject = $uname[0]." Finance broker registration";

			$message = '<html>
			<head>
			<title>Registration Verification</title>
			</head>
			<body>
			<p>
			Hello <br> <b>'.$uname[0].'</b> 

			</p>
			You have successfuly registered on <a href="http://workspace.elebnis.com/financebroker/" title="finance broker">finance broker</a>
			Call : +XX-XXXX-XXXX-XX
			<br>
			To verify your account
			</body>
			</html>';

			// Always set content-type when sending HTML email
			$headers = "MIME-Version: 1.0" . "\r\n";
			$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

			// More headers
			$headers .= 'From: <workspace@elebnis.com>' . "\r\n";
			$headers .= 'Cc: pankaj@elebnis.com' . "\r\n";

			mail($to,$subject,$message,$headers);
			

	}

	public function brokerLogin(){
		if($this->input->post()){
			$data = array(
				'username' => $this->input->post('email'),
				'password' => $this->input->post('password')
				);
			if($data = $this->Home_model->brokerLogin($data)){
				$this->session->set_userdata('type', $data->type);
				$this->session->set_userdata('username', $data->username);
				$this->session->set_userdata('name', $data->first_name);
				$this->session->set_userdata('login', 'TRUE');
				$this->session->set_userdata('last_name', $data->last_name);
				$this->setUrl($data);	
			}else{
				$this->session->set_flashdata('error', 'Username or password did not matched');
				redirect(base_url('Home/'),'refresh');
			}
		}
	}

	public function setUrl($data){
		if(isset($data) && !empty($data)){
			if($data->type == 2){

				$msg = $this->Home_model->getNoOfUnreadChatsById($data->id);
				$this->session->set_userdata('notification', $msg); 
				$this->session->set_userdata('user_type', $data->type);
				$this->session->set_userdata('profile_id', $data->id);
				$this->session->set_userdata('profile', base_url('Home/broker_profile/'.$data->id));
				$this->session->set_userdata('dashboard', base_url('Home/broker_dashboard/'.$data->id));
				
				$this->session->set_userdata('change_password', base_url('Home/change_password/'.$data->id));
				$this->session->set_userdata('change_subscription', base_url('Home/change_subscription/'.$data->id));
				$this->session->set_userdata('cancel_subscription', base_url('Home/cancel_subscription/'.$data->id));
				$this->session->set_userdata('deactivate_account', base_url('Home/deactivate_acc/'.$data->id));
				$this->session->set_flashdata('message', 'You have logged in as financial broker successfuly');

				if($this->Home_model->getNoCustomerById($data->id)){
					redirect(base_url('Home/broker_dashboard/'.$data->id),'refresh');
				}else{
					redirect(base_url('Home/broker_profile/'.$data->id),'refresh');
				}

			}elseif($data->type == 3){
				$msg = $this->Home_model->getNoOfUnreadChatsById($data->id);
				$this->session->set_userdata('notification', $msg); 
				$this->session->set_userdata('profile_id', $data->id);
				$this->session->set_userdata('user_type', $data->type);
				$this->session->set_userdata('profile', base_url('Customer/profile/'.$data->id));
				$this->session->set_userdata('change_password', base_url('Home/change_password/'.$data->id));
				$this->session->set_userdata('dashboard', base_url('Customer/home/'.$data->id));
				$this->session->set_flashdata('message', 'You have logged in as customer successfuly');
				redirect(base_url('Customer/home/'.$data->id),'refresh');
			}else{
				$this->session->set_userdata('profile', base_url('home'));
				$this->session->set_flashdata('warning', 'Sorry your detail did not matched to any profile');
				redirect(base_url('home'),'refresh');
			}
		}
	}

	public function broker_profile($id){
						$session_id = $this->session->userdata('profile_id');
						if($id != $session_id){
							redirect(base_url('Home/broker_profile/'.$session_id),'refresh');
						}
						$config['upload_path'] = '/uploads/profilepics';
						$config['allowed_types'] = 'gif|jpg|png|jpeg';
						$config['max_size'] = 250;
						$this->load->library('upload', $config);
						$this->upload->do_upload('profile_img_path');
						$data_upload_files = $this->upload->data();

						$image = $data_upload_files['full_path']; 

						
				

						if(isset($_POST) && !empty($_POST)){

				        $data = array(
						'last_name' => $this->input->post('last_name'),
						'first_name' => $this->input->post('first_name'),
						'business_name' => $this->input->post('business_name'),
						'credit_license' => $this->input->post('credit_license'),
						'business_specification' => implode(',', $this->input->post('business_specification')),
						'title' => $this->input->post('title'),
						'aggregator' => $this->input->post('aggregator'),
						'business_qualification' => $this->input->post('business_qualification'),
						'biography' => $this->input->post('biography'),
						'business_addess_1' => $this->input->post('business_address_1'),
						'business_addess_2' => $this->input->post('business_address_2'),

						'state' => $this->input->post('state'),


						'suburb' => $this->input->post('suburb'),
						'post_code' => $this->input->post('post_code'),
						'country' => $this->input->post('country'),
						'office' => $this->input->post('office'),
						'website' => $this->input->post('website'),
						'mobile' => $this->input->post('mobile'),
						'fax' => $this->input->post('fax'),
						'fb' => $this->input->post('facebook'),
						'tw' => $this->input->post('twitter'),
						'ln' => $this->input->post('linkedin'),
						'profile_img_path' => $image 
						
					);


			if($this->Home_model->addUserDetails($data, $id)){
				$this->session->set_flashdata('message', 'user details updated successfuly');

			}
		}
		$id = $this->session->userdata('profile_id');
		$data = $this->Home_model->getBrokerProfileById($id);
		$this->data['package'] = $this->Home_model->getPackagesAll();
		$this->data['countries'] = $this->Home_model->getCountriesAll();
		$this->data['broker'] = $this->Home_model->getBrokerProfileById($id);
		$this->data['state'] = $this->Home_model->getStateAll();

		if(isset($this->data['broker']) && !empty($this->data['broker'])){
			$current_date = time();
			$email = $this->data['broker']->address;
		}
		// echo $current_date.'<br>'.$email; exit();
		$date = $this->Home_model->getPaymentStatusbyEmail($email);
		if(isset($date) && !empty($date)){
			date_default_timezone_set('UTC');
			$x = strtotime($date);
			$diff = abs($current_date - $x);
			// $years = floor($diff / (365*60*60*24));
			// $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
			// $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
			$days = $diff/86400;
			// var_dump($days); exit();
			if($days>30){
				$this->data['subscribed_statue'] = 0;		 	
			}else{
				$this->data['subscribed_statue'] = 	1;
			}
		}else{
			$this->data['subscribed_statue'] = 0;		
		}
		// print_r($this->data['subscribed_statue']); exit();
		$bc = array(array('link' => base_url(), 'page' => 'Admin'), array('link' => '#', 'page' => 'Admin'));
        $meta = array('page_title' => 'Admin', 'bc' => $bc);
        $this->page_profile('front/broker_profile_unsubscribe', $meta, $this->data);
	}

	public function getStateByCountry($country_id){
		$data = $this->Home_model->getStateByCountry($country_id);
		$state = '<select placeholder="Please select state" class="form-control" name="states">';
		if(isset($data) && !empty($data)){
			foreach($data as $row){
				$state .= '<option value="'.$row->id.'"> '.$row->name.' </option>';
			}
		}
		$state .= '</select>';
		echo json_encode($state);
		
	}

	public function test(){
		//$this->load->view('file');
		print_r($this->session->userdata()); exit;
	}

	public function purchase_package() {
		$price = $this->uri->segment(4);
		$user_id = $this->uri->segment(3);

		$user = $this->Home_model->getUserById($user_id);
		$returnURL = base_url().'paypal/success/'.$user_id;//payment success url
		$cancelURL = base_url().'paypal/cancel/'.$user_id;//payment cancel url
		$notifyURL = base_url().'paypal/ipn';//ipn url
		//get particular product data
		$uniqueid = uniqid();
		$userID  = $user->id;//current user id
		$logo    = base_url('assets/front/img/event_plan_logo.png');
		$this->paypal_lib->add_field('return', $returnURL);
		$this->paypal_lib->add_field('cancel_return', $cancelURL);
		$this->paypal_lib->add_field('notify_url', $notifyURL);
		$this->paypal_lib->add_field('item_name', 'Finance Broker Package');
		$this->paypal_lib->add_field('custom', $user->username);
		$this->paypal_lib->add_field('item_number', $uniqueid);
		$this->paypal_lib->add_field('amount', $price);
		$this->paypal_lib->image($logo);
		$this->paypal_lib->paypal_auto_form();
	}

	public function broker_dashboard($broker_id){
		if($broker_id == ""){
			$id = $this->session->userdata('profile_id');
			redirect(base_url('Home/broker_dashboard/'.$id),'refresh');
		}
		/*$this->data['broker'] = $this->Home_model->getBrokerProfileById($broker_id);
		$return_value = $this->Home_model->gethiringDetails($broker_id);
		// var_dump($return_value); exit();
		if($return_value == 0){
			$this->broker_profile($broker_id);
		}else{
			$msg = $this->Home_model->getNoOfUnreadChatsById($broker_id);

			$bc = array(array('link' => base_url(), 'page' => 'Admin'), array('link' => '#', 'page' => 'Admin'));
	        $meta = array('page_title' => 'Admin', 'bc' => $bc, 'msg' => $msg);
	        $this->page_profile('front/broker_favourite_dashboard', $meta, $this->data);
	    }*/
		$broker = $this->Home_model->getBrokerProfileById($broker_id);
		$this->data['broker'] = $broker;

		$this->data['hiredcustomer'] = $this->Home_model->getHiredCustomerByBrokerId($broker->id);
		$this->data['customer'] = $this->Home_model->getFavouriteCustomer($broker->id);
		$this->data['requestedCustomer'] = $this->Home_model->getRequestById($broker->id);


		$msg = $this->Home_model->getNoOfUnreadChatsById($broker_id);
		
		$bc = array(array('link' => base_url(), 'page' => 'Admin'), array('link' => '#', 'page' => 'Admin'));
        $meta = array('page_title' => 'Admin', 'bc' => $bc, 'msg' => $msg);
        $this->page_profile('front/broker_favourite_dashboard', $meta, $this->data);
	}

	public function my_fb_files(){
		$this->data['val'] = "VAL";
		$bc = array(array('link' => base_url(), 'page' => 'Admin'), array('link' => '#', 'page' => 'Admin'));
        $meta = array('page_title' => 'Admin', 'bc' => $bc);
        $this->page_profile('front/my_fb_files', $meta, $this->data);
	}

	public function deactivate_acc($id){
		$session_id = $this->session->userdata('profile_id');
		if($id != $session_id){

			redirect(base_url('Home/logout/'),'refresh');
		}
		
		$this->data['val'] = "VAL";
		$bc = array(array('link' => base_url(), 'page' => 'Admin'), array('link' => '#', 'page' => 'Admin'));
        $meta = array('page_title' => 'Admin', 'bc' => $bc);
        $this->page_profile('front/deactivate_acc', $meta, $this->data);
	}

	public function change_password($id){
		$session_id = $this->session->userdata('profile_id');
		if($id != $session_id){
			redirect(base_url('Home/broker_profile/'.$session_id),'refresh');
		}
		
		$this->data['val'] = "VAL";
		$bc = array(array('link' => base_url(), 'page' => 'Admin'), array('link' => '#', 'page' => 'Admin'));
        $meta = array('page_title' => 'Admin', 'bc' => $bc);
        $this->page_profile('front/change_password', $meta, $this->data);
	}

	public function my_fb_message(){
		$this->data['chat'] = $this->Home_model->getChatAll();
		$this->data['profile_customer'] = $this->Home_model->getCustomerAll();
		$this->data['profile_broker'] = $this->Home_model->getBrokerAll();
		$this->data['customer'] = $this->Home_model->getUserByUserName($this->session->userdata('username'));


		$bc = array(array('link' => base_url(), 'page' => 'Admin'), array('link' => '#', 'page' => 'Admin'));
        $meta = array('page_title' => 'Admin', 'bc' => $bc);
        $this->page_profile('front/my_fb_message', $meta, $this->data);
	}

	public function change_subscription(){
		$uri = $this->uri->segment(3);
		$session_id = $this->session->userdata('profile_id');
		if($uri != $session_id){
			redirect(base_url('Home/broker_profile/'.$session_id),'refresh');
		}
		
		$this->data['val'] = "VAL";
		$result = $this->Home_model->get_subscribed_package_details($uri);
		$package = $this->Home_model->get_all_package_details();
		$bc = array(array('link' => base_url(), 'page' => 'Admin'), array('link' => '#', 'page' => 'Admin'));
        $meta = array('page_title' => 'Admin', 'bc' => $bc, 'pack' => $result, 'all' => $package);
        $this->page_profile('front/change_subscription', $meta, $this->data);
	}
/*=customer_home===customer=page==function===*/
	public function customer_home(){
		$this->data['val'] = "VAL";
		$bc = array(array('link' => base_url(), 'page' => 'Admin'), array('link' => '#', 'page' => 'Admin'));
        $meta = array('page_title' => 'Admin', 'bc' => $bc);
        $this->page_profile('front1/customer_home', $meta, $this->data);
	}

/*====calculater==============================*/
	public function calc(){
		$this->data['val'] = "VAL";
		$bc = array(array('link' => base_url(), 'page' => 'Admin'), array('link' => '#', 'page' => 'Admin'));
        $meta = array('page_title' => 'Admin', 'bc' => $bc);
        $this->page_profile('front/calculater/calc', $meta, $this->data);
	}
/*====calculater==============================*/
	public function all_calc(){
		$this->data['val'] = "VAL";
		$bc = array(array('link' => base_url(), 'page' => 'Admin'), array('link' => '#', 'page' => 'Admin'));
        $meta = array('page_title' => 'Admin', 'bc' => $bc);
        $this->page_profile('front/calculater/all_calc', $meta, $this->data);
	}
	
/*=customer_home===customer=page==function===*/
	//============= coded by sushant on 14june2017 ===========//
	public function customer_profile($id=''){
		if(isset($id) && !empty($id)){

			$customer_profile = $this->Home_model->getCustomerProfileByIdAndDataByBroker($id);
			
			$this->data['customer_profile'] = $customer_profile;
			
			$this->data['state'] = $this->Home_model->getStateById($customer_profile->state);
			$this->data['id'] = $this->session->userdata('profile_id');
		
			$this->data['customer'] = $this->Home_model->getUserById($this->session->userdata('profile_id'));
			$broker = $this->Home_model->getUserById($this->session->userdata('profile_id'));
   			$this->data['favourit'] = $this->Home_model->getFavouritByBrokerId($broker->id, $id);
   			//print_r($customer_profile);
			if(isset($this->data) && !empty($this->data)){
				$bc = array(array('link' => base_url(), 'page' => 'Admin'), array('link' => '#', 'page' => 'Admin'));
				$meta = array('page_title' => 'Admin', 'bc' => $bc);
				$this->page_profile('front/customer_profile', $meta, $this->data);
			}


		}else{
			$this->session->set_flashdata('warning', 'no coustomer profile found');
			redirect(base_url('home'),'refresh');
			/*$this->data['val'] = "val";
			$bc = array(array('link' => base_url(), 'page' => 'Admin'), array('link' => '#', 'page' => 'Admin'));
			$meta = array('page_title' => 'Admin', 'bc' => $bc);
			$this->page_profile('front/customer_profile', $meta, $this->data);*/	

		}
	}
	public function inactive_customer_by_id($id='',$bid=''){
		if(isset($id) && !empty($id)){
			$return_value = $this->Home_model->inactive_customer_by_id($id,$bid);
			// echo $return_value; exit();
			if($return_value != 1 ){
				$this->session->set_flashdata('message', 'Oops Something went wrong');
				redirect(base_url('Home/customer_profile/'.$id), 'refresh');
			}else{
				$this->session->set_flashdata('message', 'Deleted customer successfuly');
				redirect(base_url('Home/customer_profile/'.$id), 'refresh');
			}
		}
	}
	public function block_unblock_customer_by_id($bid='', $cid='', $bstatus=''){
		if(isset($cid) && !empty($cid)){
			if($bstatus != '1'){
				$status = '1';
			}else{
			
				$status = '0';
			}
			$return_value = $this->Home_model->block_unblock_customer_by_id($bid,$cid,$status);
			if($return_value != 1 ){
				$this->session->set_flashdata('message', 'Oops Something went wrong');
				redirect(base_url('Home/customer_profile/'.$cid), 'refresh');
			}else{
				$this->session->set_flashdata('message', 'Blocked customer successfuly');
				redirect(base_url('Home/customer_profile/'.$cid), 'refresh');
			}
		}
	}
	public function accept_invitation($id='',$broker_id=''){
		$data = array('status' => '1');
		$return_value = $this->Home_model->update_table_broker_hiring($id,$broker_id,$data);
		if($return_value == 1){
			$this->session->set_flashdata('message', 'Request Accepted');
				redirect(base_url('Home/customer_profile/'.$id), 'refresh');
		}
	}
	//============= end coded by sushant on 14june2017 ===========//

	public function logout(){
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('name');
		$this->session->unset_userdata('login');
		$this->session->unset_userdata('last_name');
		$this->session->unset_userdata('profile');
		$this->session->unset_userdata('change_password');
		$this->session->unset_userdata('change_subscription');
		$this->session->unset_userdata('cancel_subscription');
		$this->session->unset_userdata('deactivate_account');

		redirect(base_url('home'),'refresh');
	}
	//============= end coded by sushant on 16june2017 ===========//
	public function ajax_marks_as_favourite_change($value='', $cid='', $bid=''){
			$return_value = $this->Home_model->marks_as_favourite_change($value,$cid,$bid);
				echo $return_value;
	}
	public function request_for_rate($cid='', $bid=''){
		if (isset($cid) && !empty($cid) && isset($bid) && !empty($bid)) {
			$return_value = $this->Home_model->request_for_rate($cid,$bid);
			if($return_value == 1){
				$this->session->set_flashdata('message', 'Request For Rate has Sent');
				redirect(base_url('Home/customer_profile/'.$cid), 'refresh');
			}else{
				$this->session->set_flashdata('message', 'Oops Something went wrong!');
				redirect(base_url('Home/customer_profile/'.$cid), 'refresh');
			}
		}
	}
	public function fb_account_deactivate_by_id($cid){
		if($this->input->post('other'))
			$other = $this->input->post('other');
		else
			$other = '';
		if($this->input->post() && isset($cid) && !empty($cid)){
			$data = array('description' => $other, 'reason_id'=>json_encode(implode(',', $this->input->post('reson'))));
			$return_value = $this->Home_model->fb_account_deactivate_by_id($cid,$data);
			if($return_value == 1){
				$this->session->set_flashdata('message', 'Account Deactivated');
				redirect(base_url('/'), 'refresh');
			}
		}
	}
	public function old_password_validate($password='',$id=''){
		if(isset($password) && !empty($password) && isset($id) && !empty($id)){
			$return_value = $this->Home_model->old_password_validate(urlencode($password), $id);
			echo $return_value; 
		}
	}
	public function change_password_action($id){
		if($this->input->post()){
			$data = array('password' => $this->input->post('newpassword'));
			$return_value = $this->Home_model->change_password($data,$id);
			if($return_value == 1){
				$this->session->set_flashdata('message', 'Password Changed successfully');
				redirect(base_url('Home/change_password/').$id, 'refresh');
			}
		}
	}
	//============= end coded by sushant on 16june2017 ===========//
	public function addChat($fb_id, $c_id){
		if($this->input->post()){
			$data = array('chat' => $this->input->post('chat'), 'fb_id' => $fb_id, 'c_id' => $c_id);
			if($this->Home_model->addChat($data)){
				echo json_encode(array('message' => 'chat added sussfully'));
			}
		}
	}

	public function getChatAll(){
		$data = $this->Home_model->getChatAll();
		$msg = '';
		foreach($data as $row){
			$broker = $this->Home_model->getUsersById($row->fb_id);
			$customer = $this->Home_model->getUsersById($row->c_id);
			$msg .= '<div class="messag_list">
              <p class="fb_msg_hed"><span class="nam_ltr_circl">'.strtoupper(substr($customer->first_name, 0, 1)).''.strtoupper(substr($customer->last_name, 0, 1)).'<span class="nam_ltr_avail"></span></span><span class="fb_user_name">'.$customer->first_name.' '.$customer->last_name.' - In a busy</span> <span class="pull-right"><small>7:25 AM</small></span></p>
              <p class="fb_msg_txt">'.$row->chat.'</p>
            </div>';
		}
		echo $msg;
	}

	//============= start code by sushant on 17june2017 ===========//
	public function ajax_validate_email($email){
		$return_value = $this->Home_model->ajax_validate_email($email);
		if($return_value != 1){
			echo $return_value;
		}else{
			$password = random_string('alnum', 6);
			$this->load->library('email');
			$this->email->from('financebroker@elebnis.com', 'Admin Finance Broker');
			$this->email->to($email);
			$this->email->subject('New Password');
			$this->email->message('Your New Password is: '.$password);
			$this->email->send();
			if($this->db->update('user', array('password' => $password), array('username' => $email))){
				echo $return_value;
			}
		}
	}
	public function ajax_notification_details_by_id($id=''){
		if(isset($id) && !empty($id)){
			$return_value = $this->Home_model->get_notification_details_by_id($id);
			if(isset($return_value) && !empty($return_value)){
				foreach ($return_value as $key) {
					echo '<li><a href="">You have '.$key->total.' unread message/s from '.$key->name.'</a></li>';
				}
			}else{
				echo '<li>Sorry! There is no notification</li>';
			}
		}
	}
	public function subscribe_package_pay_broker($value, $u_id) {
		$price = $value;
		$user_id = $u_id;

		$user = $this->Home_model->getUserById($user_id);
		$returnURL = base_url().'subscribe_package_url_paypal/'.$user_id;//payment success url
		$cancelURL = base_url().'subscribe_package_url_paypal/'.$user_id;//payment cancel url
		$notifyURL = base_url().'paypal/ipn';//ipn url
		//get particular product data
		$uniqueid = uniqid();
		$userID  = $user->id;//current user id
		$logo    = base_url('assets/front/img/event_plan_logo.png');
		$this->paypal_lib->add_field('return', $returnURL);
		$this->paypal_lib->add_field('cancel_return', $cancelURL);
		$this->paypal_lib->add_field('notify_url', $notifyURL);
		$this->paypal_lib->add_field('item_name', 'Finance Broker Subscribe Package');
		$this->paypal_lib->add_field('custom', $user->username);
		$this->paypal_lib->add_field('item_number', $uniqueid);
		$this->paypal_lib->add_field('amount', $price);
		$this->paypal_lib->image($logo);
		$this->paypal_lib->paypal_auto_form();
	}
	public function subscribe_package_url_paypal(){
		//user didn't give his credential of paypal
	}
	public function subscribe_package($p_type='', $b_id='', $p_id='', $amt=''){
		if(isset($p_type) && isset($b_id) && isset($p_id)){
			// $this->subscribe_package_pay_broker($amt, $b_id);
			$data = array('subscribed_packege_id' => $p_id, 'subscribed_packege_type' => $p_type);
			$return_value = $this->Home_model->update_subscription_of_broker($b_id, $data);
			if($return_value == 1){
				$this->session->set_flashdata('message', 'Package Subscribed successfully');
				redirect(base_url('Home/change_subscription/').$b_id, 'refresh');
			}
		}
	}
	public function cancel_subscription_by_id($id=''){
		if (isset($id) && !empty($id)) {
			$return_value = $this->Home_model->cancel_subscription_by_id($id);
			if($return_value == 1 ){
				$this->session->set_flashdata('message', 'Cancel Subscription successfully');
				redirect(base_url('Home/change_subscription/').$id, 'refresh');
			}
		}
	}
	//============= end code by sushant on 17june2017 ===========//

	public function passEncrypt($pass){
		$pass = base64_encode($pass);
		$pass = base64_encode($pass);
		$pass = base64_encode($pass);
		$pass = base64_encode($pass);
		$pass = base64_encode($pass);
		$pass = base64_encode($pass);
		$pass = base64_encode($pass);
		$pass = base64_encode($pass);
		$pass = base64_encode($pass);
		$pass = base64_encode($pass);
		return $pass;
	}

	public function passDecrypt($pass){
		$pass = base64_decode($pass);
		$pass = base64_decode($pass);
		$pass = base64_decode($pass);
		$pass = base64_decode($pass);
		$pass = base64_decode($pass);
		$pass = base64_decode($pass);
		$pass = base64_decode($pass);
		$pass = base64_decode($pass);
		$pass = base64_decode($pass);
		$pass = base64_decode($pass);
		return $pass;
	}

	public function search($closetome = '', $state_id = '', $post_code = '', $rating = ''){
		if(isset($closetome) && !empty($closetome)){
			$closetome = $closetome;
		}else{
			$closetome = '';
		}

		if(isset($state_id) && !empty($state_id)){
			$state_id = $state_id;
		}else{
			$state_id = '';
		}
		
		if(isset($post_code) && !empty($post_code)){
			$post_code = $post_code;
		}else{
			$post_code = '';
		}
		
		if(isset($rating) && !empty($rating)){
			$rating = $rating;
		}else{
			$rating = '';
		}

		if($data = $this->Home_model->getSearch($closetome, $state_id, $post_code, $rating)){
			print_r($data); 
		}

		//echo $closetome.'<br>'.$state_id.'<br>'.$post_code.'<br>'.$rating;

	}

	public function session(){
		print_r($this->session->userdata()); 
	}

	public function getUserInfoAll(){
		$broker_id = $this->input->get('broker_id');
		$customer_id = $this->input->get('customer_id');
		$data = $this->Home_model->getUserChat($customer_id, $broker_id);
		$msg = '';
		foreach($data as $row){
			$broker = $this->Home_model->getUsersById($row->fb_id);
			$customer = $this->Home_model->getUsersById($row->c_id);
			$msg .= '<div class="messag_list">
              <p class="fb_msg_hed"><span class="nam_ltr_circl">'.strtoupper(substr($customer->first_name, 0, 1)).''.strtoupper(substr($customer->last_name, 0, 1)).'<span class="nam_ltr_avail"></span></span><span class="fb_user_name">'.$customer->first_name.' '.$customer->last_name.' - In a busy</span> <span class="pull-right"><small>7:25 AM</small></span></p>
              <p class="fb_msg_txt">'.$row->chat.'</p>
            </div>';
		}
		echo $msg;
		echo json_encode($data);
	}





}

