<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Database extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('Admin_model');
		$this->load->model('Auth_model');
		$this->Auth_model->auth($this->session->userdata());

	}

	public function index()
	{
		if($this->db->query("")){
			echo 'Database created successfully';
		}else{
			echo 'Something went wrong';
		}
	}

	public function drop(){
		if($this->db->query("DROP TABLE `fb_chat`, `fb_countries`, `fb_currency`, `fb_customer_hiring`, `fb_deactivated_account`, `fb_migrations`, `fb_package_content`, `fb_payment`, `fb_phone_code`, `fb_states`, `fb_static_pages`, `fb_user`, `fb_user_details`, `fb_user_login`";
		}
	}


}

/* End of file Admin.php */
/* Location: ./application/controllers/Admin.php */

