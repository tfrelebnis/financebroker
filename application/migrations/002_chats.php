<?php
class Migration_Chats extends CI_Migration
{
    public function up()
    {
      $this->chat();
      $this->countries();
      $this->currency();
      $this->customer_hiring();
      $this->deactivated_account();
      $this->package_content();
      $this->payment();
      $this->phone_code();
      $this->states();
      $this->static_pages();
      $this->user();
      $this->user_details();
      $this->user_login();
      $this->block_details();
    }

    public function down()
    {
        $this->dbforge->drop_table('chat');
        $this->dbforge->drop_table('countries');
        $this->dbforge->drop_table('currency');
        $this->dbforge->drop_table('customer_hiring');
        $this->dbforge->drop_table('deactivated_account');
        $this->dbforge->drop_table('package_content');
        $this->dbforge->drop_table('payment');
        $this->dbforge->drop_table('phone_code');
        $this->dbforge->drop_table('states');
        $this->dbforge->drop_table('static_pages');
        $this->dbforge->drop_table('user');
        $this->dbforge->drop_table('user_details');
        $this->dbforge->drop_table('user_login');
        $this->dbforge->drop_table('block_details');
    }

    public function chat(){
        $this->dbforge->add_field(
           array(
                'id' => array(
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => true,
                'auto_increment' => true
              ),
              'chat' => array(
                 'type' => 'longtext',
                 'null' => true,
              ),
              'fb_id' => array(
                 'type' => 'INT',
                 'constraint' => 5,
                 'null' => true,
              ),
              'c_id' => array(
                 'type' => 'INT',
                 'constraint' => 5,
                 'null' => true,
              ),
              'read_status' => array(
                 'type' => 'ENUM',
                 'constraint' => array('0','1'),
                 'default' => '0',
                 'null' => true,
              ),
              'date TIMESTAMP DEFAULT CURRENT_TIMESTAMP',
          )
        );

        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('chat');
    }

    public function countries(){
      $this->dbforge->add_field(
           array(
                'id' => array(
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => true,
                'auto_increment' => true
              ),
              'country_code' => array(
                 'type' => 'VARCHAR',
                 'default' => 'NULL',
                 'constraint' => 5,
                 'null' => true,
              ),
              'country_name' => array(
                 'type' => 'VARCHAR',
                 'default' => 'NULL',
                 'constraint' => 50,
                 'null' => true,
              ),
           )
        );

        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('countries');
    }

    public function currency(){
      $this->dbforge->add_field(
           array(
                'id' => array(
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => true,
                'auto_increment' => true
              ),
              'iso' => array(
                 'type' => 'CHAR',
                 'constraint' => 3,
                 'null' => true,
              ),
              'name' => array(
                 'type' => 'VARCHAR',
                 'constraint' => 255,
                 'null' => true,
              ),
           )
        );
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('currency');
    }

    public function customer_hiring(){
      $this->dbforge->add_field(
           array(
                'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => true,
                'auto_increment' => true
              ),
               'customer_id' => array(
                 'type' => 'INT',
                 'constraint' => 11,
                 'null' => true,
              ),
              'broker_id' => array(
                 'type' => 'INT',
                 'constraint' => 11,
                 'null' => true,
              ),
              'status' => array(
                 'type' => "ENUM",
                 'constraint' => array('0','1','2'),
                 'default' => '0',
                 'null' => true,
              ),
              'favourite' => array(
                 'type' => "ENUM",
                 'constraint' => array('0','1'),
                 'default' => '0',
                 'null' => true,
              ),
              'star_rate' => array(
                 'type' => "ENUM",
                 'constraint' => array('0','6','1','2','3','4','5'),
                 'default' => '0',
                 'null' => true,
              ),

              'update_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP',
              )
        );

        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('customer_hiring');
    }

    public function deactivated_account(){
      $this->dbforge->add_field(
           array(
                'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => true,
                'auto_increment' => true
              ),
               'user_id' => array(
                 'type' => 'INT',
                 'constraint' => 11,
                 'null' => true,
              ),
               'reason_id' => array(
                 'type' => 'INT',
                 'constraint' => 11,
                 'null' => true,
              ),
              'description' => array(
                 'type' => 'longtext',
                 'null' => true,
              ),

              'updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP',
              )
        );

        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('deactivated_account');
    }

    public function package_content(){
      $this->dbforge->add_field(
           array(
                'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => true,
                'auto_increment' => true
              ),
               'package_id' => array(
                 'type' => 'ENUM',
                 'constraint' => array('1','2'),
                 'default' => '1',
                 'null' => true,
              ),
              'package_name' => array(
                 'type' => 'VARCHAR',
                 'default' => 'NULL',
                 'constraint' => 50,
                 'null' => true,
              ),
              'package_details' => array(
                 'type' => 'longtext',
                 'null' => true,
              ),
               'monthly_fee' => array(
                 'type' => 'INT',
                 'constraint' => 11,
                 'default' => '0',
                 'null' => true,
              ),
               'annualy_fee' => array(
                 'type' => 'INT',
                 'constraint' => 11,
                 'default' => '0',
                 'null' => true,
              ),
              'status' => array(
                 'type' => "ENUM",
                 'constraint' => array('active','inactive'),
                 'default' => 'active',
                 'null' => true,
              ),
              'monthly_currency_name' => array(
                 'type' => 'VARCHAR',
                 'default' => 'NULL',
                 'constraint' => 50,
                 'null' => true,
              ),
              'annual_currency_name' => array(
                 'type' => 'VARCHAR',
                 'default' => 'NULL',
                 'constraint' => 50,
                 'null' => true,
              ),
             
             'date_created TIMESTAMP DEFAULT CURRENT_TIMESTAMP',
              'who' => array(
                 'type' => 'VARCHAR',
                 'default' => 'Admin',
                 'constraint' => 15,
                 'null' => true,
              ),
               'cuurency_id' => array(
                 'type' => 'INT',
                 'constraint' => 11,
                 'default' => '0',
                 'null' => true,
              ),
           )
        );

        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('package_content');
    }

    public function payment(){
      $this->dbforge->add_field(
           array(
                'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => true,
                'auto_increment' => true
              ),
              'payer_email' => array(
                 'type' => 'VARCHAR',
                 'default' => 'NULL',
                 'constraint' => 200,
                 'null' => true,
              ),
              'payer_id' => array(
                 'type' => 'VARCHAR',
                 'default' => 'NULL',
                 'constraint' => 200,
                 'null' => true,
              ),
              'payer_status' => array(
                 'type' => 'VARCHAR',
                 'default' => 'NULL',
                 'constraint' => 15,
                 'null' => true,
              ),
              'first_name' => array(
                 'type' => 'VARCHAR',
                 'default' => 'NULL',
                 'constraint' => 100,
                 'null' => true,
              ),
              'last_name' => array(
                 'type' => 'VARCHAR',
                 'default' => 'NULL',
                 'constraint' => 100,
                 'null' => true,
              ),
              'address_name' => array(
                 'type' => 'VARCHAR',
                 'default' => 'NULL',
                 'constraint' => 100,
                 'null' => true,
              ),
              'address_street' => array(
                 'type' => 'VARCHAR',
                 'default' => 'NULL',
                 'constraint' => 100,
                 'null' => true,
              ),
              'address_city' => array(
                 'type' => 'VARCHAR',
                 'default' => 'NULL',
                 'constraint' => 100,
                 'null' => true,
              ),
              'address_state' => array(
                 'type' => 'VARCHAR',
                 'default' => 'NULL',
                 'constraint' => 100,
                 'null' => true,
              ),
              'address_country_code' => array(
                 'type' => 'VARCHAR',
                 'default' => 'NULL',
                 'constraint' => 100,
                 'null' => true,
              ),
              'address_zip' => array(
                 'type' => 'INT',
                 'default' => '0',
                 'constraint' => 10,
                 'null' => true,
              ),
              'residence_country' => array(
                 'type' => 'VARCHAR',
                 'default' => 'NULL',
                 'constraint' => 100,
                 'null' => true,
              ),
              'txn_id' => array(
                 'type' => 'VARCHAR',
                 'default' => 'NULL',
                 'constraint' => 100,
                 'null' => true,
              ),
              'mc_currency' => array(
                 'type' => 'VARCHAR',
                 'default' => 'NULL',
                 'constraint' => 100,
                 'null' => true,
              ),
              'mc_gross' => array(
                 'type' => 'VARCHAR',
                 'default' => 'NULL',
                 'constraint' => 100,
                 'null' => true,
              ),
              'protection_eligibility' => array(
                 'type' => 'VARCHAR',
                 'default' => 'NULL',
                 'constraint' => 100,
                 'null' => true,
              ),
              'payment_gross' => array(
                 'type' => 'VARCHAR',
                 'default' => 'NULL',
                 'constraint' => 100,
                 'null' => true,
              ),
              'payment_status' => array(
                 'type' => 'VARCHAR',
                 'default' => 'NULL',
                 'constraint' => 100,
                 'null' => true,
              ),
              'update_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP',
              'pending_reason' => array(
                 'type' => 'VARCHAR',
                 'default' => 'NULL',
                 'constraint' => 255,
                 'null' => true,
              ),
              'payment_type' => array(
                 'type' => 'VARCHAR',
                 'default' => 'NULL',
                 'constraint' => 50,
                 'null' => true,
              ),
              'item_name' => array(
                 'type' => 'VARCHAR',
                 'default' => 'NULL',
                 'constraint' => 50,
                 'null' => true,
              ),
              'item_number' => array(
                 'type' => 'VARCHAR',
                 'default' => 'NULL',
                 'constraint' => 50,
                 'null' => true,
              ),
              'quantity' => array(
                 'type' => 'INT',
                 'default' => '0',
                 'constraint' => 20,
                 'null' => true,
              ),
              'txn_type' => array(
                 'type' => 'VARCHAR',
                 'default' => 'NULL',
                 'constraint' => 50,
                 'null' => true,
              ),
              'payment_date' => array(
                 'type' => 'VARCHAR',
                 'default' => 'NULL',
                 'constraint' => 50,
                 'null' => true,
              ),
              'business' => array(
                 'type' => 'VARCHAR',
                 'default' => 'NULL',
                 'constraint' => 50,
                 'null' => true,
              ),
              'notify_version' => array(
                 'type' => 'VARCHAR',
                 'default' => 'NULL',
                 'constraint' => 50,
                 'null' => true,
              ),
              'custom' => array(
                 'type' => 'VARCHAR',
                 'default' => 'NULL',
                 'constraint' => 100,
                 'null' => true,
              ),
              'verify_sign' => array(
                 'type' => 'VARCHAR',
                 'default' => 'NULL',
                 'constraint' => 255,
                 'null' => true,
              ),
           )
        );

        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('payment');
    }

    public function phone_code(){
      $this->dbforge->add_field(
           array(
                'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => true,
                'auto_increment' => true
              ),
              'iso' => array(
                 'type' => 'CHAR',
                 'constraint' => 3,
                 'null' => true,
              ),
              'name' => array(
                 'type' => 'VARCHAR',
                 'default' => 'NULL',
                 'constraint' => 50,
                 'null' => true,
              ),
              'nicename' => array(
                 'type' => 'VARCHAR',
                 'default' => 'NULL',
                 'constraint' => 50,
                 'null' => true,
              ),
              'iso3' => array(
                 'type' => 'CHAR',
                 'constraint' => 3,
                 'null' => true,
              ),
               'numcode' => array(
                 'type' => 'INT',
                 'constraint' => 11,
                 'default' => '0',
                 'null' => true,
              ),
               'phonecode' => array(
                 'type' => 'INT',
                 'constraint' => 11,
                 'default' => '0',
                 'null' => true,
              ),
           )
        );

        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('phone_code');
    }

    public function states(){
      $this->dbforge->add_field(
           array(
                'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => true,
                'auto_increment' => true
              ),
              'name' => array(
                 'type' => 'VARCHAR',
                 'default' => 'NULL',
                 'constraint' => 50,
                 'null' => true,
              ),
               'country_id' => array(
                 'type' => 'INT',
                 'constraint' => 11,
                 'default' => '1',
                 'null' => true,
              ),
           )
        );

        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('states');
    }

    public function static_pages(){
      $this->dbforge->add_field(
           array(
                'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => true,
                'auto_increment' => true
              ),
              'name' => array(
                 'type' => 'VARCHAR',
                 'default' => 'NULL',
                 'constraint' => 200,
                 'null' => true,
              ),
              'label' => array(
                 'type' => 'VARCHAR',
                 'default' => 'NULL',
                 'constraint' => 200,
                 'null' => true,
              ),
              'content' => array(
                 'type' => 'longtext',
                 'null' => true,
              ),
              'url_text' => array(
                 'type' => 'VARCHAR',
                 'default' => 'NULL',
                 'constraint' => 200,
                 'null' => true,
              ),
              'link' => array(
                 'type' => 'VARCHAR',
                 'default' => 'NULL',
                 'constraint' => 200,
                 'null' => true,
              ),
              'date TIMESTAMP DEFAULT CURRENT_TIMESTAMP',
              'who' => array(
                 'type' => 'VARCHAR',
                 'default' => 'NULL',
                 'constraint' => 200,
                 'null' => true,
              ),
           )
        );

        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('static_pages');
    }

    public function user(){
      $this->dbforge->add_field(
           array(
                'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => true,
                'auto_increment' => true
              ),
              'type' => array(
                 'type' => 'INT',
                 'default' => '0',
                 'constraint' => 5,
                 'null' => true,
              ),
              'username' => array(
                 'type' => 'VARCHAR',
                 'constraint' => 50,
                 'null' => true,
              ),
              'password' => array(
                 'type' => 'VARCHAR',
                 'default' => 'NULL',
                 'constraint' => 50,
                 'null' => true,
              ),
              'last_name' => array(
                 'type' => 'VARCHAR',
                 'constraint' => 100,
                 'null' => true,
              ),
              'first_name' => array(
                 'type' => 'VARCHAR',
                 'constraint' => 100,
                 'null' => true,
              ),
              'state' => array(
                 'type' => 'VARCHAR',
                 'constraint' => 100,
                 'null' => true,
              ),
              'business_name' => array(
                 'type' => 'VARCHAR',
                 'constraint' => 100,
                 'null' => true,
              ),
              'credit_license' => array(
                 'type' => 'VARCHAR',
                 'constraint' => 100,
                 'null' => true,
              ),
              'business_specification' => array(
                 'type' => 'VARCHAR',
                 'constraint' => 100,
                 'null' => true,
              ),
              'address' => array(
                 'type' => 'VARCHAR',
                 'constraint' => 200,
                 'null' => true,
              ),
              
              'date TIMESTAMP DEFAULT CURRENT_TIMESTAMP',
              'payment_status' => array(
                 'type' => 'INT',
                 'default' => '0',
                 'constraint' => 4,
                 'null' => true,
              ),
              'paid_amout' => array(
                 'type' => 'INT',
                 'default' => '0',
                 'constraint' => 15,
                 'null' => true,
              ),
              'title' => array(
                 'type' => 'VARCHAR',
                 'constraint' => 200,
                 'null' => true,
              ),
              'aggregator' => array(
                 'type' => 'VARCHAR',
                 'constraint' => 50,
                 'null' => true,
              ),
              'business_qualification' => array(
                 'type' => 'longtext',
                 'null' => true,
              ),
              'biography' => array(
                 'type' => 'longtext',
                 'null' => true,
              ),
              'business_addess_1' => array(
                 'type' => 'VARCHAR',
                 'constraint' => 200,
                 'null' => true,
              ),
              'business_addess_2' => array(
                 'type' => 'VARCHAR',
                 'default' => 'N/A',
                 'constraint' => 200,
                 'null' => true,
              ),
              'state' => array(
                 'type' => 'VARCHAR',
                 'default' => 'NULL',
                 'constraint' => 110,
                 'null' => true,
              ),
              'suburb' => array(
                 'type' => 'VARCHAR',
                 'constraint' => 110,
                 'null' => true,
              ),
              'post_code' => array(
                 'type' => 'VARCHAR',
                 'constraint' => 200,
                 'null' => true,
              ),
              'country' => array(
                 'type' => 'VARCHAR',
                 'default' => 'NULL',
                 'constraint' => 200,
                 'null' => true,
              ),
              'office' => array(
                 'type' => 'VARCHAR',
                 'constraint' => 200,
                 'null' => true,
              ),
              'website' => array(
                 'type' => 'VARCHAR',
                 'constraint' => 200,
                 'null' => true,
              ),
              'mobile' => array(
                 'type' => 'VARCHAR',
                 'constraint' => 200,
                 'null' => true,
              ),
              'fax' => array(
                 'type' => 'VARCHAR',
                 'default' => 'N/A',
                 'constraint' => 50,
                 'null' => true,
              ),
              'fb' => array(
                 'type' => 'VARCHAR',
                 'default' => 'N/A',
                 'constraint' => 50,
                 'null' => true,
              ),
              'tw' => array(
                 'type' => 'VARCHAR',
                 'default' => 'N/A',
                 'constraint' => 50,
                 'null' => true,
              ),
              'ln' => array(
                 'type' => 'VARCHAR',
                 'default' => 'N/A',
                 'constraint' => 50,
                 'null' => true,
              ),
              'status' => array(
                 'type' => 'INT',
                 'default' => '0',
                 'constraint' => 4,
                 'null' => true,
              ),
              'status' => array(
                 'type' => "ENUM",
                 'constraint' => array('0','1'),
                 'default' => '0',
                 'null' => true,
              ),
              'subscribed_packege_type' => array(
                 'type' => "ENUM",
                 'constraint' => array('1','12','0'),
                 'default' => '0',
                 'null' => true,
              ),
              'subscribed_packege_id' => array(
                 'type' => "INT",
                 'constraint' => 11,
                 'default' => '0',
                 'null' => true,
              ),
              'delete_status' => array(
                 'type' => "ENUM",
                 'constraint' => array('0','1'),
                 'default' => '0',
                 'null' => true,
              ),
              'block_status' => array(
                 'type' => "ENUM",
                 'constraint' => array('0','1'),
                 'default' => '0',
                 'null' => true,
              ),
              'household_income' => array(
                 'type' => 'INT',
                 'default' => '0',
                 'constraint' => 15,
                 'null' => true,
              ),
              'age' => array(
                 'type' => 'INT',
                 'default' => '18',
                 'constraint' => 5,
                 'null' => true,
              ),
              'broker_status' => array(
                 'type' => 'VARCHAR',
                 'default' => 'I am Busy',
                 'constraint' => 200,
                 'null' => true,
              ),
              'family_members' => array(
                 'type' => 'VARCHAR',
                 'default' => 'N/A',
                 'constraint' => 100,
                 'null' => true,
              ),
              'home' => array(
                 'type' => 'VARCHAR',
                 'default' => 'NULL',
                 'constraint' => 25,
                 'null' => true,
              ),
              'phone_code' => array(
                 'type' => 'VARCHAR',
                 'default' => 'NULL',
                 'constraint' => 5,
                 'null' => true,
              ),
              'profile_img_path' => array(
                 'type' => 'VARCHAR',
                 'default' => 'NULL',
                 'constraint' =>255,
                 'null' => true,
              ),
           )
        );

        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('user');
    }

    public function user_details(){
      $this->dbforge->add_field(
           array(
                'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => true,
                'auto_increment' => true
              ),
              'user_id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'null' => true,
              ),
              'title' => array(
                 'type' => 'VARCHAR',
                 'default' => 'NULL',
                 'constraint' => 50,
                 'null' => true,
              ),
              'aggregator' => array(
                 'type' => 'VARCHAR',
                 'constraint' => 50,
                 'null' => true,
              ),
              'business_qualification' => array(
                 'type' => 'longtext',
                 'null' => true,
              ),
              'biography' => array(
                 'type' => 'longtext',
                 'null' => true,
              ),
              'business_addess_1' => array(
                 'type' => 'VARCHAR',
                 'default' => 'NULL',
                 'constraint' => 200,
                 'null' => true,
              ),
              'business_addess_2' => array(
                 'type' => 'VARCHAR',
                 'default' => 'NULL',
                 'constraint' => 200,
                 'null' => true,
              ),
              'state' => array(
                 'type' => 'VARCHAR',
                 'default' => 'NULL',
                 'constraint' => 110,
                 'null' => true,
              ),
              'suburb' => array(
                 'type' => 'VARCHAR',
                 'default' => 'NULL',
                 'constraint' => 110,
                 'null' => true,
              ),
              'post_code' => array(
                 'type' => 'VARCHAR',
                 'default' => 'NULL',
                 'constraint' => 200,
                 'null' => true,
              ),
              'country' => array(
                 'type' => 'VARCHAR',
                 'default' => 'NULL',
                 'constraint' => 200,
                 'null' => true,
              ),
              'office' => array(
                 'type' => 'VARCHAR',
                 'default' => 'NULL',
                 'constraint' => 200,
                 'null' => true,
              ),
              'website' => array(
                 'type' => 'VARCHAR',
                 'default' => 'NULL',
                 'constraint' => 200,
                 'null' => true,
              ),
              'mobile' => array(
                 'type' => 'VARCHAR',
                 'default' => 'NULL',
                 'constraint' => 200,
                 'null' => true,
              ),
              'fax' => array(
                 'type' => 'VARCHAR',
                 'default' => 'NULL',
                 'constraint' => 50,
                 'null' => true,
              ),
              'fb' => array(
                 'type' => 'VARCHAR',
                 'default' => 'NULL',
                 'constraint' => 50,
                 'null' => true,
              ),
              'tw' => array(
                 'type' => 'VARCHAR',
                 'default' => 'NULL',
                 'constraint' => 50,
                 'null' => true,
              ),
              'ln' => array(
                 'type' => 'VARCHAR',
                 'default' => 'NULL',
                 'constraint' => 50,
                 'null' => true,
              ),
              'status' => array(
                 'type' => 'INT',
                 'default' => '0',
                 'constraint' => 4,
                 'null' => true,
              ),
           )
        );

        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('user_details');
    }

    public function user_login(){
       $this->dbforge->add_field(
           array(
                'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => true,
                'auto_increment' => true
              ),
              'username' => array(
                 'type' => 'VARCHAR',
                 'default' => 'NULL',
                 'constraint' => 200,
                 'null' => true,
              ),
              'password' => array(
                 'type' => 'VARCHAR',
                 'default' => 'NULL',
                 'constraint' => 200,
                 'null' => true,
              ),
              'ip_info' => array(
                 'type' => 'VARCHAR',
                 'default' => 'NULL',
                 'constraint' => 200,
                 'null' => true,
              ),
              'user_agent' => array(
                 'type' => 'VARCHAR',
                 'default' => 'NULL',
                 'constraint' => 200,
                 'null' => true,
              ),
           )
        );

        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('user_login');
    }

    public function user_login(){
       $this->dbforge->add_field(
           array(
                'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => true,
                'auto_increment' => true
              ),
              'hiring_id' => array(
                 'type' => 'INT',
                 'constraint' => 11
              ),
              'blocked_by' => array(
                 'type' => 'ENUM',
                 'constraint' => array('0','1','2'),
                 'default' => '0'
              ),
              'unblock_by' => array(
                 'type' => 'ENUM',
                 'constraint' => array('0','1','2'),
                 'default' => '0'
              ),

              'updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP',
           )
        );

        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('user_login');
    }
}

