<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Customer_model extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}

	public function getCustomerByEmail($email){
		if(isset($email) && !empty($email)){
			$q = $this->db->get_where('user', array('address' => $email));
			if($q->num_rows() > 0){
				foreach(($q->result()) as $row){
					$data = $row;
				}
				return $data;
			}
		}
	}

	public function getCustomerById($id){
		if(isset($id) && !empty($id)){
			$q = $this->db->get_where('user', array('id' => $id));
			if($q->num_rows() > 0){
				foreach(($q->result()) as $row){
					$data = $row;
				}
				return $data;
			}
		}
	}

	public function addCustomer($data){
		if(isset($data) && !empty($data)){
			if($this->db->insert('user', $data)){
				return TRUE;
			}
		}
	}

	public function validateEmail($email){
		if(isset($email) && !empty($email)){
			$q = $this->db->get_where('user', array('address' => $email));
			if($q->num_rows() > 0){
				$data['message'] = 'Email address is already registered';
			}else{
				$data['message'] = 'fine';
			}
			return $data;
		}
	}

	public function getBrokerProfileByIdAndData($id){
		if(isset($id) && !empty($id)){
			$this->db->where(array('U.id' => $id, 'type' => '2')); 
			$this->db->join('broker_hiring as BH', 'BH.broker_id = '.$id, 'left');
			$this->db->from('user as U');
			$this->db->select('U.*,BH.id as chid,BH.customer_id,BH.broker_id,BH.status as bhstatus, BH.favourite,BH.block_status,BH.delete_status');
			$q = $this->db->get();
			if($q->num_rows() > 0){
				foreach(($q->result()) as $row){
					$data = $row;
				}
				return $data;
			}
		}
	}

	
	public function getNoOfUnreadChatsById($id){
		if(isset($id) && !empty($id)){
			$where_data = array('fb_id'=> $id, 'read_status' => '0');
			$this->db->select('*');
			$this->db->from('chat');
			$this->db->where($where_data);
			$num_results = $this->db->count_all_results();
				return $num_results;
		}
	}

	public function getBrokerAll(){
		$where_data = array('type' => '2');
		$this->db->select('U.*,bh.id as bhid,bh.status as bhstatus, bh.customer_id, bh.broker_id, bh.favourite,bh.block_status,bh.delete_status');
		$this->db->from('user as U');
		$this->db->join('broker_hiring as bh', 'bh.broker_id = U.id', 'left');
		$this->db->where($where_data);
		$q = $this->db->get();
		if($q->num_rows() > 0){
			foreach(($q->result()) as $row){
				$data[] = $row;
			}
			return $data;
		}
	}

	public function getStateById($id){
		$q = $this->db->get_where('states', array('id' => $id));
		if($q->num_rows() > 0){
			foreach(($q->result()) as $row){
				$data = $row;
			}
			return $data;
		}
	}
    public function getState(){
		$q = $this->db->get('states');
		if($q->num_rows() > 0){
			foreach(($q->result()) as $row){
				$data[] = $row;
			}
			return $data;
		}
	}
	

		public function getPhoneCodeAll(){
		$q = $this->db->get('phone_code');
		if($q->num_rows() > 0){
			foreach(($q->result()) as $row){
				$data[] = $row;
			}
			return $data;
		}
	}
	public function addCustomerDetails($data, $id){
		if(isset($data) && !empty($data)){
			if($this->db->update('user', $data, array('id' => $id))){
				return TRUE;
			}
		}
	}
	//=============== start by sushant 21 june 2017 ===========//
	public function ajax_get_broker_details_by_keyword($key){
		if(isset($key) && !empty($key)){
			$this->db->select('U.*,bh.id as bhid,bh.status as bhstatus, bh.customer_id, bh.broker_id, bh.favourite,bh.block_status,bh.delete_status');
			$this->db->from('user as U');
			$this->db->join('broker_hiring as bh', 'bh.broker_id = U.id', 'left');
			$this->db->where('type', 2);
			$this->db->like('U.first_name', $key);
			$this->db->or_like('U.last_name', $key);
			$this->db->order_by('U.first_name', 'asc');
			$q = $this->db->get();
			if($q->num_rows()>0){
				return $q->result();
			}
		}else{
			$this->db->select('U.*,bh.id as bhid,bh.status as bhstatus, bh.customer_id, bh.broker_id, bh.favourite,bh.block_status,bh.delete_status');
			$this->db->from('user as U');
			$this->db->join('broker_hiring as bh', 'bh.broker_id = U.id', 'left');
			$this->db->where('type', 2);
			$this->db->order_by('U.first_name', 'asc');
			$q = $this->db->get();
			if($q->num_rows()>0){
				return $q->result();
			}
		}
	}
	public function getZipcodeById($id){
		if(isset($id) && !empty($id)){
			$arr_criteria = array('id' => $id, 'type' => 3);
			$this->db->select('post_code');
			$this->db->from('user');
			$this->db->where($arr_criteria);
			$q = $this->db->get();
			if($q->num_rows()>0){
				foreach ($q->result() as $value) {
					$data = $value->post_code;
				}
				return $data;
			}
		}
	}
	//=============== end by sushant 21 june 2017 ===========//
	//=============== start by sushant 22 june 2017 ===========//
	public function add_contact_to_broker($wdata){
		$query = $this->db->get_where('broker_hiring', $wdata);
		if($query->num_rows()>0){
			$data = array('status' => '2');
			$this->db->update('broker_hiring', $data, $wdata);
			return 1;
		}else{
			$wdata['status'] = '2';
			$q = $this->db->insert('broker_hiring', $wdata);
			return $q;
		}
	}
	public function block_unblock_to_broker($wdata,$status){
		if($status == '0')
			$status = '1';
		else
			$status = '0';
		$query = $this->db->get_where('broker_hiring', $wdata);
		if($query->num_rows()>0){
			foreach ($query->result() as $value) {
				$hiring_id = $value->id;
			}
			if($status == '1')
				$blockdata = array('hiring_id' => $hiring_id, 'blocked_by' => '2');
			else
				$blockdata = array('hiring_id' => $hiring_id, 'unblock_by' => '2');
			$data = array('block_status' => $status);
			$this->db->update('broker_hiring', $data, $wdata);
			$this->db->insert('block_details', $blockdata);
			return 1;
		}else{
			$wdata['block_status'] = $status;
			$q = $this->db->insert('broker_hiring', $wdata);
			return $q;
		}
	}
	public function block_unblock_customer_by_id($bid,$id,$status){
		$data = array('block_status' => $status);
		$condition = array('customer_id' => $id, 'broker_id' => $bid);
		if($this->db->update('customer_hiring', $data, $condition)){
			return 1;
		}
		else{
			return 0;
		}
	}
	public function cancel_request($bid,$cid){
		$condition = array('broker_id' => $bid, 'customer_id' => $cid);
		$data = array('status' => '0');
		if($this->db->update('broker_hiring', $data, $condition)){
			return 1;
		}else{
			return 0;
		}
	}
	//=============== end by sushant 22 june 2017 ===========//

	public function addFavourit($broker_id, $customer_id){
		if(isset($broker_id) && !empty($broker_id)){
			if(isset($broker_id) && !empty($broker_id)){
				$q = $this->db->get_where('favorite', array('broker_id' => $broker_id, 'customer_id' => $customer_id));
				if($q->num_rows() > 0){
					foreach(($q->result()) as $row){
						$data_fav = $row;
					}
					if($data_fav->status == '0'){
						if($this->db->update('favorite', array('status' => '1'), array('id' => $data_fav->id))){
							$data = array('msg' => 'filling');
						}
					}else{
						if($this->db->update('favorite', array('status' => '0'), array('id' => $data_fav->id))){
							$data = array('msg' => 'blank');
						}
					}
				}else{
					if($this->db->insert('favorite', array('customer_id' => $customer_id, 'broker_id' => $broker_id, 'status' => '1'))){
						$data = array('msg' => 'filling');
					}
				}
				return $data;	
			}		
		}
	}

	public function getFavouritByBrokerId($broker_id, $customer_id){
		if(isset($broker_id) && !empty($broker_id)){
			if(isset($customer_id) && !empty($customer_id)){
				$q = $this->db->get_where('favorite', array('broker_id' => $broker_id, 'customer_id' => $customer_id));
				if($q->num_rows() > 0){
					foreach(($q->result()) as $row){
						$data = $row;
					}
					if($data->status == '1'){
						$data = array('msg' => 'fa fa-heart');
					}else{
						$data = array('msg' => 'fa fa-heart-o');
					}
				}else{
					$data = array('msg' => 'fa fa-heart-o');
				}
				return $data;
			}
		}
	}

	public function favouritCustomer($customer_id){
		if(isset($customer_id) && !empty($customer_id)){
			$q = $this->db->get_where('favorite', array('customer_id' => $customer_id));
			if($q->num_rows() > 0){
				foreach(($q->result()) as $row){
					$data[] = $row;
				}
				return $data;
			}
		}
	}

	public function getUserById($user_id){
		if(isset($user_id) && !empty($user_id)){
			$q = $this->db->get_where('user', array('id' => $user_id));
			if($q->num_rows() > 0){
				foreach(($q->result()) as $row){
					$data = $row;
				}
				return $data;
			}
		}
	}

	public function getUserByUsername($username){
		if(isset($username) && !empty($username)){
			$q = $this->db->get_where('user', array('username' => $username));
			if($q->num_rows() > 0){
				foreach(($q->result()) as $row){
					$data = $row;
				}
				return $data;
			}
		}
	}



}

/* End of file Admin_model.php */
/* Location: ./application/models/Admin_model.php */