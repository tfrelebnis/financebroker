<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_model extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}
	public function get_package_value(){
		
		$this->db->select('*');

		$query = $this->db->get('fb_package_content');

		return $query->result();

	}
	public function getPackageInfoById($id) {
		if (isset($id) && !empty($id)) {
			$q = $this->db->get_where('fb_package_content', array('id' => $id));
		    if ($q->num_rows() > 0) {
				foreach (($q->result()) as $row) {
					$data[] = $row;
				}
				return $data;
			}
		}
	}
    public function getCurrency(){
        $this->db->order_by("iso","asc");
       $q=$this->db->get('fb_currency');
       //echo $this->db->last_query(); exit();
       return $q->result();

    } 
    public function Updatepackage($data,$id){
       $this->db->where('id',$id);
       $q=$this->db->update('fb_package_content',$data);
       return $q;
    }

	public function addStaticPage($data){
		if(isset($data) && !empty($data)){
			if($this->db->insert('static_pages', $data)){
				return TRUE;
			}
			return FALSE;
		}
	}

	public function getStaticPageAll(){
		$q = $this->db->get('static_pages');
		if($q->num_rows() > 0){
			foreach(($q->result()) as $row){
				$data[] = $row;
			}
			return $data;
		}
	}

	public function deleteStaticPage($id){
		if(isset($id) && !empty($id)){
			if($this->db->delete('static_pages', array('id' => $id))){
			
				return TRUE;
			}
		}
	}

	public function getStaticPageById($id){
		if(isset($id) && !empty($id)){
			$q = $this->db->get_where('static_pages', array('id' => $id));
			if($q->num_rows() > 0){
				foreach(($q->result()) as $row){
					$data = $row;
				}
				return $data;
			}
		}
	}

	public function addUpdatePage($data, $id){
		if(isset($id) && !empty($id)){
			if($this->db->update('static_pages', $data, array('id' => $id))){
				return TRUE;
			}
			return FALSE;
		}
	}

	public function validateName($data){
		if(isset($data) && !empty($data)){
			$q = $this->db->get_where('static_pages', array('name' => $data['name']));
			if($q->num_rows() == 0){
				return TRUE;
			}
			return FALSE;
		}
	}

	public function validateLabel($data){
		if(isset($data) && !empty($data)){
			$q = $this->db->get_where('static_pages', array('label' => $data['label']));
			if($q->num_rows() == 0){
				return TRUE;
			}
			return FALSE;
		}
	}

}

/* End of file Admin_model.php */
/* Location: ./application/models/Admin_model.php */