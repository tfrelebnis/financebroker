<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth_model extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}

	public function login($data){
		$q = $this->db->get_where('user', array('username' => $data['username'], 'password' => $data['password']));
		if($q->num_rows() > 0){
			if($this->db->insert('user_login', $data)){
				return TRUE;
			}
		}
	}

	public function auth($data){
		if($data['login'] != 1){
			redirect(base_url('Welcome'),'refresh');
		}
	}

}

/* End of file Admin_model.php */
/* Location: ./application/models/Admin_model.php */