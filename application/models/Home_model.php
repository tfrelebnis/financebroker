<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home_model extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}

	public function addBroker($data){
		if(isset($data) && !empty($data)){
			if($this->db->insert('user', $data)){
				return TRUE;
			}
		return FALSE;	
		}
	}

	public function validateEmail($email){
		if(isset($email) && !empty($email)){
			$q = $this->db->get_where('user', array('address' => $email));
			if($q->num_rows() > 0){
				$data['message'] = 'Email address is already registered';
			}else{
				$data['message'] = 'fine';
			}
			return $data;
		}
	}

	public function validateBusinessName($bname){
		if(isset($bname) && !empty($bname)){
			$q = $this->db->get_where('user', array('business_name' => $bname));
			if($q->num_rows() > 0){
				$data['message'] = 'business Name is already registered';
			}else{
				$data['message'] = 'fine';
			}
			return $data;
		}
	}

	public function brokerLogin($data){
		if(isset($data) && !empty($data)){
			$q = $this->db->get_where('user', array('username' => $data['username'], 'password' => $data['password'], 'status' => '1'));
			
			if($q->num_rows() > 0){
				foreach(($q->result()) as $row){
					if($row->password != ''){
						$data = $row;
					}
				}
				return $data;
			}
			return FALSE;
		}
	}

	public function getBrokerProfileById($id){
		if(isset($id) && !empty($id)){
			$q = $this->db->get_where('user', array('id' => $id, 'type' => '2'));
			if($q->num_rows() > 0){
				foreach(($q->result()) as $row){
					$data = $row;
				}
				return $data;
			}
		}
	}

	public function getCustomerProfileById($id){
		if(isset($id) && !empty($id)){
			$q = $this->db->get_where('user', array('id' => $id, 'type' => '3'));
			if($q->num_rows() > 0){
				foreach(($q->result()) as $row){
					$data = $row;
				}
				return $data;
			}
		}
	}

	//============= coded by sushant on 14june2017 ===========//
	public function getCustomerProfileByIdAndData($id){
		if(isset($id) && !empty($id)){
			$this->db->where(array('U.id' => $id, 'type' => '3')); 
			//$this->db->join('broker_hiring as BH', 'BH.broker_id = '.$id, 'left');
			$this->db->join('customer_hiring as CH', 'CH.customer_id = '.$id, 'left');
			$this->db->from('user as U');
			$this->db->select('U.*,CH.id as chid,CH.customer_id,CH.broker_id,CH.status as chstatus, CH.favourite,CH.star_rate,CH.block_status,CH.delete_status');
			$q = $this->db->get();
			//echo $this->db->last_query();
			if($q->num_rows() > 0){
				foreach(($q->result()) as $row){
					$data = $row;
				}
				return $data;
			}
		}
	}
	public function getCustomerProfileByIdAndDataByBroker($id){
		if(isset($id) && !empty($id)){
			$this->db->where(array('U.id' => $id, 'type' => '3')); 
			$this->db->join('broker_hiring as CH', 'CH.customer_id = '.$id, 'left');
			$this->db->from('user as U');
			$this->db->select('U.*,CH.id as chid,CH.customer_id,CH.broker_id,CH.status as chstatus, CH.favourite,CH.block_status,CH.delete_status');
			$q = $this->db->get();
			// echo $this->db->last_query(); exit();
			if($q->num_rows() > 0){
				foreach(($q->result()) as $row){
					$data = $row;
				}
				return $data;
			}
		}
	}
	public function update_table_customer_hiring($id,$broker_id,$data){
		$condition = array('customer_id'=> $id, 'broker_id' => $broker_id);
		if($this->db->update('customer_hiring', $data, $condition)){
			return 1;
		}
		else{
			return 0;	
		}
	}
	public function update_table_broker_hiring($id,$broker_id,$data){
		$condition = array('customer_id'=> $id, 'broker_id' => $broker_id);
		if($this->db->update('broker_hiring', $data, $condition)){
			return 1;
		}
		else{
			return 0;	
		}
	}
	//============= end code by sushant on 14june2017 ===========//

	public function getCountriesAll(){
		$q = $this->db->get('countries');
		if($q->num_rows() > 0){
			foreach(($q->result()) as $row){
				$data[] = $row;
			}
			return $data;
		}
	}

	public function getCountryById($id){
		if(isset($id) && !empty($id)){
			$q = $this->db->get_where('countries', array('id' => $id));
			if($q->num_rows() > 0){
				foreach(($q->num_rows()) as $row){
					$data = $row;
				}
				return $data;
			}
		}
	}

	public function getStateByCountry($country_id){
		if(isset($country_id) && !empty($country_id)){
			$q = $this->db->get_where('states', array('country_id' => $country_id));
			if($q->num_rows() > 0){
				foreach(($q->result()) as $row){
					$data[] = $row;
				}
				return $data;
			}
		}
	}

	public function getUserById($user_id){
		if(isset($user_id) && !empty($user_id)){
			$q = $this->db->get_where('user', array('id' => $user_id));
			if($q->num_rows() > 0){
				foreach(($q->result()) as $row){
					$data = $row;
				}
				return $data;
			}
		}
	}

	public function addUserDetails($data, $id){
		if(isset($data) && !empty($data)){
			if($this->db->update('user', $data, array('id' => $id))){
				return TRUE;
			}
		}
	}

	public function getPackagesAll(){
		$q = $this->db->get('package_content');
		if($q->num_rows() > 0){
			foreach(($q->result()) as $row){
				$data[] = $row;
			}
			return $data;
		}
	}

	public function getStateAll(){
		$q = $this->db->get_where('states', array('country_id' => '13'));
		if($q->num_rows() > 0){
			foreach(($q->result()) as $row){
				$data[] = $row;
			}
			return $data;
		}
	}

	//============= coded by sushant on 14june2017 ===========//
	public function inactive_customer_by_id($id,$bid){
		$data = array('delete_status' => '1');
		$condition = array('customer_id'=>$id, 'broker_id'=>$bid);
		if($this->db->update('customer_hiring', $data, $condition)){
			return 1;
		}
		else{
			return 0;
		}
	}
	public function block_unblock_customer_by_id($bid,$id,$status){
		$data = array('block_status' => $status);
		$condition = array('customer_id' => $id, 'broker_id' => $bid);
		if($this->db->update('customer_hiring', $data, $condition)){
			$query = $this->db->get_where('customer_hiring', $condition);
			if($query->num_rows()>0){
				foreach ($query->result() as $value) {
					$hiring_id = $value->id;
				}
				if($status == '1')
					$blockdata = array('hiring_id' => $hiring_id, 'blocked_by' => '1');
				else
					$blockdata = array('hiring_id' => $hiring_id, 'unblock_by' => '1');
				$this->db->insert('block_details', $blockdata);
				return 1;
			}
			else
				return 0;
		}
	}
	//============= end coded by sushant on 14june2017 ===========//
	//============= end code by sushant on 16june2017 ===========//
	public function marks_as_favourite_change($value='',$cid='',$bid=''){
		// var_dump($value,$cid,$bid); exit();
		$condition = array('customer_id' => $cid, 'broker_id' => $bid);
		if($value == '1'){
			$data = array('favourite' => '0');
			if($this->db->update('customer_hiring', $data, $condition)){
				return 1;
			}else{
				return 0;
			}
		}else{
			$data = array('favourite' => '1');
			if($this->db->update('customer_hiring', $data, $condition)){
				return 1;
			}else{
				return 0;
			}
		}
	}
	public function request_for_rate($cid='',$bid=''){
		$condition = array('customer_id' => $cid, 'broker_id' => $bid);
		$data = array('star_rate' => '6');
		if($this->db->update('customer_hiring', $data, $condition)){
				return 1;
			}else{
				return 0;
			}
	}
	public function fb_account_deactivate_by_id($id,$data){
		// print_r($data); exit();
		$condition = array('id' => $id);
		$update_data = array('status' => '0');
		if($this->db->update('user', $update_data, $condition)){
			$return_value = $this->db->insert('deactivated_account', $data);
			return $return_value; 
		}
	}
	public function old_password_validate($password,$id){
		$arr_criteria = array('password' => urldecode($password), 'id' =>$id);
		$limit = 1;
		$q = $this->db->get_where('user', $arr_criteria, $limit);
		if($q->num_rows()>0){
			return 1;
		}else{
			return 0;
		}
	}
	public function change_password($data,$id){
		$condition = array('id' => $id);
		if($this->db->update('user', $data, $condition)){
			return 1;
		}
	}

	//============= end code by sushant on 16june2017 ===========//

	public function getChatAll(){
		$q = $this->db->get('chat');
		if($q->num_rows() > 0){
			foreach(($q->result()) as $row){
				$data[] =  $row;
			}
			return $data;
		}
	}

	public function addChat($data){
		$q = $this->db->get('chat');
		if($this->db->insert('chat', $data)){
			return TRUE;
		}
	}

	public function getCustomerAll(){
		$q = $this->db->get_where('user', array('type' => '3'));
		if($q->num_rows() > 0){
			foreach(($q->result()) as $row){
				$data[] =  $row;
			}
			return $data;
		}
	}

	public function getBrokerAll(){
		$q = $this->db->get_where('user', array('type' => '2'));
		if($q->num_rows() > 0){
			foreach(($q->result()) as $row){
				$data[] =  $row;
			}
			return $data;
		}
	} 

	//============= start code by sushant on 17june2017 ===========//
	public function ajax_validate_email($email){
		if(isset($email) && !empty($email)){
			$q = $this->db->get_where('user', array('address' => $email));
			// echo $this->db->last_query(); exit();
			if($q->num_rows() > 0){
				return 1;
			}else{
				return 0;
			}
		}
	}
	public function getNoOfUnreadChatsById($id){
		if(isset($id) && !empty($id)){
			$where_data = array('fb_id'=> $id, 'read_status' => '0');
			$this->db->select('*');
			$this->db->from('chat');
			$this->db->where($where_data);
			$num_results = $this->db->count_all_results();
				return $num_results;
		}
	}
	public function get_notification_details_by_id($id){
		$where_data = array('fb_id' => $id, 'read_status' => '0');
		$this->db->select('U.first_name as name, c_id, count(c_id) as total');
		$this->db->from('chat');
		$this->db->join('user as U', 'U.id = chat.c_id', 'left');
		$this->db->where($where_data);
		$this->db->group_by('c_id');
		$q = $this->db->get();
		if($q->num_rows()>0){
			return $q->result();
		}
	}
	//============= end code by sushant on 17june2017 ===========//
	public function getUsersById($fb_id){
		if(isset($fb_id) && !empty($fb_id)){
			$q = $this->db->get_where('user', array('id' => $fb_id));
			if($q->num_rows() > 0){
				foreach(($q->result()) as $row){
					$data = $row;
				}
				return $data;
			}
		}
	}

//============= start code by sushant on 19june2017 ===========//
public function get_subscribed_package_details($id){
	if (isset($id) && !empty($id)) {
		$this->db->select('U.subscribed_packege_type, PC.*');
		$this->db->from('user as U');
		$this->db->join('package_content as PC', 'PC.id = U.subscribed_packege_id', 'inner');
		$q = $this->db->get();
		if($q->num_rows()>0){
			return $q->result();
		}
	}
}
public function get_all_package_details(){
	$arr_criteria = array('status' => 'active');
	$q =  $this->db->get_where('package_content', $arr_criteria);
	if($q->num_rows()>0){
			return $q->result();
		}
}
public function update_subscription_of_broker($id, $data){
	$condition = array('id' => $id);
	if($this->db->update('user', $data, $condition)){
		return 1;
	}
	else{
		return 0;
	}
}
//============= end code by sushant on 19june2017 ===========//

	public function getSearch($closetome = '', $state_id = '', $post_code = '', $rating = ''){
		if(isset($state_id) && !empty($state_id)){
			$query = "SELECT * FROM `fb_user` WHERE post_code = '".$post_code."' OR state = '".$state_id."'";
		}
	}


//============= start code by sushant on 20june2017 ===========//
public function cancel_subscription_by_id($id){
	$data = array('subscribed_packege_id' => '', 'subscribed_packege_type' => '');
	$condition = array('id' => $id);
	if($this->db->update('user', $data, $condition)){
		return 1;
	}else{
		return 0;
	}
}
public function getPaymentStatusbyEmail($email){
	$this->db->select('update_date');
	$this->db->from('payment');
	$this->db->where('custom', $email);
	$this->db->order_by('update_date', 'desc');
	$this->db->limit(1);
	$q = $this->db->get();
	// echo $this->db->last_query(); exit();
	if($q->num_rows()>0){
		foreach (($q->result()) as $key) {
			$date = $key->update_date;
		}
		return $date;
	}
}
public function gethiringDetails($id){
	$arr_criteria = array('broker_id' => $id);
	$q = $this->db->get_where('customer_hiring', $arr_criteria);
	if($q->num_rows()>0){
		return 1;
	}else{
		return 0;
	}
}
//============= end code by sushant on 20june2017 ===========//

	public function getHiredCustomerByBrokerId($broker_id){
		if(isset($broker_id) && !empty($broker_id)){
			$q = $this->db->get_where('customer_hiring', array('broker_id' => $broker_id), 2);
			print_r($q->num_rows());
			if($q->num_rows() > 0){
				foreach(($q->result()) as $row){
					$data[] = $this->getUserById($row->customer_id);
				}
				return $data;
			}
		}
	}

	public function getUserChat($customer_id, $broker_id){
		if(isset($customer_id) && !empty($customer_id)){
			$q = $this->db->get_where('chat', array('fb_id' => $broker_id, 'c_id' => $customer_id));
			if($q->num_rows() > 0){
				foreach(($q->result()) as $row){
					$data[] = $row;
				}
				return $data;
			}
		}
	}

	public function getUserByUserName($username){
		if(isset($username) && !empty($username)){
			$q = $this->db->get_where('user', array('username' => $username));
			if($q->num_rows() > 0){
				foreach(($q->result()) as $row){
					$data = $row;
				}
				return $data;
			}
		}
	}

	public function getFavouriteCustomer($broker_id){
		if(isset($broker_id) && !empty($broker_id)){
			$q = $this->db->get_where('favorite', array('broker_id' => $broker_id), 2);
			if($q->num_rows() > 0){
				foreach(($q->result()) as $row){
					$data[] = $this->getUserById($row->customer_id);
				}
				return $data;
			}
		}
	}



	public function getStateById($id){
		$q = $this->db->get_where('states', array('id' => $id));
		if($q->num_rows() > 0){
			foreach(($q->result()) as $row){
				$data = $row;
			}
			return $data;
		}
	}

	public function getRequestById($broker_id){
		if(isset($broker_id) && !empty($broker_id)){
			$q = $this->db->get_where('broker_hiring', array('broker_id' => $broker_id));
			if($q->num_rows() > 0){
				foreach(($q->result()) as $row){
					$data[] = $this->getUserById($row->customer_id);
				}
				return $data;
			}
		}
	}

	public function getFavouritByBrokerId($broker_id, $customer_id){
		if(isset($broker_id) && !empty($broker_id)){
			if(isset($customer_id) && !empty($customer_id)){
				$q = $this->db->get_where('favorite', array('broker_id' => $broker_id, 'customer_id' => $customer_id));
				if($q->num_rows() > 0){
					foreach(($q->result()) as $row){
						$data = $row;
					}
					if($data->status == '1'){
						$data = array('msg' => 'fa fa-heart');
					}else{
						$data = array('msg' => 'fa fa-heart-o');
					}
				}else{
					$data = array('msg' => 'fa fa-heart-o');
				}
				return $data;
			}
		}
	}

	public function getNoCustomerById($broker_id){
		if(isset($broker_id) && !empty($broker_id)){
			$q = $this->db->get_where('customer_hiring', array('broker_id' => $broker_id));
			if($q->num_rows() > 0){
				return true;
			}else{
				return false;
			}
		}
	}



}


/* End of file Admin_model.php */
/* Location: ./application/models/Admin_model.php */