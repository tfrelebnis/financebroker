<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Broker_model extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}

	public function getFinanceProfileById($id){
		if(isset($id) && !empty($id)){
			$q = $this->db->get_where('user', array('id' => $id, 'type' => '3'));
			if($q->num_rows() > 0){
				foreach(($q->result()) as $row){
					$data = $row;
				}
				return $data;
			}
		}
	}

}

/* End of file Admin_model.php */
/* Location: ./application/models/Admin_model.php */