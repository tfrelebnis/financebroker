<table class="table" width="100%">
	      					<thead>
					  			<tr>
								    <th >Finance Broker</th>
								    <th >Status</th>
								    <th >Business Name</th>
								    <th >State,Suburb</th>
								    <th >Distance from you</th>
								    <th >Contact Status</th>
								    <th colspan="3">Options</th>
							  	</tr>
						  	</thead>
						  	<tbody>
						  		<?php 
						  			if(isset($broker) && !empty($broker)){
						  				foreach($broker as $row){
						  					$name = $row->first_name.' '.$row->last_name;
						  					$state = $this->Customer_model->getStateById($row->state);
						  					if(isset($state) && !empty($state)){
						  						$state_name = $state->name;
						  					}else{
						  						$state_name = '';
						  					}
						  				?>
									  	<tr id="row_<?=$row->id?>">
										    <td><span class="nam_ltr_circl"><?=strtoupper(substr($row->first_name, 0, 1))?><?=strtoupper(substr($row->last_name, 0, 1))?><span class="nam_ltr_avail"></span></span><a href="<?= base_url('Customer/broker_profile/').$row->id ?>">&nbsp;<?=$name?></a></td>
										    <td><?=$row->status?></td>
										    <td><?=$row->business_name?></td>
										    <td><?=$state_name?>, <?=$row->suburb?></td>
										    <td><?=$row->id+rand(10,100)?>KM</td>
										    <?php  if($row->bhstatus == '0' || $row->bhstatus == ''){
										    	echo '<td>Not Connected</td>';
										    	}else if($row->bhstatus == '1'){
										    		echo '<td>Connected</td>';
										    	}else{
										    		echo '<td>Pending contact request</td>';
										    	}
										    ?>
										    <?php if($row->bhstatus == '0' || $row->bhstatus == ''){ ?>
										    <td><button class="transparent_btn add_contact" style="cursor: pointer;"  id="<?=$row->id?>" onclick="addCOntact()"><span class="fa fa-plus-circle"></span> &nbsp;Add contact</button></td>
										    <?php }else{ ?>
										    <td><button class="transparent_btn" style="cursor: pointer;" id="delete_<?=$row->id?>" onclick="delrow(this.id)"><span class="fa fa-trash"></span> &nbsp;Delete contact</button></td>
										    <?php } ?>
										    <?php if($row->block_status == '0'){ ?>
										    <td><button class="transparent_btn block_unblock" style="cursor: pointer;"  id="<?=$row->id?>" value="<?=$row->block_status?>" onclick="blockContact()"><span class="fa fa-ban"></span> &nbsp;Block contact</button></td>
										    <?php }else{ ?>
										    	<td><button class="transparent_btn block_unblock" style="cursor: pointer;"  id="<?=$row->id?>" value="<?=$row->block_status?>" onclick="blockContact()"><span class='fa fa-chain-broken'></span> &nbsp;Unblock contact"</button></td>
										    <?php } ?>
										    <td><span class="fa fa-heart-o like" style="cursor: pointer;"></span></td>
								  		</tr>
								  		<?php
						  				}
						  		 }else{
						  			echo '<b>There is no broker in table</b>';
						  			}?>
						  		<!-- <tr>
								    <td><span class="nam_ltr_circl">RG</span><a href="#"> &nbsp;Rick Grimes</a></td>
								    <td>In a meeting</td>
								    <td>ABC Company</td>
								    <td>Victoria,Southbank</td>
								    <td>5KM</td>
								    <td>Not Connected</td>
								    <td><button type="button" class="transparent_btn"><span class="fa fa-plus-circle"></span> &nbsp;Add contact</button></td>
								    <td><button type="button" class="transparent_btn"><span class="fa fa-ban"></span> &nbsp;Block contact</button></td>
								    <td><span class="fa fa-heart"></span></td>
						  		</tr>
						  		<tr>
								    <td><span class="nam_ltr_circl">RG<span class="nam_ltr_avail"></span></span><a href="#"> &nbsp;Rick Grimes</a></td>
								    <td>In a meeting</td>
								    <td>ABC Company</td>
								    <td>Victoria,Southbank</td>
								    <td>5KM</td>
								    <td>Not Connected</td>
								    <td><button type="button" class="transparent_btn"><span class="fa fa-trash"></span> &nbsp;Delete contact</button></td>
								    <td><button type="button" class="transparent_btn"><span class="fa fa-ban"></span> &nbsp;Block contact</button></td>
								    <td><span class="fa fa-heart"></span></td>
						  		</tr>
						  		<tr>
								    <td><span class="nam_ltr_circl">RG<span class="nam_ltr_avail"></span></span><a href="#"> &nbsp;Rick Grimes</a></td>
								    <td>In a meeting</td>
								    <td>ABC Company</td>
								    <td>Victoria,Southbank</td>
								    <td>5KM</td>
								    <td>Not Connected</td>
								    <td></td>
								    <td><button type="button" class="transparent_btn"><span class="fa fa-chain-broken"></span> &nbsp;Unblock contact </button></td>
								    <td></td>
						  		</tr> -->
						  	</tbody>
						</table>


