<?php //print_r($broker_profile); exit(); ?>
<?php $data = json_encode(array("fav_id" => $broker_profile->favourite, "bid"=>$broker_profile->broker_id, "cid"=>$broker_profile->customer_id )); ?>

<section class="customer_prof_page">

  <div class="container">

    <div class="row">

      <div class='col-sm-12'>

        <h3 class="page_brdr_titl" >Broker's Profile</h3>

      </div>

    </div><br>

  </div>

  <div class="container">

    <div class="row custmr_prfil_cont_row">

      <div class='col-xs-8'><!-- ====start col-xs-8-->

        <div class="custmr_prfil_cont">

          <h4 class="user_nam_icn"><span class="nam_ltr_circl">JD</span> <?= $broker_profile->first_name.' '.$broker_profile->last_name?></h4>

          <ul class="user_nam_lst custmr_nam_lst">

            <li><span class="fa fa-envelope-o"></span>&nbsp;&nbsp;<a href="#"><?= $broker_profile->address ?></a></li>

            <li><span class="fa fa-phone"></span>&nbsp;&nbsp;<?= $broker_profile->office?></li>

            <li><span class="fa fa-mobile"></span>&nbsp;&nbsp;<?= $broker_profile->mobile ?></li>

          </ul>

        </div><!-- box-1 end -->

        <div class="custmr_prfil_cont text-right">

          <div class="locatn_left text-left">

            <h4><span class="fa fa-location-arrow"></span> Location</h4>

            <ul>

              <li><?= $state->name?>,</li>

              <li><?= 'Australia' ?>,<?= $broker_profile->post_code ?></li>

            </ul>

          </div>

          <div class="locatn_rght">

            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3558.91175033184!2d81.02128794988296!3d26.874544768120785!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x399be28f0cce77d1%3A0x9fe2f3b7aa902000!2sElebnis!5e0!3m2!1sen!2sin!4v1476166032955" width="100%" height="200" frameborder="0" style="border:0" allowfullscreen=""></iframe>

          </div>

        </div><!-- box 2 end -->

        <div class="custmr_prfil_cont">

          <h4><span class="fa fa-user"></span> Personal Information</h4>

          <p>

            <span class="col-sm-6">Age:</span>

            <span class="col-sm-6"><?= $broker_profile->age ?></span>

          </p>

          <p>

            <span class="col-sm-6">No. of Family Members:</span>

            <span class="col-sm-6">5</span>

          </p>

          <p>

            <span class="col-sm-6">Household Income:</span>

            <span class="col-sm-6">$50,000 - $74,999</span>

          </p>

          <br>

        </div>  

      </div><!-- ==end-col-xs-8 -->

      <div class='col-xs-4'><!-- start col-xs-4 -->

        <?php if($broker_profile->bhstatus == '2' && $broker_profile->block_status == '0'){ ?> 

        <div class="cust_pro_frght_cont text-center">

          <h2><span class="fa fa-user-plus"></span></h2>

          <p class='icon_blo_txt'>You have been <span class="blue_txt">INVITED TO CONNECT</span><br> with <?= $broker_profile->first_name; ?></p>

          <?php if($broker_profile->bhstatus == '0'){ ?>

          <a href="<?= base_url('Customer/add_contact_to_broker/').$broker_profile->broker_id ?>" class="delet_btn">

            <span class="fa fa-plus-circle"></span>

            <span>Add Contact</span>

          </a>
          <?php }else if($broker_profile->bhstatus == '2'){ ?>

            <a href="<?= base_url('Customer/cancel_request/').$broker_profile->broker_id ?>" class="delet_btn">

            <span class="fa fa-close"></span>

            <span>Cancel Request</span>

          <?php } ?>

          <a href="<?= base_url('Customer/block_unblock_broker_by_id/').$broker_profile->customer_id.'/'.$broker_profile->id.'/'.$broker_profile->block_status ?>" class="delet_btn">

            <span class="fa fa-ban"></span>

            <span id="" >Block contact</span>

          </a>

          <p></p>

        </div><!-- box-8 end -->
        <div class="cust_pro_frght_cont text-center">

          <p></p>

          <h5 class='icon_blo_txt text-left'>Mark as favourite <span class="<?=$favourit['msg']?> pull-right like" id="profile_<?=$id?>"></h5>

          <p></p>

        </div>

        <?php } ?>
        <?php
        if($broker_profile->bhstatus == '0' || $broker_profile->bhstatus == ''){ ?> 

        <div class="cust_pro_frght_cont text-center">

          <h2><span class="fa fa-chain-broken"></span></h2>

          <p class='icon_blo_txt'>You are <span class="red_txt">NOT CONNECTED</span><br>with <?= $broker_profile->first_name ;?></p>
          <?php if($broker_profile->block_status == '0'){ ?> 
          <a href="<?= base_url('Customer/add_contact_to_broker/').$broker_profile->broker_id ?>" class="delet_btn">

            <span class="fa fa-plus-circle"></span>

            <span>Add Contact</span>

          </a>
          <a href="<?= base_url('Customer/block_unblock_broker_by_id/').$this->session->userdata('profile_id').'/'.$broker_profile->id.'/'.$broker_profile->block_status ?>" class="delet_btn">

            <span class="fa fa-ban"></span>

            <span id="" >Block contact</span>

          </a>

          <?php }else{ ?>
          <a href="<?= base_url('Customer/block_unblock_broker_by_id/').$this->session->userdata('profile_id').'/'.$broker_profile->id.'/'.$broker_profile->block_status ?>" class="delet_btn">

            <span class="fa fa-chain-broken"></span>

            <span>Unblock contact</span>

          </a>
          <?php } ?>

          <p></p>

        </div>
        <div class="cust_pro_frght_cont text-center">

          <p></p>

          <h5 class='icon_blo_txt text-left'>Mark as favourite <span class="<?=$favourit['msg']?> pull-right like" id="profile_<?=$id?>"></h5>

          <p></p>

        </div>

        <?php }else{ ?>
        <?php if($broker_profile->block_status == '1'){ ?> 

        <div class="cust_pro_frght_cont text-center">

          <h2><span class="fa fa-ban"></span></h2>

          <p class='icon_blo_txt'>You have <span class="red_txt">BLOCKED</span><br> <?= $broker_profile->first_name.' '.$broker_profile->last_name ?></p>

          <a href="<?= base_url('Customer/block_unblock_broker_by_id/').$this->session->userdata('profile_id').'/'.$broker_profile->id.'/'.$broker_profile->block_status ?>" class="delet_btn">

            <span class="fa fa-chain-broken"></span>

            <span>Unblock contact</span>

          </a>

          <p></p>

        </div><!-- box-7 end  -->

        <?php }else{ ?>
        <?php if($broker_profile->bhstatus != '2'){ ?>
        <div class="cust_pro_frght_cont text-center">

          <h2><span class="fa fa-link"></span></h2>

          <p class='icon_blo_txt'>You are <span class="green_txt">CONNECTED</span><br> with <?= $broker_profile->first_name.' '.$broker_profile->last_name?></p>

          <?php if($broker_profile->delete_status == '1'){ ?>

          <!-- <button type="button" class="delet_btn"> -->

          <!-- <span class="delet_btn">Contact Deleted</span> -->

          <!-- </button> -->

          <?php  }else{ ?>

          <!-- <a href="#" class="delet_btn">

            <span class="fa fa-trash-o"></span>

            <span><?= $broker_profile->delete_status == '1' ? 'Contact Deleted' :  'Delete Contact'?></span>

          </a> -->
          <?php } ?>
          <a href="<?= base_url('Customer/block_unblock_broker_by_id/').$this->session->userdata('profile_id').'/'.$broker_profile->id.'/'.$broker_profile->block_status ?>" class="delet_btn">

            <span class="fa fa-ban"></span>

            <span><?= $broker_profile->block_status == '1' ? 'Unblock Contact' :  'Block Contact'?></span>

          </a>

          <p></p>

        </div><!-- box-1 end -->
        <!-- <div class="cust_pro_frght_cont text-center">

          <p></p>

          <h5 class='icon_blo_txt text-left'><?= $broker_profile->favourite == '1' ? 'Unmark' : 'Mark' ?> as favourite <span class="fa fa-star-o pull-right"><input type="checkbox" class="fav" value=<?= $data; ?> <?= $broker_profile->favourite == '1' ? 'checked' : 'unchecked' ?>></span></h5>

          <p></p>

        </div> -->

        <?php }}}?>

        <div class="cust_pro_frght_cont text-center">

          <h2><span class="fa fa-comment-o"></span></h2>

          <p class='icon_blo_txt'>You have <span class="red_txt">2 UNREAD MESSAGES</span><br> from <?= $broker_profile->first_name.' '.$broker_profile->last_name ?></p>

          <a href="<?=base_url('Home/my_fb_message')?>" type="button" class="btn">

            SEND MESSAGE

          </a>

          <p></p>

        </div><!-- box-2 end  -->

        <div class="cust_pro_frght_cont text-center">

          <h2><span class="fa fa-paperclip"></span></h2>

          <p class='icon_blo_txt'>You have received <span class="blue_txt">3 DOCUMENTS</span><br> FROM <?= $broker_profile->first_name.' '.$broker_profile->last_name ?></p>

          <a href="<?=base_url('Home/my_fb_files')?>" type="button" class="btn">

            SEND DOCUMENT

          </a>

          <p></p>

        </div><!-- box-3 end -->

        <div class="cust_pro_frght_cont text-center">

          <br>

          <div class='select_form_group'>

            <label class="text-left col-sm-12">Application Status</label>

            <div class="col-sm-12">

              <select class="form-control">

                <option>ENQUIRY</option>>

              </select>

            </div>

          </div>

          <div class="clearfix"></div>

          <br>

        </div><!-- box-4 end -->

       <!--  <div class="cust_pro_frght_cont text-center">

          <p></p>

          <p class="text-left"> <?= $broker_profile->first_name?>'s rating of you:</p>

          <?php if($broker_profile->star_rate == '0'){ ?>

          <h3 >NO RATING</h3>

          <a href="<?= base_url('Home/request_for_rate/').$broker_profile->customer_id.'/'.$broker_profile->broker_id?>" class="delet_btn">

            <span class="fa fa-pencil"></span>

            <span>Request for rating</span>

          </a>

          <?php }else if($broker_profile->star_rate == '6'){ ?>

            <h3 >NO RATING</h3>

            <span>Requested for rating</span>

          <?php } ?>

          <p></p>

        </div> --><!-- box-5 end -->

        <!-- <div class="cust_pro_frght_cont text-center">

          <p></p>
          
          <h5 class='icon_blo_txt text-left'><?= $broker_profile->favourite == '1' ? 'Unmark' : 'Mark' ?> as favourite <span class="fa fa-star-o pull-right"><input type="checkbox" class="fav" name="fav" value=<?= $data; ?> <?= $broker_profile->favourite == '1' ? 'checked' : 'unchecked' ?>></span></h5>

          <p></p>

        </div> --><!-- box-6 end -->
        <!-- <div class="cust_pro_frght_cont text-center">

          <h2><span class="fa fa-ban"></span></h2>

          <p class='icon_blo_txt'>You have <span class="red_txt">BLOCKED</span><br> John Doe</p>

          <button type="button" class="delet_btn">

            <span class="fa fa-chain-broken"></span>

            <span>Unblock contact</span>

          </button>

          <p></p>

        </div> --><!-- box-7 end  -->
        <!-- <div class="cust_pro_frght_cont text-center">

          <h2><span class="fa fa-user-plus"></span></h2>

          <p class='icon_blo_txt'>You have been <span class="blue_txt">INVITED TO CONNECT</span><br> with John Doe</p>

          <button type="button" class="delet_btn">

            <span class="fa fa-check-circle"></span>

            <span>Accept request</span>

          </button>
          <button type="button" class="delet_btn">

            <span class="fa fa-ban"></span>

            <span id="" >Block contact</span>

          </button>

          <p></p>

        </div> --><!-- box-8 end -->
        <!-- <div class="cust_pro_frght_cont text-center">

          <p></p>

          <h5 class='icon_blo_txt text-left'>Mark as favourite <span class="fa fa-heart-o pull-right"> </span></h5>

          <p></p>

        </div> --><!-- box-9 end -->
        <!-- <div class="cust_pro_frght_cont text-center">

          <h2><span class="fa fa-chain-broken"></span></h2>

          <p class='icon_blo_txt'>You are <span class="red_txt">NOT CONNECTED</span><br>with John Doe</p>

          <button type="button" class="delet_btn">

            <span class="fa fa-ban"></span>

            <span id="" >Block contact</span>

          </button>

          <p></p>

        </div> --><!-- box-10 end  -->

      </div><!-- end col-xs-4 -->

    </div>

  </div>

</section>




<script type="text/javascript">
  $(document).ready(function() {
  $('.like').click(function(event) {
    var broker = $(this).attr('id');
    var broker_id = broker.split('_')[1];
    var customer_id = "<?=$customer->id?>";

    $.ajax({
      url: "<?=base_url('Customer/favourit')?>",
      type: 'POST',
      dataType: 'json',
      data: {broker_id: broker_id, customer_id: customer_id},
    })
    .done(function(result) {
      console.log(result);
      if(result['msg'] == 'filling'){
        $('#'+broker).removeClass("fa-heart-o");
        $('#'+broker).addClass("fa-heart");
      }
      if(result['msg'] == 'blank'){
        $('#'+broker).removeClass("fa-heart");  
        $('#'+broker).addClass("fa-heart-o"); 
      }
    })
    .fail(function() {
      console.log("error");
      
    })
    .always(function() {
      console.log("complete");
    });
    
    
  }); 
});
</script>