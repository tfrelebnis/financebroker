<!DOCTYPE html>
<html lang="en">
<head>
	<title>Finance</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" type="text/css">
	<script src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="<?=base_url('assets/front/js/script.js')?>"></script>
	<link rel="stylesheet" href="<?=base_url('assets/front/css/style.css')?>">
</head>
<body>
<!-- nav -->
<div class="top_nav2">
	<nav class="navbar navbar-inverse">
	  <div class="container-fluid">
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span> 
	      </button>
	      <div class="navbar-brand">
	      	<a  href="#">
	      		<img src="<?=base_url('assets/front/img/home_logo_new_1.png')?>" width="100%">
	      	</a>
	      </div>
	    </div>
	    <div class="collapse navbar-collapse" id="myNavbar">
	      <!-- <ul class="nav navbar-nav">
	        <li class="active"><a href="#">Customer</a></li>
	        <li><a href="#">Finance Brokers</a></li>
	        <li><a href="#">Tools</a></li> 
	      </ul> -->
	      <ul class="nav navbar-nav navbar-right">
	        <li><a href="<?=//base_url('Home') ?>" ><span class="fa fa-home"></span>  Home	</a></li>
	        <li><a href="<?=base_url('Home/my_fb_files') ?>"> <span class="fa fa-file"></span>My Files</a></li>
	        <li><a href="<?=base_url('Home/my_fb_message') ?>"><span class="fa fa-comment"></span> Messing</a></li>
	        <li class="dropdown"><a href="#"  class="dropdown-toggle" data-toggle="dropdown">
	        	<span class="fa fa-bell-o"> <span class="bell_circle">3</span></span>Notifications</a>
	        	<ul class="dropdown-menu">
		          <li><a href="#">You have 2 unread message/s from Rick Grimes</a></li>
		          <li class="divider"></li>
		          <li><a href="#">Rick Grimes has sent you Passport.pdf</a></li>
		          <li class="divider"></li>
		          <li><a href="#">Rick Grimes has just rated you 7</a></li>
		        </ul>
	        </li>
	        <li class="dropdown">
	        	<a href="#" class="dropdown-toggle" data-toggle="dropdown"> 
	        		<span class="fa fa-user"></span>
	        		<?=$this->session->userdata('last_name')?>
	        		<span class="caret"></span>
	        	</a>
		        <ul class="dropdown-menu">
		          <li><a href="#">My Profile</a></li>
		          <li class="divider"></li>
		          <li><a href="#">Change Subscription</a></li>
		          <li class="divider"></li>
		          <li><a href="#">Cancel Subscription</a></li>
		          <li class="divider"></li>
		          <li><a href="#">Change Password</a></li>
		          <li class="divider"></li>
		          <li><a href="#">Deactivate Account</a></li>
		          <li class="divider"></li>
		          <li><a href="#">Log Out</a></li>
		        </ul>
	    	</li>
	      </ul>
	    </div>
	  </div>
	</nav>
</div>
<!-- nav -->
