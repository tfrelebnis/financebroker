<!DOCTYPE html>
<html lang="en">
<head>
	<title>Finance</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" type="text/css">
	<script src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
	<link rel="stylesheet" href="<?=base_url('assets/front/css/style.css')?>">
	<script type="text/javascript" src="<?=base_url('assets/front/js/script.js')?>"></script>
</head>
<body>
<!-- nav -->
<div class="top_nav">
	<nav class="navbar navbar-inverse">
	  <div class="container-fluid">
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span> 
	      </button>
	      <div class="navbar-brand">
	      	<a href="#">
	      		<img src="<?=base_url('assets/front/img/home_logo_new_1.png')?>" width="100%">
	      	</a>
	      </div>
	    </div>
	    <div class="collapse navbar-collapse" id="myNavbar">
	      <ul class="nav navbar-nav mobile_menu_bg">
	        <li class="active"><a href="#">How it Works</a></li>
	        <li  class="menu_search_box"><a href="#">Search Now</a>
		    	<!-- <span class="search_box_menu">
		        	<span class="fa fa-search"></span>
			        <input type="Search" placeholder="Search..." class="form-control" />       
			    </span> -->
	        </li> 
	        <li><a href="#"  data-toggle="modal" data-target="#finance_broker_hub">Finance Broker Hub</a></li>
	        <!-- <li ><a href="#">Customer</a></li>
	        <li><a href="#">Finance Brokers</a></li>
	        <li><a href="#">Tools</a></li>  -->
	        
	      </ul>
	      <ul class="nav navbar-nav navbar-right">
	        <li>
	        	<a href="#" class="btn" data-toggle="modal" data-target="#custopmer_broker_conf_mdl">
	        		<span class="glyphicon glyphicon-user"></span> Sign Up
	        	</a>
	        </li>
	        <li><a href="#" data-toggle="modal" data-target="#home_login"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
	      </ul>
	    </div>
	  </div>
	</nav>
</div>
<!-- nav -->
<?php 
    if(isset($message) && !empty($message)){
        echo '<div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                '.$message.'    
            </div>';
    }
    if(isset($warning) && !empty($warning)){
        echo '<div class="alert alert-warning alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                '.$warning.'    
            </div>';
    }
    if(isset($error) && !empty($error)){
        echo '<div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                '.$error.'    
            </div>';
    }
?>

