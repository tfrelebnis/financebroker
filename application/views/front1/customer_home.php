<?php //print_r($broker); exit(); ?>
<section class="customr_home_page">
  	<div class="container">
	    <div class="row">
	      	<div class='col-sm-12'>
		        <br>
		        	<h3 class="page_brdr_titl" >Search for a broker</h3>
		        <br>
	      	</div>
	    </div>
  	</div>
  	<div class="container">
	    <div class="row customr_home_row">
	        <div class='col-md-3 search_fin'><!-- start col-md-3 -->
	        	<div class="customer_left">
	        		<div class="custmr_left_boxs text-right">
		        		<div class="form_group">
		        			<h4>ALL RECENT</h4>
		        			<button type="button" class="btn">CLEAR ALL</button>
		        		</div>
		        	</div>
		        	<div class="custmr_left_boxs">
		        		<div class="form-group">
		        			<h4>All Finance Brokers in Victoria</h4>
		        			<div class="customr_rating_n_broker">
		        				<input  id="customr_rating" type="range" name="rangeInput" min="0%"  value="30%" max="100%" onchange="updateTextInput(this.value);">
		        				<span class="customr_rating_left">Less Than 10KM</span>
		        				<span id="customr_rating_curnt_val" class="customr_rating_curnt">30%</span>
		        				<span class="customr_rating_rght">Show All  (States)</span>
		        			</div>
		        		</div>
		        	</div>
		        	<div class="custmr_left_boxs">
		        		<div class="form-group">
		        			<h4>Customer Rating</h4>
		        			<div class="customr_rating_n_broker">
		        				<input id="customr_rating1"  type="range" min="10"  max="1000" step="10" value="300">
		        			</div>
		        		</div>
		        	</div>
		        	<div class="custmr_left_boxs">
		        		<h4>Specialisation</h4>
		        		<div class="checkbox checkbox-primary">
		        			<input type="checkbox" checked id="spl_chk_box">
		        			<label for="spl_chk_box">
		        				Home Loans
		        			</label>
		        		</div>
		        		<div class="checkbox checkbox-primary">
		        			<input type="checkbox" checked id="spl_chk_box2">
		        			<label for="spl_chk_box2">
		        				Commercial Finance
		        			</label>
		        		</div>
		        		<div class="checkbox checkbox-primary">
		        			<input type="checkbox" checked id="spl_chk_box3">
		        			<label for="spl_chk_box3">
		        				Asset Finance
		        			</label>
		        		</div>
		        		<div class="checkbox checkbox-primary">
		        			<input type="checkbox" checked id="spl_chk_box4">
		        			<label for="spl_chk_box4">
		        				Personal Loans
		        			</label>
		        		</div>
		        		<div class="checkbox checkbox-primary">
		        			<input type="checkbox" checked id="spl_chk_box5">
		        			<label for="spl_chk_box5">
		        				Debtor Finance/Invoice Discounting
		        			</label>
		        		</div>
		        		<div class="checkbox checkbox-primary">
		        			<input type="checkbox" checked id="spl_chk_box6">
		        			<label for="spl_chk_box6">
		        				Reverse Mortgages
		        			</label>
		        		</div>
		        		<div class="checkbox checkbox-primary">
		        			<input type="checkbox" checked id="spl_chk_box7">
		        			<label for="spl_chk_box7">
		        				Self Managed Super Fund  Borrowing
		        			</label>
		        		</div>
		        	</div>
		        </div>
	      	</div><!-- col-md-3 end -->
	      	<div class='col-md-9'><!-- col-md-9 start -->
	      		<div class="customer_right">
	      			<div class="row search_bx_sorting text-right">
	      				<div class="col-xs-7  form-group">
	      					<input type="text" name="search" id="broker_search" placeholder="Search for a finance broker" class="form-control">
		      			</div>
		      			<div class="col-xs-5 form-group text-right">
		      				<div class="sort_opt_drop">
		      					<label class="col-xs-5 text-center">SORT BY</label>
		      					<div class="col-xs-7">
		      						<select class="form-control" id="sortby">
		      							<option value="">Select</option>
		      							<option value="1">Proximity(Closest to Farthest)</option>
		      							<option value="2">Proximity(Farthest to Closest)</option>
		      							<option value="3">Rating(High to Low)</option>
		      							<option value="4">Rating(Low to High)</option>
		      						</select>
		      					</div>
		      				</div>
		      			</div>
	      			</div>
	      			<div class="table-responsive broker_ajax">
	      				<table class="table" width="100%">
	      					<thead>
					  			<tr>
								    <th >Finance Broker</th>
								    <th >Status</th>
								    <th >Business Name</th>
								    <th >State,Suburb</th>
								    <th >Distance from you</th>
								    <th >Contact Status</th>
								    <th colspan="3">Options</th>
							  	</tr>
						  	</thead>
						  	<tbody>
						  		<?php 
						  			if(isset($broker) && !empty($broker)){
						  				foreach($broker as $row){
						  					$name = $row->first_name.' '.$row->last_name;
						  					$state = $this->Customer_model->getStateById($row->state);
						  					if(isset($state) && !empty($state)){
						  						$state_name = $state->name;
						  					}else{
						  						$state_name = '';
						  					}
						  					$favourit = $this->Customer_model->getFavouritByBrokerId($row->id, $customer_id);
						  				?>
									  	<tr id="row_<?=$row->id?>">
										    <td><span class="nam_ltr_circl"><?=strtoupper(substr($row->first_name, 0, 1))?><?=strtoupper(substr($row->last_name, 0, 1))?><span class="nam_ltr_avail"></span></span><a href="<?= base_url('Customer/broker_profile/').$row->id ?>">&nbsp;<?=$name?></a></td>
										    <td><?=$row->status?></td>
										    <td><?=$row->business_name?></td>
										    <td><?=$state_name?>, <?=$row->suburb?></td>
										    <td><?=$row->id+rand(10,100)?>KM</td>

										    <?php  if($row->bhstatus == '0' || $row->bhstatus == ''){
										    	echo '<td>Not Connected</td>';
										    	}else if($row->bhstatus == '1'){
										    		echo '<td>Connected</td>';
										    	}else{
										    		echo '<td>Pending contact request</td>';
										    	}
										    ?>
										    <?php if($row->bhstatus == '0' || $row->bhstatus == ''){ ?>
										    <td><button class="transparent_btn add_contact" style="cursor: pointer;"  id="<?=$row->id?>"><span class="fa fa-plus-circle"></span> &nbsp;Add contact</button></td>
										    <?php }else{ ?>
										    <td><button class="transparent_btn" style="cursor: pointer;"  id="delete_<?=$row->id?>" onclick="delrow(this.id)"><span class="fa fa-trash"></span> &nbsp;Delete contact</button></td>
										    <?php } ?>
										    <?php if($row->block_status == '0'){ ?>
										    <td><button class="transparent_btn block_unblock" style="cursor: pointer;"  id="<?=$row->id?>" value="<?=$row->block_status?>"><span class="fa fa-ban"></span> &nbsp;Block contact</button></td>
										    <?php }else{ ?>
										    	<td><button class="transparent_btn block_unblock" style="cursor: pointer;"  id="<?=$row->id?>" value="<?=$row->block_status?>"><span class='fa fa-chain-broken'></span> &nbsp;Unblock contact"</button></td>
										    <?php } ?>
										    <td><span class="<?=$favourit['msg']?> like" style="cursor: pointer;" id="like_<?=$row->id?>"></span></td>
								  		</tr>
								  		<?php
						  				}
						  		 }else{
						  			echo '<b>There is no broker in table</b>';
						  			}?>
						  		<!-- <tr>
								    <td><span class="nam_ltr_circl">RG</span><a href="#"> &nbsp;Rick Grimes</a></td>
								    <td>In a meeting</td>
								    <td>ABC Company</td>
								    <td>Victoria,Southbank</td>
								    <td>5KM</td>
								    <td>Not Connected</td>
								    <td><button type="button" class="transparent_btn"><span class="fa fa-plus-circle"></span> &nbsp;Add contact</button></td>
								    <td><button type="button" class="transparent_btn"><span class="fa fa-ban"></span> &nbsp;Block contact</button></td>
								    <td><span class="fa fa-heart"></span></td>
						  		</tr>
						  		<tr>
								    <td><span class="nam_ltr_circl">RG<span class="nam_ltr_avail"></span></span><a href="#"> &nbsp;Rick Grimes</a></td>
								    <td>In a meeting</td>
								    <td>ABC Company</td>
								    <td>Victoria,Southbank</td>
								    <td>5KM</td>
								    <td>Not Connected</td>
								    <td><button type="button" class="transparent_btn"><span class="fa fa-trash"></span> &nbsp;Delete contact</button></td>
								    <td><button type="button" class="transparent_btn"><span class="fa fa-ban"></span> &nbsp;Block contact</button></td>
								    <td><span class="fa fa-heart"></span></td>
						  		</tr>
						  		<tr>
								    <td><span class="nam_ltr_circl">RG<span class="nam_ltr_avail"></span></span><a href="#"> &nbsp;Rick Grimes</a></td>
								    <td>In a meeting</td>
								    <td>ABC Company</td>
								    <td>Victoria,Southbank</td>
								    <td>5KM</td>
								    <td>Not Connected</td>
								    <td></td>
								    <td><button type="button" class="transparent_btn"><span class="fa fa-chain-broken"></span> &nbsp;Unblock contact </button></td>
								    <td></td>
						  		</tr> -->
						  	</tbody>
						</table>
	      			</div>
	      		</div>
	      	</div><!-- col-md-9 end -->
      	</div>
  	</div>
</section>
<script type="text/javascript">
// function addBroker(id){
// 	// alert(id); return false;

// 	document.getElementById(id).innerHTML = "<span class='fa fa-trash'></span> &nbsp;Delete contact";
// }
$(document).ready(function(e){
	 $('.add_contact').on('click', function() {
		var bid =  $(this).attr('id');
		// alert(bid); return false;
		 $.ajax({
                    type: 'POST',
                    async: false,
                    url: "<?= base_url('Customer/ajax_add_contact_to_broker/')?>"+bid,             
                    success: function(result){
                    	if(result == 1){
                        	location.reload();
                        }
                    }, 
                    error: function(e){
                        console.log(e);
                    }
            });
	 });
});

// function blockBroker(id){
// 	document.getElementById(id).innerHTML = "<span class='fa fa-chain-broken'></span> &nbsp;Unblock contact";
// }
$(document).ready(function(e){
	 $('.block_unblock').on('click', function() {
		var bid =  $(this).attr('id');
		var status =  $(this).val();
		// alert(status); return false;
		 $.ajax({
                    type: 'POST',
                    async: false,
                    url: "<?= base_url('Customer/ajax_block_unblock_to_broker/')?>"+bid+"/"+status,             
                    success: function(result){
                        if(result == 1){
                        	location.reload();
                        }
                    }, 
                    error: function(e){
                        console.log(e);
                    }
            });
	 });
});


function updateTextInput(val) {
    var x = val;
    document.getElementById("customr_rating_curnt_val").innerHTML = x+'%';
    document.getElementById("customr_rating_curnt_val").style.left=x+'%';
}
$(document).ready(function() {
	$('.like').click(function(event) {
		var broker = $(this).attr('id');
		var broker_id = broker.split('_')[1];
		var customer_id = "<?=$customer_id?>";

		$.ajax({
			url: "<?=base_url('Customer/favourit')?>",
			type: 'POST',
			dataType: 'json',
			data: {broker_id: broker_id, customer_id: customer_id},
		})
		.done(function(result) {
			console.log(result);
			if(result['msg'] == 'filling'){
				$('#'+broker).removeClass("fa-heart-o");
				$('#'+broker).addClass("fa-heart");
			}
			if(result['msg'] == 'blank'){
				$('#'+broker).removeClass("fa-heart");	
				$('#'+broker).addClass("fa-heart-o");	
			}
		})
		.fail(function() {
			console.log("error");
			
		})
		.always(function() {
			console.log("complete");
		});
		
		
	});	
});
$(document).ready(function() {
	$('#broker_search').keyup(function(){
		var val = $(this).val();
		var sort_val = $('#sortby').val();
		// alert(sort_val); return false;
		$.ajax({
			type: "POST",
			url: "<?= base_url('Customer/search_broker_by_name/')?>"+val+'/'+sort_val,
			success: function(data){
			$(".broker_ajax").html(data);
			},
		});
	});
});

function delrow(delid){
	var id = delid.split('_')[1];
	$('#row_'+id).remove();
}

function addCOntact(){
	$('.add_contact').on('click', function() {
		var bid =  $(this).attr('id');
		// alert(bid); return false;
		 $.ajax({
                    type: 'POST',
                    async: false,
                    url: "<?= base_url('Customer/ajax_add_contact_to_broker/')?>"+bid,             
                    success: function(result){
                    	if(result == 1){
                        	location.reload();
                        }
                    }, 
                    error: function(e){
                        console.log(e);
                    }
            });
	 });
}

function blockContact(){
	$('.block_unblock').on('click', function() {
		var bid =  $(this).attr('id');
		var status =  $(this).val();
		// alert(status); return false;
		 $.ajax({
                    type: 'POST',
                    async: false,
                    url: "<?= base_url('Customer/ajax_block_unblock_to_broker/')?>"+bid+"/"+status,             
                    success: function(result){
                        if(result == 1){
                        	location.reload();
                        }
                    }, 
                    error: function(e){
                        console.log(e);
                    }
            });
	 });
}

</script>