<?php
include_once 'header1.php';
?>
<section class="my_profile_page_sec">
  <div class="container">
    <div class="row">
        <h3 class="page_brdr_titl">My Profile</h3>
    </div>
    <br>
    <div class="row my_profile_contnt_row row-eq-height">
      <div class="col-lg-8">
        <form class="left_my_profil form-horizontal"><!-- start form -->
          <div class="row my_prof_box my_prof_box1"><!-- box1 start -->
            <div class="col-sm-6">
              <div class="form-group">
                <label for="mp_lName"  class="control-label col-xs-5">Last Name<span class="red_txt">*</span>:</label>
                <div class="col-xs-7">
                  <input id="mp_lName"  type="text" class="form-control"  placeholder="Please Enter Last Name">
                </div>
              </div>
              <div class="form-group">
                <label for="mp_fname"  class="control-label col-xs-5">First Name<span class="red_txt">*</span>:</label>
                <div class="col-xs-7">
                  <input id="mp_fname"  type="text" class="form-control" placeholder="Please Enter First Name ">
                </div>
              </div>
              <div class="form-group">
                <label for="mp_title"  class="control-label col-xs-5">Title<span class="red_txt">*</span>:</label>
                <div class="col-xs-7">
                  <input id="mp_title"  type="text" class="form-control" placeholder="Please Enter Title">
                </div>
              </div>
              <div class="form-group">
                <label  class="control-label col-xs-5">Aggregator<span class="red_txt">*</span>:</label>
                <div class="col-xs-7">
                  <select class="form-control" >
                    <option value="-1">Please select Aggregator</option>
                    <option >1</option>
                    <option>2</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label for="mp_qualification"  class=" col-xs-12"> Business Qualification<span class="red_txt">*</span>:</label>
                <div class="col-xs-12">
                  <textarea class="form-control" id="mp_qualification" placeholder="Please Enter Your Business Qualification" name=""></textarea>
                </div>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label for="mp_bsns_name"  class="control-label col-xs-5">Business Name<span class="red_txt">*</span>:</label>
                <div class="col-xs-7">
                  <input id="mp_bsns_name"  type="text" class="form-control" placeholder="Please Enter Business Name">
                </div>
              </div>
              <div class="form-group">
                <label for="mp_cred"  class="control-label col-xs-5">Credit Licence Number<span class="red_txt">*</span>:</label>
                <div class="col-xs-7">
                  <input id="mp_cred"  type="text" class="form-control" placeholder="Please Enter Credit Licence Number">
                </div>
              </div>
              <div class="form-group">
                <label  class="control-label col-xs-5">Business Specilisations<span class="red_txt">*</span>:</label>
                <div class="col-xs-7">
                  <select class="form-control" >
                    <option value="-1">Please Enter Business Specilisations</option>
                    <option >1</option>
                    <option>2</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label for="mp_prof_bio"  class="col-xs-12"> Profile/Biography<span class="red_txt">*</span>:</label>
                <div class="col-xs-12">
                  <textarea class="form-control" id="mp_prof_bio" placeholder="Please tell us something about who are you" name=""></textarea>
                </div>
              </div>
            </div>
          </div><!-- box1 end -->
          <br>
          <div class="row my_prof_box my_prof_box2"><!-- box2 start -->
            <div class="col-sm-6">
              <div class="form-group">
                <label for="mp_add_lin1"  class="control-label col-xs-5">Business Address<span class="red_txt">*</span>:</label>
                <div class="col-xs-7">
                  <input id="mp_add_lin1"  type="text" class="form-control" placeholder="Address Line 1">
                </div>
              </div>
              <div class="form-group">
                <label for="mp_add_lin2"  class="control-label col-xs-5"></label>
                <div class="col-xs-7">
                  <input id="mp_add_lin2"  type="text" class="form-control" placeholder="Address Line 2">
                </div>
              </div>
              <div class="form-group">
                <label for="mp_sate"  class="control-label col-xs-5">State<span class="red_txt">*</span>:</label>
                <div class="col-xs-7">
                  <input id="mp_sate"  type="text" class="form-control" placeholder="Please Enter State">
                </div>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label for="mp_suburb"  class="control-label col-xs-5">Suburb<span class="red_txt">*</span>:</label>
                <div class="col-xs-7">
                  <input id="mp_suburb"  type="text" class="form-control" placeholder="Please Enter Suburb">
                </div>
              </div>
              <div class="form-group">
                <label for="mp_pst_cod"  class="control-label col-xs-5">Post Code<span class="red_txt">*</span>:</label>
                <div class="col-xs-7">
                  <input id="mp_pst_cod"  type="text" class="form-control" placeholder="Please Enter Post Code">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-xs-5">Country<span class="red_txt">*</span>:</label>
                <div class="col-xs-7">
                  <select class="form-control" >
                    <option value="-1">Country</option>
                    <option>1</option>
                    <option>2</option>
                  </select>
                </div>
              </div>
            </div>
          </div><!-- box2 end -->
            <br>
          <div class="row my_prof_box my_prof_box3"><!-- box3 start -->
            <div class="col-sm-6">
              <div class="form-group">
                <label for="mp_em_addd"  class="control-label col-xs-5">Email Address<span class="red_txt">*</span>:</label>
                <div class="col-xs-7">
                  <input id="mp_em_addd"  type="text" class="form-control" placeholder="Please Enter Email Address">
                </div>
              </div>
              <div class="form-group">
                <label for="mp_websit"  class="control-label col-xs-5">Website<span class="red_txt">*</span>:</label>
                <div class="col-xs-7">
                  <input id="mp_websit"  type="text" class="form-control" placeholder="Please Enter Website">
                </div>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label for="mp_offce"  class="control-label col-xs-5">Office<span class="red_txt">*</span>:</label>
                <div class="col-xs-7">
                  <input id="mp_offce"  type="text" class="form-control" placeholder="Please enter your office no">
                </div>
              </div>
              <div class="form-group">
                <label for="mp_mobil"  class="control-label col-xs-5">Mobile<span class="red_txt">*</span>:</label>
                <div class="col-xs-7">
                  <input id="mp_mobil"  type="text" class="form-control" placeholder="Please enter your mobile no">
                </div>
              </div>
              <div class="form-group">
                <label for="mp_fax"  class="control-label col-xs-5">Fax<span class="red_txt">*</span>:</label>
                <div class="col-xs-7">
                  <input id="mp_fax"  type="text" class="form-control" placeholder="Please enter your fax no">
                </div>
              </div>
            </div>
          </div><!-- box3 end -->
                    <br>
          <div class="row my_prof_box my_prof_box4"><!-- box4 start -->
            <div class="col-sm-6">
              <div class="form-group">
                <label for="mp_facb"  class="control-label col-xs-5">Facebook:</label>
                <div class="col-xs-7">
                  <input id="mp_facb"  type="text" class="form-control" placeholder="Please enter your facebook URl">
                </div>
              </div>
              <div class="form-group">
                <label for="mp_lnkdin"  class="control-label col-xs-5">Linkedin:</label>
                <div class="col-xs-7">
                  <input id="mp_lnkdin"  type="text" class="form-control" placeholder="Please enter your Linkedin URl">
                </div>
              </div>
              <div class="form-group">
                <label for="mp_twittr"  class="control-label col-xs-5">Twitter:</label>
                <div class="col-xs-7">
                  <input id="mp_twittr"  type="text" class="form-control" placeholder="Please enter your Twitter URl">
                </div>
              </div>
            </div>
            <div class="col-sm-6">
              <!-- content -->
            </div>
          </div><!-- box4 end -->
          <br>
          <div class="row text-center">
             <button type="button"  class="btn">SAVE CHANGES</button>
          </div>
        </form><!-- close-form -->
        <br>
      </div>
      <div class="col-lg-4">
        <form class="rght_my_profil">
          <div class="row my_prof_rght_box my_prof_rght_box2"><!-- right_box2 start-->
            <div class="col-sm-12 text-center">
              <div class="not_visible_sontnt">
                <p class="fa fa-check "></p>
                <p>Your profile is currently</p>
                <h2 class="text-center green_txt">VISIBLE</h2>
                <p class="text-left">Your current subscription.</p>
                <div class="pkg_cntnt">
                  <h3>NORMAL PACKAGE</h3>
                  <P></P>
                </div>
                <div class="pkg_cntnt text-center">
                  <div class="pkg_month">
                    <h5>MONTHLY</h5>
                    <h2>$22/month</h2>
                    <p>inclusive of GST</p>
                    <!-- <button type="button" class="btn">SUBSCRIBE</button> -->
                  </div>
                  <div class="pkg_annual cancl_change">
                    <button type="button"  class="btn">CANCEL SUBSCRIPTION</button>
                    <button type="button"  class="btn">CHANGE SUBSCRIPTION</button>
                  </div>
                </div>
                <P class="text-left">Lorem Ipsum is simply dummy text</p>
                <br>
              </div>
            </div>
          </div><!-- right_box1 start-->
          <br>
        </form>
      </div><!-- col-sm-4 right end -->
    </div>
  </div>
</section>
<?php
include_once 'footer.php';
?>

