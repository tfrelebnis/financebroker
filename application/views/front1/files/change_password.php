
<section class="change_pass_page">
  	<div class="container">
	    <div class="row">
	      	<div class='col-sm-12'>
		        <br>
		        	<h3 class="page_brdr_titl" >Change Password </h3>
		        <br>
	      	</div>
	    </div>
  	</div>

    <div class="container">
    	<div class="row">
      		<div class='col-sm-12'>
      			<div class="change_pass_form">
	      			<form class="form-horizontal">
	      				<div class="form-group">
	      					<label for="ch_ps_old" class="control-label col-sm-4">Old Password<span class="red_txt">*</span></label>
	      					<div class="col-sm-8">
	      						<input type="text" class="form-control" id="ch_ps_old" placeholder="Please enter your password">
	      					</div>
	      				</div>
	      				<div class="form-group">
	      					<label for="ch_ps_new" class="control-label col-sm-4">New Password<span class="red_txt">*</span></label>
	      					<div class="col-sm-8">
	      						<input type="text" class="form-control" id="ch_ps_new" placeholder="Password must be minimum of 5 characters">
	      					</div>
	      				</div>
	      				<div class="form-group">
	      					<label for="ch_ps_retyp" class="control-label col-sm-4">Retype Password<span class="red_txt">*</span></label>
	      					<div class="col-sm-8">
	      						<input type="text" class="form-control" id="ch_ps_retyp" placeholder="Please retype your password">
	      					</div>
	      				</div>
	      				<div class="form-group">
	      					<div class="col-sm-offset-4 col-sm-8">
	      						<input type="submit" class="btn" value="Save Changes">
	      					</div>
	      				</div>
	      			</form>
	      		</div>
      		</div>
	    </div>
  	</div>
</section>