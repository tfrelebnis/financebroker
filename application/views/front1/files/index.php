  <section class="home_slidr_sec text-center">
    <div id="myCarousel" class="carousel slide carousel-fade" data-ride="carousel">
      <!-- Wrapper for slides -->
      <div class="carousel-inner">
        <div class="item active">
          <div class="slid_imags"></div>
          <!-- <img src="img/1.jpg" alt=""> -->
          <!-- <div class="carousel-caption">
              <h2>SHYAM RAJ VERMA</h2>
          </div> -->
        </div>

        <div class="item">
          <div class="slid_imags"></div>
          <!-- <img src="img/2.jpg" alt=""> -->
          <!-- <div class="carousel-caption">
              <h2>SHYAM RAJ VERMA</h2>
          </div> -->
        </div>
      </div>

      <!-- Left and right controls -->
      <a class="left carousel-control" href="#myCarousel" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#myCarousel" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
    <!-- ================ -->
  </section>
  <section class="how_to_sec">
    <div class="container">
      <div class="row">
        <div class='col-sm-12 '>
          <h2 class="text-center">How It Works</h2>
          <p>
            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
          </p>
          <p>
            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
          </p>
        </div>
      </div>
        <br><br>
      <div class="row">
        <div class="col-sm-4">
          <div class="how_to_contnt text-center">
              <img src="<?=base_url('assets/front/img/tax-form.jpg')?>" alt="400px × 267px1" class="image-bottom-space">
              <h4><a href="#">Financial Consultant</a></h4>
              <p class="content-top-space">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
          </div>
        </div>
        <div class="col-sm-4">
          <div class="how_to_contnt text-center">
              <img src="<?=base_url('assets/front/img/finance-report.jpg')?>" alt="400px × 267px1" class="image-bottom-space">
              <h4><a href="#">Financial Consultant</a></h4>
              <p class="content-top-space">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
          </div>
        </div>
        <div class="col-sm-4">
          <div class="how_to_contnt text-center">
              <img src="<?=base_url('assets/front/img/filling-tax-form.jpg')?>" alt="400px × 267px1" class="image-bottom-space">
              <h4><a href="#">Financial Consultant</a></h4>
              <p class="content-top-space">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
          </div>
        </div>
      </div>
    </div>
  </section>


