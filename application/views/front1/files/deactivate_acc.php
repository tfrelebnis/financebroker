
<section class="change_pass_page">
  	<div class="container">
	    <div class="row">
	      	<div class='col-sm-12'>
		        <br>
		        	<h3 class="page_brdr_titl" >Deactivate My Account</h3>
		        <br>
	      	</div>
	    </div>
  	</div>

    <div class="container">
    	<div class="row">
      		<div class='col-sm-12'>
      			<div class="change_pass_form deact_acc_form">
	      			<form class="form-horizontal">
	      				<div class="form-group">
	      					<label for="ch_ps_old" class="control-label col-sm-12">We are sorry you had to deactivate your account.Please let us know reason for deactivation<span class="red_txt">*</span></label>
	      				</div>
	      				<div class="form-group">
	      					<div class="col-sm-offset-1 col-sm-11">
		      					<div class="checkbox">
								  <label for="deact_acc_chk_bx"><input type="checkbox" id="deact_acc_chk_bx">Reason 1 text</label>
								</div>
							</div>
	      				</div>
	      				<div class="form-group">
	      					<div class="col-sm-offset-1 col-sm-11">
		      					<div class="checkbox">
								  <label for="deact_acc_chk_bx2"><input type="checkbox" id="deact_acc_chk_bx2">Reason 2 text</label>
								</div>
							</div>
	      				</div>
	      				<div class="form-group">
	      					<div class="col-sm-offset-1 col-sm-11">
		      					<div class="checkbox">
								  <label for="deact_acc_chk_bx3"><input type="checkbox" id="deact_acc_chk_bx3">Reason 3 text</label>
								</div>
							</div>
	      				</div>
	      				<div class="form-group">
	      					<div class="col-sm-offset-1 col-sm-11">
		      					<div class="checkbox">
								  <label for="deact_acc_chk_bx4"><input type="checkbox" id="deact_acc_chk_bx4">Reason 4 text</label>
								</div>
							</div>
	      				</div>
	      				<div class="form-group">
	      					<div class="col-sm-offset-1 col-sm-11">
		      					<div class="checkbox">
								  <label for="deact_acc_chk_bx5"><input type="checkbox" id="deact_acc_chk_bx5">Other:</label>
								</div>
							</div>
	      				</div>
	      				<div class="form-group">
	      					<div class="col-sm-offset-1 col-sm-11">
		      					<input type="text" class="form-control" >
							</div>
	      				</div>
	      				<div class="form-group">
	      					<div class="col-sm-offset-4 col-sm-8">
	      						<input type="submit" class="btn" value="DEACTIVATE MY ACCOUNT">
	      					</div>
	      				</div>
	      			</form>
	      		</div>
      		</div>
	    </div>
  	</div>
</section>