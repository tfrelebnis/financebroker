
<section class="my_fb_msg_page">
  <div class="container">
    <div class="row">
      <div class='col-sm-12'>
        <br>
        <h3 class="page_brdr_titl text-right" ><span>My Messages</span> <small><span>Your Status: <span class="green_txt"> Available </span><span class="fa fa-cog"></span></span></small></h3>
      </div>
    </div><br>
  </div>
  <div class="container">
    <div class="row msg_fb_cont_row">
      <div class='col-xs-4'><!-- start col-xs-4 -->
        <div class="msg_fb_srch_contnt">
          <h4>ALL RECENT</h4>
          <div class=" msg_fb_srch_icn">
            <span class="fa fa-search"></span>
            <input type="text" class="form-control" placeholder="Search my contacts">
          </div>
          <hr>
          <div class="messag_list">
            <p class="fb_msg_hed"><span class="nam_ltr_circl">RG<span class="nam_ltr_avail"></span></span><span class="fb_user_name">Rick Grines - In a meeting</span> <span class="pull-right"><small>Yesterday</small></span></p>
            <p class="fb_msg_txt">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
          </div>
          <div class="messag_list">
            <p class="fb_msg_hed"><span class="nam_ltr_circl">RG<span class="nam_ltr_avail"></span></span><span class="fb_user_name">Rick Grines - In a meeting</span> <span class="pull-right"><small>Yesterday</small></span></p>
            <p class="fb_msg_txt">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
          </div>
          <br>
          <br><br><br>
        </div>
      </div><!-- end col-xs-4 -->
      <div class='col-xs-8 rght_msg_fb_cntnt'><!-- ====start col-xs-8-->
        <div class="rght_msg_fb_detail">
          <p><span class="nam_ltr_circl">RG<span class="nam_ltr_avail"></span></span><span  class="fb_user_name">Rick Grines - In a meeting</span></p> 
          <p class="rght_msg_fb_date">April 17, 2017(Monday)</p>
          <div class="messag_list active">
            <p class="fb_msg_hed"><span class="nam_ltr_circl">RG<span class="nam_ltr_avail"></span></span><span class="fb_user_name">Rick Grines - In a meeting</span> <span class="pull-right"><small>7:25 AM</small></span></p>
            <p class="fb_msg_txt">Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
          </div>
          <div class="messag_list">
            <p class="fb_msg_hed"><span class="nam_ltr_circl">RG<span class="nam_ltr_avail"></span></span><span class="fb_user_name">Rick Grines - In a meeting</span> <span class="pull-right"><small>7:25 AM</small></span></p>
            <p class="fb_msg_txt">Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
          </div>
          <div class="form-group send_file_msgs">
            <div class="input-group fb_edit_attach_send_btn_cntnt">
              <div class="fb_edit_attach_send_btn">
                <div class="input-group">
                  <span class="input-group-btn">
                    <button class="btn btn-secondary" type="button"><span class="fa fa-pencil"></span></button>
                  </span>
                  <input type="text" class="form-control" placeholder="Product name">
                  <span class="input-group-btn">
                    <button class="btn btn-secondary" type="button"><span class="fa fa-paperclip"></span></button>
                  </span>
                </div>
              </div>
              <div class="input-group-btn fb_edt_attch_send">
                <button type="button" class="btn ">Send</button>
              </div>
            </div>
          </div>
        </div>
      </div><!-- ==end-col-xs-8 -->
    </div>
  </div>
</section>
