<section class="change_subscription_page">
  	<div class="container">
	    <div class="row">
	      	<div class='col-sm-12'>
		        <br>
		        	<h3 class="page_brdr_titl" >Change Subscription</h3>
		        <br>
	      	</div>
	    </div>
  	</div>
  	<div class="container ">
	    <div class="row change_subscription_row">
	      	<div class='col-sm-3'>
	      		<div class="not_visible_sontnt left_ch_subscription text-center">
	      			<br>
					<p class="fa fa-check "></p>
					<p >Your current subscription.</p>
					<h3>NORMAL PACKAGE</h3>
	      			<div class="ch_month_box">
	      				<h4>MONTHLY</h4>
	      				<h3>$22/month</h3>
	      				<p><small>Inclusive of GST</small></p>
	      			</div>
	      			<br>
	      		</div>
	      	</div>
	      	<div class='col-sm-9'>
	      		<div class="right_ch_subscription text-center">
	      			<br>
	      			<p>You may upgrade your subscription to:</p>
	      			<div class="feature_n_pkg_box">
		                <div class="pkg_cntnt">
		                  <h3>NORMAL PACKAGE</h3>
		                  <P>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</P>
		                </div>
		                <div class="pkg_cntnt text-center">
		                  <div class="pkg_month">
		                    <h5>MONTHLY</h5>
		                    <h3>$22/month</h3>
		                    <p><small>inclusive of GST</small></p>
		                    <h5 class="green_txt">CURRENT SUBSCRIPTION</h5>
		                  </div>
		                  <div class="pkg_annual cancl_change">
		                  	<h5>ANNUAL</h5>
		                  	<h3>$220/year</h3>
		                  	<p><small>inclusive of GST</small></p>
		                    <button type="button"  class="btn">SUBSCRIBE</button>
		                  </div>
		                </div>
	                </div>
		            <div class="feature_n_pkg_box">
		                <div class="pkg_cntnt">
		                  <h3>FEATURED PACKAGE</h3>
		                  <P>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</P>
		                </div>
		                <div class="pkg_cntnt text-center cancl_change">
		                  <div class="pkg_month">
		                    <h5>MONTHLY</h5>
		                    <h3>$55/month</h3>
		                    <p><small>inclusive of GST</small></p>
		                    <button type="button" class="btn">SUBSCRIBE</button>
		                  </div>
		                  <div class="pkg_annual cancl_change">
		                  	<h5>ANNUAL</h5>
		                  	<h3>$550/year</h3>
		                  	<p><small>inclusive of GST</small></p>
		                    <button type="button"  class="btn">SUBSCRIBE</button>
		                  </div>
		                </div>
                		<br>
	      			</div>
	      		</div>
	      	</div>
      	</div>
  	</div>
</section>