
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>Add Static Page
            </h1>
        </div>
        <!-- END PAGE TITLE -->
        <!-- BEGIN PAGE TOOLBAR -->
        <div class="page-toolbar">
            <!-- BEGIN THEME PANEL -->
            <div class="btn-group btn-theme-panel">
                <a href="javascript:;" class="btn dropdown-toggle" data-toggle="dropdown">
                    <i class="icon-settings"></i>
                </a>
                <div class="dropdown-menu theme-panel pull-right dropdown-custom hold-on-click">
                    <div class="row">
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <h3>HEADER</h3>
                            <ul class="theme-colors">
                                <li class="theme-color theme-color-default active" data-theme="default">
                                    <span class="theme-color-view"></span>
                                    <span class="theme-color-name">Dark Header</span>
                                </li>
                                <li class="theme-color theme-color-light " data-theme="light">
                                    <span class="theme-color-view"></span>
                                    <span class="theme-color-name">Light Header</span>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-12 seperator">
                            <h3>LAYOUT</h3>
                            <ul class="theme-settings">
                                <li> Theme Style
                                    <select class="layout-style-option form-control input-small input-sm">
                                        <option value="square">Square corners</option>
                                        <option value="rounded" selected="selected">Rounded corners</option>
                                    </select>
                                </li>
                                <li> Layout
                                    <select class="layout-option form-control input-small input-sm">
                                        <option value="fluid" selected="selected">Fluid</option>
                                        <option value="boxed">Boxed</option>
                                    </select>
                                </li>
                                <li> Header
                                    <select class="page-header-option form-control input-small input-sm">
                                        <option value="fixed" selected="selected">Fixed</option>
                                        <option value="default">Default</option>
                                    </select>
                                </li>
                                <li> Top Dropdowns
                                    <select class="page-header-top-dropdown-style-option form-control input-small input-sm">
                                        <option value="light">Light</option>
                                        <option value="dark" selected="selected">Dark</option>
                                    </select>
                                </li>
                                <li> Sidebar Mode
                                    <select class="sidebar-option form-control input-small input-sm">
                                        <option value="fixed">Fixed</option>
                                        <option value="default" selected="selected">Default</option>
                                    </select>
                                </li>
                                <li> Sidebar Menu
                                    <select class="sidebar-menu-option form-control input-small input-sm">
                                        <option value="accordion" selected="selected">Accordion</option>
                                        <option value="hover">Hover</option>
                                    </select>
                                </li>
                                <li> Sidebar Position
                                    <select class="sidebar-pos-option form-control input-small input-sm">
                                        <option value="left" selected="selected">Left</option>
                                        <option value="right">Right</option>
                                    </select>
                                </li>
                                <li> Footer
                                    <select class="page-footer-option form-control input-small input-sm">
                                        <option value="fixed">Fixed</option>
                                        <option value="default" selected="selected">Default</option>
                                    </select>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END THEME PANEL -->
        </div>
        <!-- END PAGE TOOLBAR -->
    </div>
    <!-- END PAGE HEAD-->
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
        <?=$bc?>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE BASE CONTENT -->
    
    <div class="row">
        <div class="col-md-10">
            <!-- BEGIN VALIDATION STATES-->
            <div class="portlet light portlet-fit portlet-form bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class=" icon-layers font-green"></i>
                        <span class="caption-subject font-green sbold uppercase">Please fill in the fields</span>
                    </div>
                    <div class="actions">
                        <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                            <i class="icon-cloud-upload"></i>
                        </a>
                        <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                            <i class="icon-wrench"></i>
                        </a>
                        <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                            <i class="icon-trash"></i>
                        </a>
                    </div>
                </div>
                <div class="portlet-body">
                    <!-- BEGIN FORM-->
                    <form class="form-horizontal" id="form_sample_1" method="post">
                        <div class="form-body">
                            <div class="alert alert-danger display-hide">
                                <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                            <div class="alert alert-success display-hide">
                                <button class="close" data-close="alert"></button> Your form validation is successful! </div>
                            <div class="form-group form-md-line-input">
                                <label class="col-md-3 control-label" for="form_control_1">Static Page Name
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" placeholder="" name="name" value="<?=$static_page->name?>">
                                    <div class="form-control-focus"> </div>
                                    <span class="help-block">e.g. Terms and Conditions</span>
                                </div>
                            </div>
                            <div class="form-group form-md-line-input">
                                <label class="col-md-3 control-label" for="form_control_1">Header Label
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" placeholder="" name="label" value="<?=$static_page->label?>">
                                    <div class="form-control-focus"> </div>
                                    <span class="help-block">e.g. Terms and Conditions</span>
                                </div>
                            </div>
                            <div class="form-group last">
                                <label class="control-label col-md-3">Content
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-9">
                                    <textarea class="ckeditor form-control" name="content" rows="6" data-error-container="#editor2_error"><?=$static_page->content?></textarea>
                                    <div id="editor2_error"> </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">(Optional)
                                </label>
                            </div>
                            <div class="form-group form-md-line-input">
                                <label class="col-md-3 control-label" for="form_control_1">URL Text
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" placeholder="" name="url" value="<?=$static_page->url_text?>">
                                    <div class="form-control-focus"> </div>
                                </div>
                            </div>
                            <div class="form-group form-md-line-input">
                                <label class="col-md-3 control-label" for="form_control_1">Links
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" placeholder="" name="links" value="<?=$static_page->link?>">
                                    <div class="form-control-focus"> </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn green">Update</button>
                                    <button type="reset" class="btn default">Reset</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
            <!-- END VALIDATION STATES-->
        </div>
    </div>
</div>
<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->