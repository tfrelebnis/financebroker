
<section class="my_fb_msg_page">
  <div class="container">
    <div class="row">
      <div class='col-sm-12'>
        <br>
        <h3 class="page_brdr_titl text-right" ><span>My Messages</span> <small><span>Your Status: <span class="green_txt"> Available </span><span class="fa fa-cog"></span></span></small></h3>
      </div>
    </div><br>
  </div>
  <div class="container">
    <div class="row msg_fb_cont_row">
      <div class='col-xs-4'><!-- start col-xs-4 -->
        <div class="msg_fb_srch_contnt">
          <h4>ALL RECENT</h4>
          <div class=" msg_fb_srch_icn">
            <span class="fa fa-search"></span>
            <input type="text" class="form-control search_contact" placeholder="Search my contacts">
          </div>
          <hr>
          <div class="all_recent_listing">
            
          <?php 
            foreach($profile_customer as $prow){
                echo '<div class="grid messag_list" id="id_'.$prow->id.'" onclick="contactInfo(this.id)">
              <p class="fb_msg_hed"><span class="nam_ltr_circl">'.strtoupper(substr($prow->first_name, 0, 1)).''.strtoupper(substr($prow->last_name, 0, 1)).'<span class="nam_ltr_avail"></span></span><span class="fb_user_name">'.$prow->first_name.' '.$prow->last_name.' - In a meeting</span> <span class="pull-right"><small>Yesterday</small></span></p>
              <p class="fb_msg_txt">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            </div>';
            }
          ?>
            



            <div class="messag_list">
              <p class="fb_msg_hed"><span class="nam_ltr_circl">RG<span class="nam_ltr_avail"></span></span><span class="fb_user_name">Rick Grines - In a meeting</span> <span class="pull-right"><small>Yesterday</small></span></p>
              <p class="fb_msg_txt">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            </div>
          </div>
        </div>
      </div><!-- end col-xs-4 -->
      <div class='col-xs-8 rght_msg_fb_cntnt'><!-- ====start col-xs-8-->
        <div class="rght_msg_fb_detail">
          <p><span class="nam_ltr_circl">RG<span class="nam_ltr_avail"></span></span><span  class="fb_user_name">Rick Grines - In a meeting</span></p> 
          <p class="rght_msg_fb_date">April 17, 2017(Monday)</p>
          <div class="msg_fg_detail">
            <div id="screen">
              <?php 
              foreach($chat as $row){
              ?>
              <div class="messag_list active">
                <p class="fb_msg_hed"><span class="nam_ltr_circl"><?=strtoupper(substr($row->first_name, 0, 1)).''.strtoupper(substr($row->last_name, 0, 1))?><span class="nam_ltr_avail"></span></span><span class="fb_user_name">Rick Grines - In a meeting</span> <span class="pull-right"><small>7:25 AM</small></span></p>
                <p class="fb_msg_txt"><?=$row->chat?></p>
              </div>
              <?php }?>
              <div class="messag_list">
                <p class="fb_msg_hed"><span class="nam_ltr_circl">RG<span class="nam_ltr_avail"></span></span><span class="fb_user_name">Rick Grines - In a meeting</span> <span class="pull-right"><small>7:25 AM</small></span></p>
                <p class="fb_msg_txt">Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
              </div>
            </div>
          </div>
          <div class="form-group send_file_msgs">
            <div class="input-group fb_edit_attach_send_btn_cntnt">
              <div class="fb_edit_attach_send_btn">
                <div class="input-group">
                  <span class="input-group-btn">
                    <button class="btn btn-secondary" type="button"><span class="fa fa-pencil"></span></button>
                  </span>
                  <input type="text" class="form-control" placeholder="Product name" id="chat_send_box" name="chat">
                  <span class="input-group-btn">
                    <button class="btn btn-secondary" type="button"><span class="fa fa-paperclip"></span></button>
                  </span>
                </div>
              </div>
              <input type="hidden" name="broker_id" id="broker_id" value="">
              <input type="hidden" name="customer_id" value="<?=$customer->id?>">
              <div class="input-group-btn fb_edt_attch_send">
                <button class="btn" id="send_chat">Send</button>
              </div>
            </div>
          </div>
        </div>
      </div><!-- ==end-col-xs-8 -->
    </div>
  </div>
</section>

<script>
$(document).ready(function(){
  setInterval(function(){
    $("#screen").load("<?=base_url('Home/getChatAll')?>")
    }, 2000);
});

function contactInfo(contact_id){
  var broker_id = contact_id.split('_')[1];
  $('#broker_id').attr('value', broker_id);
  var customer_id = $('#customer_id').attr('value', broker_id);
  $.ajax({
    url: "<?=base_url('Home/getUserInfoAll?customer_id=')?>"+customer_id+'&broker_id='+broker_id,
    type: 'POST',
    dataType: 'json',
  })
  .done(function(result) {
    console.log("success");
    console.log(result);
  })
  .fail(function(error) {
    console.log("error");
    console.log(error);
  })
  .always(function(complete) {
    console.log("complete");
    console.log(complete);
  });
  
}

$(document).ready(function (){
  $('.search_contact').keyup(function (){
    //alert('this');
  })
});
</script>