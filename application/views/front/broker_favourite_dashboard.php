
<section class="my_fav_dash_page_sec">
  <div class="container">
    <div class="row">
      <div class='col-sm-12'>
        <h3 class="page_brdr_titl" >My Favourite Customers</h3>
      </div>
    </div><br>
    <div class="row dashbord_row">
      <div class="col-lg-7"><!-- col-lg-7 start -->
      <!-- Code for multiple customer -->
      <?php 
        if(isset($customer) && !empty($customer)){
          foreach($customer as $row){
            ?>
              <div class="col-sm-6">
                <div class="dash_box_list_cntnt">
                  <h4 class='news_titl'><a href="<?= base_url('Home/customer_profile/'.$row->id); ?>"><?=$row->first_name?> <?=$row->last_name?> </a><span class="fa fa-angle-double-right pull-right"></span></h4>
                  <div class="msgs_containr">
                    <p class="msg_notice_count">Messages <span class="red_txt pull-right">2  unread</span></p>
                    <div  class="inner_msg_dv">
                      <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry...</p>
                      <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry...</p>
                      <div class="form-group text-center">
                        <a href="#">Send Message</a>
                      </div>
                    </div>
                  </div>
                  <hr>
                  <div class="form-group">
                    <label class="control-label col-sm-12">Application Status</label>
                    <div class="col-sm-12">
                      <select class="form-control">
                        <option>Enquiry</option>
                        <option></option>
                      </select>
                    </div>
                  </div>
                  <hr>
                  <div class="msgs_containr">
                    <p class="msg_notice_count">Shared Documents: </p>
                    <div  class="inner_msg_dv">
                      <p>Lorem Ipsum <span class="msg_sent_date pull-right">Sent on April 6,2017</span></p>
                      <p>Lorem Ipsum <span class="msg_sent_date pull-right">Sent on April 6,2017</span></p>
                      <div class="form-group text-center">
                        <a href="#">Send Message</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            <?php 
          }
        }else{
          echo "There is no customer in Favourite list";
        }
      ?>
      
        <!-- Code for multiple customer -->
        <!-- <div class="col-sm-6">
          <div class="dash_box_list_cntnt">
            <h4 class='news_titl'><a href="<?= base_url('Home/customer_profile/25'); ?>">Tayler Dawson</a><span class="fa fa-angle-double-right pull-right"></span></h4>
            <div class="msgs_containr">
              <p class="msg_notice_count">Messages <span class=" pull-right">No new messages</span></p>
              <div class="inner_msg_dv">
                <div class="form-group text-center">
                  <a href="#">Send Message</a>
                </div>
              </div>
            </div>
            <hr>
            <div class="form-group">
              <label class="control-label col-sm-12">Application Status</label>
              <div class="col-sm-12">
                <select class="form-control">
                  <option>Documentation</option>
                  <option></option>
                </select>
              </div>
            </div>
            <hr>
            <div class="msgs_containr">
              <p class="msg_notice_count">Shared Documents: <span class="pull-right">No shared Documents</span></p>
              <div  class="inner_msg_dv">
                <div class="form-group text-center">
                  <a href="#">Send Message</a>
                </div>
              </div>
            </div>
          </div>
        </div> -->
      </div><!-- col-lg-7 end -->
      <div class="col-lg-5"><!-- col-lg-5 start -->
        <div class="col-xs-12">
          <div class="dshbrd_custmr_rght">
           <!--  <span class="dashbord_rght_bg"></span> -->
            <div class="dash_box_list_cntnt "> 
              <h4 class="tabl_capton ">My Customers</h4>
              <?php print_r($hiredcustomer); ?>
              <table class="table dsh_bord_customr_chat_tabl text-center">
                <thead>
                  <tr>
                    <th>Customer</th>
                    <th>Application Status</th>
                    <th width="85px">Options</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td><span class="nam_ltr_circl">JD</span><a href="<?= base_url('Home/customer_profile/') ?>">John Doe</a> </td>
                    <td>Enquiry</td>
                    <td><span class="fa fa-comment-o"></span><span class='fa fa-paperclip'></span></td>
                  </tr>
                  <tr>
                    <td><span class="nam_ltr_circl">JD<span class="nam_ltr_avail"></span></span><a href="<?= base_url('Home/customer_profile/') ?>">John Doe</a></td>
                    <td>Enquiry</td>
                    <td><span class="fa fa-comment-o"></span><span class='fa fa-paperclip'></span></td>
                  </tr>
                  <tr>
                    <td><span class="nam_ltr_circl">JD<span class="nam_ltr_question fa fa-question"></span></span><a href="<?= base_url('Home/customer_profile/') ?>">John Doe</a></td>
                    <td></td>
                    <td><span class="fa fa-comment-o"><span class="chat_contng">1</span></span><span class='fa fa-paperclip'></span></td>
                  </tr>
                  <tr>
                    <td>hello</td>
                    <td></td>
                    <td></td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div class="dash_box_list_cntnt "> 
              <h4 class="tabl_capton ">Customers' Marks as Favourit</h4>
              <table class="table dsh_bord_customr_chat_tabl text-center">
                <thead>
                  <tr>
                    <th>Customer</th>
                    <th>Application Status</th>
                    <th width="85px">Options</th>
                  </tr>
                </thead>
                <tbody>
                <?php 
                if(isset($customer) && !empty($customer)){
                  foreach($customer as $row){ 
                ?>
                  <tr>
                    <td><span class="nam_ltr_circl"><?=strtoupper(substr($row->first_name, 0, 1))?><?=strtoupper(substr($row->last_name, 0, 1))?></span><a href="<?= base_url('Home/customer_profile/'.$row->id) ?>"><?=$row->first_name.' '.$row->last_name?></a> </td>
                    <td>Enquiry</td>
                    <td><span class="fa fa-comment-o"></span><span class='fa fa-paperclip'></span></td>
                  </tr>
                  <?php }
                }else{
                    echo '<tr>
                    <td>
                    There is no customer in list<td><tr>';
                  }

                  ?>
                  
                </tbody>
              </table>
            </div>
            <div class="dash_box_list_cntnt "> 
              <h4 class="tabl_capton ">Customers' Add Requests</h4>
              <table class="table dsh_bord_customr_chat_tabl text-center">
                <thead>
                  <tr>
                    <th>Customer</th>
                    <th>Application Status</th>
                    <th width="85px">Options</th>
                  </tr>
                </thead>
                <tbody>
                <?php 
                if(isset($requestedCustomer) && !empty($requestedCustomer)){
                  foreach($requestedCustomer as $row){ 
                ?>
                  <tr>
                    <td><span class="nam_ltr_circl"><?=strtoupper(substr($row->first_name, 0, 1))?><?=strtoupper(substr($row->last_name, 0, 1))?></span><a href="<?= base_url('Home/customer_profile/'.$row->id) ?>"><?=$row->first_name.' '.$row->last_name?></a> </td>
                    <td>Enquiry</td>
                    <td><span class="fa fa-comment-o"></span><span class='fa fa-paperclip'></span></td>
                  </tr>
                  <?php }
                }else{
                    echo '<tr>
                    <td>
                    There is no customer in list<td><tr>';
                  }

                  ?>
                  
                </tbody>
              </table>
            </div>
          </div>
          <div class='clearfix'></div>
        </div>
        <div class='clearfix'></div>
      </div><!-- col-lg-5 start -->
      <div class='clearfix'></div>
    </div>
    <div class='clearfix'></div>
  </div>
   <div class='clearfix'></div>
</section>

