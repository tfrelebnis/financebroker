<?php 
  $business_specification = explode(',', $broker->business_specification);
?>
<section class="my_profile_page_sec">
  <div class="container">
    <div class="row">
        <h3 class="page_brdr_titl">My Profile</h3>
    </div>
    <br>
    <div class="row my_profile_contnt_row row-eq-height">
      <form method="POST" name="BrokerForm" onsubmit="return formBrokerProfileValidation()">
        <div class="col-lg-8">
          <div class="left_my_profil form-horizontal"><!-- start form -->
            <div class="row my_prof_box my_prof_box1"><!-- box1 start -->
              <div class="col-sm-6">
                <div class="form-group">
                  <label for="last_name"  class="control-label col-xs-5">Last Name<span class="red_txt">*</span>:</label>
                  <div class="col-xs-7">
                    <input id="last_name" type="text" class="form-control"  placeholder="Please Enter Last Name" name="last_name" value="<?=$broker->last_name?>">
                    <div class="last_name error-msg red_txt "></div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="first_name"  class="control-label col-xs-5">First Name<span class="red_txt">*</span>:</label>
                  <div class="col-xs-7">
                    <input id="first_name" type="text" class="form-control" placeholder="Please Enter First Name " name="first_name" value="<?=$broker->first_name?>">
                    <div class="first_name error-msg red_txt "></div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="title"  class="control-label col-xs-5">Title<span class="red_txt">*</span>:</label>
                  <div class="col-xs-7">
                    <input id="title" type="text" class="form-control" placeholder="Please Enter Title" name="title" value="<?=$broker->title?>">
                    <div class="title error-msg red_txt "></div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="aggregator" class="control-label col-xs-5">Aggregator<span class="red_txt">*</span>:</label>
                  <div class="col-xs-7">
                    <select class="form-control" name="aggregator" id="aggregator">
                      <option value="0">Please select Aggregator</option>
                      <option <?=$broker->aggregator == '1' ? 'selected="selected"' : ''?>>1</option>
                      <option <?=$broker->aggregator == '2' ? 'selected="selected"' : ''?>>2</option>
                    </select>
                    <div class="aggregator error-msg red_txt "></div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="business_qualification"  class="control-label col-xs-5"> Business Qualifications<span class="red_txt">*</span>:</label>
                  <div class="col-xs-7">
                    <textarea class="form-control" id="business_qualification" placeholder="Please Enter Your Business Qualification" name="business_qualification"><?=$broker->business_qualification?></textarea>
                    <div class="business_qualification error-msg red_txt "></div>
                  </div>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                  <label for="business_name"  class="control-label col-xs-5">Business Name<span class="red_txt">*</span>:</label>
                  <div class="col-xs-7">
                    <input id="business_name" type="text" class="form-control" placeholder="Please Enter Business Name" name="business_name" value="<?=$broker->business_name?>">
                    <div class="business_name error-msg red_txt "></div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="credit_license"  class="control-label col-xs-5">Credit Licence Number<span class="red_txt">*</span>:</label>
                  <div class="col-xs-7">
                    <input id="credit_license" type="text" class="form-control" placeholder="Please Enter Credit Licence Number" name="credit_license" value="<?=$broker->credit_license?>">
                    <div class="credit_license error-msg red_txt "></div>
                  </div>
                </div>
                <div class="form-group">
                  <label id="business_specification" class="control-label col-xs-5">Business Specialisations<span class="red_txt">*</span>:</label>
                  <div class="col-xs-7">
                    <select multiple name="business_specification[]" class="form-control mySelect" id="selectEvents">
                      <option value="1" <?=in_array('1', $business_specification) ? 'selected="selected"' : ''?>>Trade Fair</option>
                      <option value="2" <?=in_array('2', $business_specification) ? 'selected="selected"' : ''?>>Party</option>
                      <option value="3" <?=in_array('3', $business_specification) ? 'selected="selected"' : ''?>>Foo</option>
                      <option value="4" <?=in_array('4', $business_specification) ? 'selected="selected"' : ''?>>Bar</option>
                    </select>
                    <div class="business_specification error-msg red_txt "></div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="biography"  class="col-xs-5"> Profile/Biography<span class="red_txt">*</span>:</label>
                  <div class="col-xs-7">
                    <textarea class="form-control" id="biography" placeholder="Please tell us something about who are you" name="biography"><?=$broker->biography?></textarea>
                    <div class="biography error-msg red_txt "></div>
                  </div>
                </div>
              </div>
            </div><!-- box1 end -->
            <br>
            <div class="row my_prof_box my_prof_box2"><!-- box2 start -->
             
              <div class="form-group">
                <label for="business_address_1"  class="control-label col-xs-3">Business Address 1<span class="red_txt">*</span>:</label>
                <div class="col-xs-7">
                  <input id="business_address_1" type="text" class="form-control" placeholder="Address Line 1" name="business_address_1" value="<?=$broker->business_addess_1?>">
                  <div class="business_address_1 error-msg red_txt "></div>
                </div>

              </div>
              <div class="form-group">
                <label for="business_address_2"  class="control-label col-xs-3">Business Address 2</label>
                <div class="col-xs-7">
                  <input id="business_address_2" type="text" class="form-control" placeholder="Address Line 2" name="business_address_2" value="<?=$broker->business_addess_2?>">
                  <div class="business_address_2 error-msg red_txt "></div>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                  <label for="suburb"  class="control-label col-xs-5">Suburb<span class="red_txt">*</span>:</label>
                  <div class="col-xs-7">
                    <input id="suburb" type="text" class="form-control" placeholder="Please Enter Suburb" name="suburb" value="<?=$broker->suburb?>">
                    <div class="suburb error-msg red_txt "></div>
                  </div>
                </div>
               
                <div class="form-group">
                  <label for="states"  class="control-label col-xs-5">State<span class="red_txt">*</span>:</label>
                  <div class="col-xs-7" id="state">
                  <select id="states" name="state" class="form-control">
                  <?php 
                      foreach($state as $row){
                        $val = $broker->state == $row->id ? 'selected = "selected"' : '';
                        echo '<option value="'.$row->id.'" '.$val.'>'.$row->name.'</option>';
                      }
                  ?>
                    </select>
                    <div class="states error-msg red_txt "></div>
                  </div>
                </div>

              </div>
              <div class="col-sm-6">
                 <div class="form-group">
                  <label for="post_code"  class="control-label col-xs-5">Post Code<span class="red_txt">*</span>:</label>
                  <div class="col-xs-7">
                    <input id="post_code" type="text" class="form-control" placeholder="Please Enter Post Code" name="post_code" value="<?=$broker->post_code?>">
                    <div class="post_code error-msg red_txt "></div>
                  </div>
                </div>
              
                <div class="form-group">
                  <label for="country" class="control-label col-xs-5">Country<span class="red_txt">*</span>:</label>
                  <div class="col-xs-7">
                    <select id="country" class="form-control">
                    <?php
                        foreach($countries as $row){
                          
                            $country[$row->id] = $row->country_name;   
                             $cor = $row->id =='13'? 'selected="selected"' : '';
                             if( isset($cor) && !empty($cor) ){
                               echo '<option value="'.$row->id.'" '.$cor.'>'.$row->country_name.'</option>'; 
                             }
                     }     
                        
                ?>
                  </select>
                   
                    
                    <div class="country error-msg red_txt "></div>
                  </div>
                </div>
                
            </div><!-- box2 end -->
          </div>
              <br>
            <div class="row my_prof_box my_prof_box3"><!-- box3 start -->
              <div class="col-sm-6">
                <div class="form-group">
                  <label for="email_address"  class="control-label col-xs-5">Email Address<span class="red_txt">*</span>:</label>
                  <div class="col-xs-7">
                    <span><?=$broker->address?></span>
                    <div class=" error-msg red_txt "></div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="website"  class="control-label col-xs-5">Website<span class="red_txt">*</span>:</label>
                  <div class="col-xs-7">
                    <input id="website" type="text" class="form-control" placeholder="Please Enter Website" name="website" value="<?=$broker->website?>">
                    <div class="website error-msg red_txt "></div>
                  </div>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                  <label for="office"  class="control-label col-xs-5">Office:</label>
                  <div class="col-xs-7">
                    <input id="office" type="text" class="form-control" placeholder="Please enter your office no" name="office" value="<?=$broker->office?>">
                
                  </div>
                </div>
                <div class="form-group">
                  <label for="mobile"  class="control-label col-xs-5">Mobile<span class="red_txt">*</span>:</label>
                  <div class="col-xs-7">
                    <input id="mobile" type="text" class="form-control" placeholder="Please enter your mobile no" name="mobile" value="<?=$broker->mobile?>">
                  <div class="mobile error-msg red_txt "></div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="fax"  class="control-label col-xs-5">Fax:</label>
                  <div class="col-xs-7">
                    <input id="fax" type="text" class="form-control" placeholder="Please enter your fax no" name="fax" value="<?=$broker->fax?>">
                   
                  </div>
                </div>
              </div>
            </div><!-- box3 end -->
                      <br>
            <div class="row my_prof_box my_prof_box4"><!-- box4 start -->
              <div class="col-sm-6">
                <div class="form-group">
                  <label for="facebook" class="control-label col-xs-5">Facebook:</label>
                  <div class="col-xs-7">
                    <input id="facebook" type="text" class="form-control" placeholder="Please enter your facebook URl" name="facebook" value="<?=$broker->fb?>">
                    
                  </div>
                </div>
                <div class="form-group">
                  <label for="linkedin"  class="control-label col-xs-5">Linkedin:</label>
                  <div class="col-xs-7">
                    <input id="linkedin" type="text" class="form-control" placeholder="Please enter your Linkedin URl" name="linkedin" value="<?=$broker->ln?>">
                  
                  </div>
                </div>
                <div class="form-group">
                  <label for="twitter"  class="control-label col-xs-5">Twitter:</label>
                  <div class="col-xs-7">
                    <input id="twitter" name="twitter" type="text" class="form-control" placeholder="Please enter your Twitter URl" value="<?=$broker->tw?>">
                    
                  </div>
                </div>
              </div>
              <div class="col-sm-6">
                <!-- <div class="form-group">
                  <label for=""  class="control-label col-xs-5">Name<span class="red_txt">*</span>:</label>
                  <div class="col-xs-7">
                    <input id="" type="text" class="form-control" placeholder="Please">
                  </div>
                </div>
                <div class="form-group">
                  <label for=""  class="control-label col-xs-5"><span class="red_txt">*</span>:</label>
                  <div class="col-xs-7">
                    <input id="" type="text" class="form-control" placeholder="Please">
                  </div>
                </div>
                <div class="form-group">
                  <label for=""  class="control-label col-xs-5">Name<span class="red_txt">*</span>:</label>
                  <div class="col-xs-7">
                    <input id="" type="text" class="form-control" placeholder="Please">
                  </div>
                </div> -->
              </div>
            </div><!-- box4 end -->
            <br>
            <div class="row text-center">
               <button type="submit"  class="btn">SAVE CHANGES</button>
            </div>
          </div><!-- close-form -->
          <br>
        </div>
      </form>
      <form method="get">
      <?php 
        if($broker->payment_status == '1' && $subscribed_statue == 1){
      ?>
      <div class="col-lg-4">
        <div class="rght_my_profil">
          <div class="row my_prof_rght_box my_prof_rght_box2"><!-- right_box2 start-->
            <div class="col-sm-12 text-center">
              <div class="not_visible_sontnt">
                <p class="fa fa-check "></p>
                <p>Your profile is currently</p>
                <h2 class="text-center green_txt">VISIBLE</h2>
                <p class="text-left">Your current subscription.</p>
                <div class="pkg_cntnt">
                  <h3>NORMAL PACKAGE</h3>
                  <P></P>
                </div>
                <div class="pkg_cntnt text-center">
                  <div class="pkg_month">
                    <h5>MONTHLY</h5>
                    <h3>$22/month</h3>
                    <p>inclusive of GST</p>
                    <!-- <button type="button" class="btn">SUBSCRIBE</button> -->
                  </div>
                  <div class="pkg_annual cancl_change">
                    <button type="button"  class="btn">CANCEL SUBSCRIPTION</button>
                    <button href="button"  class="btn">CHANGE SUBSCRIPTION</button>
                  </div>
                </div>
                <P class="text-left">Subscribed on April 24 , 2017</p>
                <br>
              </div>
            </div>
          </div><!-- right_box1 start-->
          <br>
        </div>
      </div>
      <?php }elseif($broker->payment_status == '0' || $broker->payment_status == '2' || $subscribed_statue == 0){?>
      <div class="col-lg-4"><!-- col-sm-4 start -->
        <div class="rght_my_profil">
          <div class="row my_prof_rght_box my_prof_rght_box1"><!-- right_box1 start-->
            <div class="col-sm-12 text-center">
              <div class="upload_prof_photo">
                <span class="fa fa-user"></span>
                <input id="profile_img_path" type="file" name="profile_img_path">
                <h4>Upload Profile Photo</h4>
              </div>
              <div class="upload_bsns_logo">
                <span class="fa fa-file-image-o"></span>
                <input id="" type="file">
                <h4>Upload Business Logo</h4>
              </div>
            </div>
          </div><!-- right_box1 start-->
        </div>
        <p></p>
        <div class="rght_my_profil">
          <div class="row my_prof_rght_box my_prof_rght_box2"><!-- right_box2 start-->
            <div class="col-sm-12 text-center">
              <div class="not_visible_sontnt">
                <p class="fa fa-close "></p>
                <p>Your profile is currently</p>
                <h2 class="text-center red_txt">NOT VISIBLE</h2>
                <p>Subscribe to any of the packages below to make your profile visible to customers.</p>
                <?php 
                  if($broker->payment_status == '2'){
                    echo '<i class="red_txt">Your payment was not successful</i>';
                  }
                ?>
                <!-- broker package -->
                <div class="pkg_cntnt">
                  <h3>NORMAL PACKAGE</h3>
                  <P>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</P>
                </div>
                <div class="pkg_cntnt text-center">
                <?php 
                $i= 1;
                  foreach($package as $row){
                    if($i%2==0){
                      $class = 'pkg_annual';
                    }else{
                      $class = 'pkg_month';
                    }
                ?>
                  <div class="<?=$class?>">
                    <h5><?=$row->package_name?></h5>
                    <h3>$<?=$row->monthly_fee?>/month</h3>
                    <p>inclusive of GST</p>
                    <a type="button" class="btn" href="<?=base_url('Home/purchase_package/'.$this->uri->segment('3').'/'.$row->monthly_fee.'/');?>" target="_blank">SUBSCRIBE</a>
                  </div>
                  <?php $i++; }?>
                  <!-- <div class="pkg_month">
                    <h5>MONTHLY</h5>
                    <h2>$22/month</h2>
                    <p>inclusive of GST</p>
                    <a type="button" class="btn" href="<?=base_url('Home/purchase_package/'.$this->uri->segment('3').'/22/');?>" target="_blank">SUBSCRIBE</a>
                  </div>
                  <div class="pkg_annual">
                    <h5>ANNUAL</h5>
                    <h2>$220/year</h2>
                    <p>inclusive of GST</p>
                    <a type="button" href="<?=base_url('Home/purchase_package/'.$this->uri->segment('3').'/220/');?>" target="_blank" class="btn">SUBSCRIBE</a>
                  </div> -->
                </div>
                <br>
                <div class="pkg_cntnt">
                  <h3>FEATURED PACKAGE</h3>
                  <P>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</P>
                </div>
                <div class="pkg_cntnt text-center">
                  <div class="pkg_month">
                    <h5>MONTHLY</h5>
                    <h3>$22/month</h3>
                    <p>inclusive of GST</p>
                    <a type="button" class="btn" href="<?=base_url('Home/purchase_package/'.$this->uri->segment('3').'/22/');?>" target="_blank">SUBSCRIBE</a>
                  </div>
                  <div class="pkg_annual">
                    <h5>ANNUAL</h5>
                    <h3>$220/year</h3>
                    <p>inclusive of GST</p>
                    <a type="button"  class="btn" href="<?=base_url('Home/purchase_package/'.$this->uri->segment('3').'/220/');?>" target="_blank">SUBSCRIBE</a>
                  </div>
                </div>
              <!-- broker package -->
              </div>
            </div>
          </div><!-- right_box1 start-->
          <br>
        </div>
      </div>
      <?php }?>
      <!-- col-sm-4 right end -->
      </form>
    </div>
  </div>
</section>

<script type="text/javascript">
  $(document).ready(function() {
    $('#country').change(function (){
      var country  = $('#country').val();
      $.ajax({
        url: "<?=base_url('Home/getStateByCountry/')?>"+country,
        type: 'POST',
        dataType: 'json',
      })
      .done(function(result) {
        $('#state').empty();
        $('#state').append(result);
      })
      .fail(function(error) {
        console.log("error");
        console.log(error);
      });
    });

    $('.form-control').change(function (){
      $('.error-msg').empty();
    });

  });

  /*code validation for given form */
  function formBrokerProfileValidation() {
    var last_name = document.forms["BrokerForm"]["last_name"].value;
    var business_name = document.forms["BrokerForm"]["business_name"].value;
    var first_name = document.forms["BrokerForm"]["first_name"].value;
    var title = document.forms["BrokerForm"]["title"].value;
    var aggregator = document.forms["BrokerForm"]["aggregator"].value;
    var website = document.forms["BrokerForm"]["website"].value;
    var business_qualification = document.forms["BrokerForm"]["business_qualification"].value;
    var mobile = document.forms["BrokerForm"]["mobile"].value;
    var biography = document.forms["BrokerForm"]["biography"].value;
    var suburb = document.forms["BrokerForm"]["suburb"].value;
    

    if(last_name == "" || business_name == "" || website == "" || first_name == "" || title == "" || aggregator == "" || business_qualification == ""){

        if (last_name == "") {
          $('.last_name').empty();
          $('.last_name').append('Last Name is required');
          document.forms["BrokerForm"]["last_name"].focus();
        }
        if (first_name == "") {
          $('.first_name').empty();
          $('.first_name').append('First name is required');
          document.forms["BrokerForm"]["first_name"].focus();
        }
        if (title == "") {
          $('.title').empty();
          $('.title').append('title is required');
          document.forms["BrokerForm"]["title"].focus();
        }
        if (business_name == "") {
          $('.business_name').empty();
          $('.business_name').append('Business Name is required');
          document.forms["BrokerForm"]["business_name"].focus();
        }
        if (aggregator == "") {
            $('.aggregator').empty();
          $('.aggregator').append('aggregator is required');
          document.forms["BrokerForm"]["aggregator"].focus();
        }
        if (business_qualification == "") {
            $('.business_qualification').empty();
          $('.business_qualification').append('Business Name is required');
          document.forms["BrokerForm"]["business_qualification"].focus();
        }
        if (website == "") {
            $('.website').empty();
          $('.website').append('Website is required');
          document.forms["BrokerForm"]["website"].focus();
        }
        if (mobile == "") {
            $('.mobile').empty();
          $('.mobile').append('Mobile is required');
          document.forms["BrokerForm"]["mobile"].focus();
        }
        if (biography == "") {
            $('.biography').empty();
          $('.biography').append('Biography is required');
          document.forms["BrokerForm"]["biography"].focus();
        }
        if (suburb == "") {
            $('.suburb').empty();
          $('.suburb').append('Suburb is required');
          document.forms["BrokerForm"]["suburb"].focus();
        }
        if (post_code == "") {
            $('.post_code').empty();
          $('.post_code').append('Post Code is required');
          document.forms["BrokerForm"]["post_code"].focus();
        }
        

      return false;
    }

  }
</script>