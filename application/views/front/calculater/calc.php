

<section class="calc_pag_content">
	<br><br>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 text-center">
                <div class="calulator-inner">
                    <a href="#"><h4><span class="fa fa-calculator"></span> How much can I borrow?</h4></a>
                    <p><small>Borrowing capacity calculator -Calculate how much you can borrow from the lenders based on your existing income and expenses.</small></p>
                    <div class="view_calc"> <a href="#">View Calculator ></a></div>
                </div>
            
                <div class="calulator-inner">
                    <a href="#"><h4><span class="fa fa-calculator"></span> Home Loan Repayments</h4></a>
                    <p><small>Repayments calculator - Work out what your minimum weekly, fortnightly or monthly repayments would be on your home loan.</small></p>
                    <div class="view_calc"> <a href="#">View Calculator ></a></div>
                </div>
            
                <div class="calulator-inner">
                    <a href="#"><h4><span class="fa fa-calculator"></span> Comparison Rate</h4></a>
                    <p><small>Comparison rate calculator reflects the total annual cost to a borrower of a loan. It wraps up interest payments and fees and expresses all these costs in one rate.</small></p>
                    <div class="view_calc"> <a href="#">View Calculator ></a></div>
                </div>
            
                <div class="calulator-inner">
                    <a href="#"><h4><span class="fa fa-calculator"></span> Stamp Duty</h4></a>
                    <p><small>Stamp duty adds significantly to the cost of buying a property.  Find out just how much stamp duty, mortgage registration fees and transfer fees will cost in your state or territory.</small></p>
                    <div class="view_calc"> <a href="#">View Calculator ></a></div>
                </div>
            
                <div class="calulator-inner">
                    <a href="#"><h4><span class="fa fa-calculator"></span> Extra Repayments </h4></a>
                    <p><small>See how much money you can save when you make extra repayments throughout your loan. See how much quicker you could pay off your loan.</small></p>
                    <div class="view_calc"> <a href="#">View Calculator ></a></div>
                </div>
            
                <div class="calulator-inner">
                    <a href="#"><h4><span class="fa fa-calculator"></span> Lump Sum Payment</h4></a>
                    <p><small>What will be the effect on your loan if you pay off a lump sum? You could save on interest and pay off your loan sooner by paying a lump sum.</small></p>
                    <div class="view_calc"> <a href="#">View Calculator ></a></div>
                </div>
            
                <div class="calulator-inner">
                    <a href="#"> <h4><span class="fa fa-calculator"></span> Split Loan</h4></a>
                    <p><small>If you're thinking of splitting your loan into fixed and variable rate periods, our calculator can work out estimated repayment and interest amounts for you.</small></p>
                    <div class="view_calc"> <a href="#">View Calculator ></a></div>
                </div>
            
                <div class="calulator-inner">
                    <a href="#"> <h4><span class="fa fa-calculator"></span> Reverse Mortgage</h4></a>
                    <p><small>Use the equity in your home to receive a lump sum or a periodic payment.</small></p>
                    <div class="view_calc"> <a href="#">View Calculator ></a></div>
                </div>
            </div>
        </div>
	</div>
	<br>
 <section class="budget_titl">
    <div class="container">
        <div class="row">
        	<div class="col-xs-12">
        		<h3>FINANCIAL TOOLS</h3>
        	</div>
        </div>
    </div>
 </section>
 	<br>
    <div class="container">
        <div class="row">
        	<div class="col-xs-12 text-center">
                <div class="finac_tol">
                    <a href="#"><h4><span class="fa fa-calculator"></span> Achieve My Savings Target</h4></a>
                    <p><small>Do you have something important you'd like to save for? Find out how much you need to save periodically to get there.</small></p>
                    <div class="view_calc"> <a href="#">View Calculator ></a></div>
                </div>
                <div class="finac_tol">
                    <a href="#"><h4><span class="fa fa-calculator"></span> Budget</h4></a>
                    <p><small>Input all your incomings and outgoings to see if you are in surplus or have a shortfall. See where your money is going and how you can save more</small></p>
                    <div class="view_calc"> <a href="#">View Calculator ></a></div>
                </div>
            </div>
        </div>
    </div>
</section>