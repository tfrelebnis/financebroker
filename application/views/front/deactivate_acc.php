<?php $uri = $this->uri->segment(3); ?>
<section class="change_pass_page">
  	<div class="container">
	    <div class="row">
	      	<div class='col-sm-12'>
		        <br>
		        	<h3 class="page_brdr_titl" >Deactivate My Account</h3>
		        <br>
	      	</div>
	    </div>
  	</div>

    <div class="container">
    	<div class="row">
      		<div class='col-sm-12'>
      			<div class="change_pass_form deact_acc_form">
	      			<form class="form-horizontal" action="<?= base_url('Home/fb_account_deactivate_by_id/').$uri ?>" method="POST">
	      				<div class="form-group">
	      					<label for="ch_ps_old" class="control-label col-sm-12">We are sorry you had to deactivate your account.Please let us know reason for deactivation<span class="red_txt">*</span></label>
	      				</div>
	      				<div class="form-group">
	      					<div class="col-sm-offset-1 col-sm-11">
		      					<div class="radio radio-info">
								  <input type="radio" id="1" class="deact_acc_chk_bx" name="reson[]" value="1"> 
								  <label for="1">Reason 1 Low Self-Esteem</label>
								</div>
							</div>
	      				</div>
	      				<div class="form-group">
	      					<div class="col-sm-offset-1 col-sm-11">
		      					<div class="radio radio-info">
		      						<input type="radio" id="2" class="deact_acc_chk_bx" name="reson[]" value="2">
								  <label for="2">Reason 2 The Job Hunt</label>
								</div>
							</div>
	      				</div>
	      				<div class="form-group">
	      					<div class="col-sm-offset-1 col-sm-11">
		      					<div class="radio radio-info">
		      						<input type="radio" id="3" class="deact_acc_chk_bx" name="reson[]" value="3">
								  <label for="3">Reason 3 Not satisfied</label>
								</div>
							</div>
	      				</div>
	      				<div class="form-group">
	      					<div class="col-sm-offset-1 col-sm-11">
		      					<div class="radio radio-info">
		      						<input type="radio" id="4" class="deact_acc_chk_bx" name="reson[]" value="4">
								  <label for="4">Reason 4 Found best option</label>
								</div>
							</div>
	      				</div>
	      				<div class="form-group other5">
	      					<div class="col-sm-offset-1 col-sm-11">
		      					<div class="radio radio-info">
		      					<input type="radio" id="5" class="deact_acc_chk_bx" name="reson[]" value="5">
								  <label for="5">Other:</label>
								</div>
							</div>
	      				</div>
	      				<div class="form-group hidden text_box">
	      					<div class="col-sm-offset-1 col-sm-11">
		      					<input type="text" class="form-control" id="text5" name="other">
							</div>
	      				</div>
	      				<div class="form-group">
	      					<div class="col-sm-offset-4 col-sm-8">
	      						<input type="submit" id="submit_form" class="btn" value="DEACTIVATE MY ACCOUNT">
	      					</div>
	      				</div>
	      			</form>
	      		</div>
      		</div>
	    </div>
  	</div>
</section>