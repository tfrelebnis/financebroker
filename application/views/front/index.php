  
  <section class="home_slidr_sec">
      <div class="container">
        <form class="row form-horizontal revolution_form">
          <div class="form-group col-sm-12 rev_cont">
            <div class="dropdown fb_br">
              <a class="dropdown-toggle" data-toggle="dropdown" href="#">JOIN THE REVOLUTION</a>
              <ul class="dropdown-menu">
                <li ><a href="#" class=""><span class="borrower_icon"></span>Borrower</a></li>
                <li ><a href="#" class=""><span class="broker_icon"></span>Finance Broker</a></li>
              </ul>
            </div>
            <!-- <div class="dropdown">
              <button class="btn btn-default dropdown-toggle" type="button" id="menu1" data-toggle="dropdown">JOIN THE REVOLUTION &nbsp;&nbsp;<span class="fa fa-caret-down"></span>
                <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                  <li role="presentation"><a href="#" class=""><span class="borrower_icon"></span>Borrower</a></li>
                  <li role="presentation"><a href="#" class=""><span class="broker_icon"></span>Finance Broker</a></li>
                </ul>
              </button>
            </div> -->

          </div>
        </form>
        <form class="form-horizontal row find_broker_row">
          <div class=" col-sm-12">
            <div class="find_broker_contnt">
              <div class="col-sm-2 hedng flt_lft"><h3>FIND A BROKER</h3></div>
              <div class="row form_ele  flt_lft">
                    <select class="selectpicker" data-style="close_me" data-width="135px" title="Search By" data-header="Search By">
                      <option>Closest to me</option>
                      <option>Best rating</option>
                    </select>
                  <!--<select class="selectpicker" data-style="state_fr" data-header="Best rating" title="Best rating" data-width="117">
                      <option>1</option>
                      <option>2</option>
                      <option>3</option>
                    </select>-->
                    <select class="selectpicker" data-style="sel_form" title="State" data-header="State" data-width="90px">
                      <option>NSW</option>
                      <option>VIC</option>
                      <option>QLD</option>
                      <option>WA</option>
                      <option>SA</option>
                      <option>NT</option>
                      <option>ACT</option>
                      <option>TAS</option>
                    </select>
                  <div class=" post_cod">
                    <input type="text" class="form-control " placeholder="Post Code">
                  </div>
                  <div class="text-right find_broker_btn">
                    <input type="submit" class="btn" value="Search">
                  </div>
              </div>
            </div>
          </div>
          <div class=" col-sm-4">
            
          </div>
        </form>
      </div>
  </section>
  <section class="how_to_sec">
    <div class="container">
      <div class="row">
        <div class="col-sm-4">
          <div class="how_to_contnt text-center">
              <img src="<?=base_url('assets/front/img/1-1.png')?>" alt="400px × 267px1" style="background: rgb(239, 108, 0) none repeat scroll 0% 0%; border-radius: 50%;" class="image-bottom-space">
              <h4><a href="#">Borrowers</a></h4>
              
          </div>
        </div>
        <div class="col-sm-4">
          <div class="how_to_contnt text-center">
              <img src="<?=base_url('assets/front/img/2-1-1.png')?>" alt="400px × 267px1" class="image-bottom-space">
              <h4><a href="#">Finance Brokers</a></h4>
              
          </div>
        </div>
        <div class="col-sm-4">
          <div class="how_to_contnt text-center">
              <img src="<?=base_url('assets/front/img/3-1.png')?>" alt="400px × 267px1" style="background-color: rgb(25, 123, 152); border-radius: 50%;" class="image-bottom-space">
              <h4><a href="#">Tools</a></h4>
              
          </div>
        </div>
      </div>
    </div>
  </section>


