
<section class="my_fb_file_page">
  <div class="container">
    <div class="row">
      <div class='col-sm-12'>
        <br>
        <h3 class="page_brdr_titl text-right" ><span>My Files</span> <small><span>Your Status: <span class="green_txt"> Available </span><span class="fa fa-cog"></span></span></small></h3>
      </div>
    </div><br>
  </div>
  <div class="container">
    <div class="row msg_fb_cont_row files_fb_cont_row">
      <div class='col-xs-4'><!-- start col-xs-4 -->
        <div class="msg_fb_srch_contnt">
          <h4>ALL RECENT</h4>
          <div class=" msg_fb_srch_icn">
            <span class="fa fa-search"></span>
            <input type="text" class="form-control" placeholder="Search my contacts">
          </div>
          <div class="messag_list">
            <p class="fb_msg_hed"><span class="nam_ltr_circl">RG<span class="nam_ltr_avail"></span></span><span class="fb_user_name">Rick Grines - In a meeting</span> <span class="pull-right"><small>Yesterday</small></span></p>
            <p class="fb_msg_txt">Passport.pdf</p>
          </div>
          <div class="messag_list">
            <p class="fb_msg_hed"><span class="nam_ltr_circl">RG<span class="nam_ltr_avail"></span></span><span class="fb_user_name">Rick Grines - In a meeting</span> <span class="pull-right"><small>Yesterday</small></span></p>
            <p class="fb_msg_txt">Certificate of Employeement.pdf</p>
          </div>
          <br>
          <br><br><br>
        </div>
      </div><!-- end col-xs-4 -->
      <div class='col-xs-8 rght_msg_fb_cntnt rght_file_fb_cntnt'><!-- ====start col-xs-8-->
        <div class="rght_msg_fb_detail ">
          <div class="fil_pag_tab text-right"><!-- <span class="nam_ltr_circl">RG<span class="nam_ltr_avail"></span></span> --><span  class="fb_user_name pull-left">Rick Grines - In a meeting</span>
            <div class="form-group">
              <!-- <label for="files" class="">Send a File</label>&nbsp;&nbsp;
              <input id="files" style="visibility:hidden;" type="file">
              <span class="fa fa-cog "></span> -->
              <div class="dropdown">
                <button class="send_fil_drop_btn btn dropdown-toggle" type="button" data-toggle="dropdown">Send a File
                <span class="fa fa-cog"></span></button>
                <ul class="dropdown-menu">
                  <li><label  class="btn"  for="filesupload">Upload a File
                    <input id="filesupload" style="visibility:hidden;" type="file">
                  </label></li>
                  <li><a href="#" class="btn">Share via Dropbox</a></li>
                </ul>
              </div>
            </div>
          </div> 
          <p class="rght_msg_fb_date">April 17, 2017(Monday)</p>
          <div class="messag_list active">
            <p class="fb_msg_hed">Passport.pdf</span> <span class="pull-right"><small>You have <span class="blue_txt">SENT</span>&nbsp; on 7:25 AM</small></span></p>
            <p class="fb_msg_txt"></p>
          </div>
          <div class="messag_list">
            <p class="fb_msg_hed">Passport.pdf</span> <span class="pull-right"><small>You have <span class="yelo_txt">RECEIVED</span>&nbsp; on 7:10 AM</small></span></p>
            <p class="fb_msg_txt"></p>
          </div>
        </div>
      </div><!-- ==end-col-xs-8 -->
    </div>
  </div>
</section>
