<section class="my_profile_page_sec">
  <div class="container">
    <div class="row">
      <h3 class="page_brdr_titl">My Profile</h3>
    </div>
    <br>
    <form method="POST" name="CustomerForm" enctype="multipart/form-data" onsubmit="return formValidation()" class="row my_profile_contnt_row row-eq-height">
        <div class="col-lg-8">
          <div class="left_my_profil form-horizontal"><!-- start form -->
            <div class="row my_prof_box my_prof_box1"><!-- box1 start -->
              <div class="col-sm-6">
                <div class="form-group">
                  <label for="last_name"  class="control-label col-xs-5">Last Name<span class="red_txt">*</span>:</label>
                  <div class="col-xs-7">
                    <input id="last_name" type="text" class="form-control"  placeholder="Please Enter Last Name" name="last_name" value="<?=$profile->last_name?>">
                    <span class="form-control-feedback "></span>
                    <div class="last_name error-msg red_txt "></div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="first_name"  class="control-label col-xs-5">First Name<span class="red_txt">*</span>:</label>
                  <div class="col-xs-7">
                    <input id="first_name" type="text" class="form-control" placeholder="Please Enter First Name " name="first_name" value="<?=$profile->first_name?>" >
                    <span class="form-control-feedback "></span>
                    <div class="first_name error-msg red_txt "></div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="title"  class="control-label col-xs-5">Age<span class="red_txt">*</span>:</label>
                  <div class="col-xs-7">
                    <select class="form-control" name="cage" id="cage">
                      
                      <?php 
                      for($i=18; $i<=65; $i++){
                       
                        $cor = $i == $profile->age? 'selected="selected"' : '';
                        echo '<option value="'.$i.'"'.$cor.'">'.$i.'</option>';
                      }
                      ?>
                    </select>
                    
                    <div class="cage error-msg red_txt "></div>
                  </div>
                </div>
                
                <div class="form-group">
                  <label for="household_income"  class="control-label col-xs-5"> Household Income<span class="red_txt">*</span>:</label>
                  <div class="col-xs-7">
                    <input type="text" class="form-control" id="household_income" placeholder="Please enter your House hold income" name="household_income" value="<?=$profile->household_income?>">
                    <span class="form-control-feedback "></span>
                    
                    <div class="household_income error-msg red_txt "></div>
                  </div>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                  <label for="state"  class="control-label col-xs-5">State<span class="red_txt">*</span>:</label>
                  <div class="col-xs-7">
                   <select class="form-control" name="state"  required>
                     <?php

                     foreach($state as $row){
                       
                      $cor = $row->id == $profile->state? 'selected="selected"' : '';
                      echo '<option value="'.$row->id.'" '.$cor.'>'.$row->name.'</option>';
                    }
                    ?>
                  </select>
                  <span class="form-control-feedback "></span>
                  <div class="state error-msg red_txt "></div>
                </div>
              </div>
              <div class="form-group">
                <label for="credit_license"  class="control-label col-xs-5">Suburb<span class="red_txt">*</span>:</label>
                <div class="col-xs-7">
                  <input id="suburb" type="text" class="form-control" placeholder="Please Enter suburb" name="suburb"  value="<?=$profile->suburb?>">
                  <span class="form-control-feedback "></span>
                  <div class="suburb error-msg red_txt "></div>

                </div>
              </div>
              
              
            </div>
          </div><!-- box1 end -->
          <br>
        </div><!-- box2 end -->
        <br>
        <div class="left_my_profil form-horizontal">
          <div class="row my_prof_box my_prof_box3"><!-- box3 start -->
            <div class="col-sm-6">
              <div class="form-group">
                <label for="email_address"  class="control-label col-xs-5">Email Address:</label>
                <div class="col-xs-7">
                  <span id="email_address"><?=$profile->address?></span>
                  <span class="form-control-feedback "></span>
                  <div class=" error-msg red_txt "></div>
                </div>
              </div>
              <!-- <div class="form-group">
                <label for="mobile"  class="control-label col-xs-5">Mobile:</label>
                <div class="col-xs-7">
                  <select class="form-control" name="phone_code" required>
                    <option>+61</option>
                    <?php
                    foreach($phone_code as $row){
                      $cor = $row['code'] == '+61' ? 'selected="selected"' : '';
                      echo '<option '.$cor.'>'.'+'.$row['code'].'</option>';
                      
                    }
                    ?>
                  </select>
                </div>
              </div> -->
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label for="website"  class="control-label col-xs-5">Home:</label>
                <div class="col-xs-2 mobile_phone_code">
                  <select class="form-control " name="phone_code" required>
                    <option>+61</option>
                    <?php

                    foreach($phone_code as $row){
                      $cor = $row['code'] == '+61' ? 'selected="selected"' : '';
                      echo '<option '.$cor.'>'.'+'.$row['code'].'</option>';
                      
                    }
                    ?>
                  </select>
                </div>
                <div class="col-xs-5 mobile_phone_code">
                  <input type="text" class="form-control" name="home" placeholder="Please enter your home no" >
                </div>
                 
              </div>
              <div class="form-group">
                <label for="website"  class="control-label col-xs-5">Mobile:</label>
                <div class="col-xs-2 mobile_phone_code">
                  <select class="form-control " name="phone_code" required>
                    <option>+61</option>
                    <?php
                    foreach($phone_code as $row){
                      $cor = $row['code'] == '+61' ? 'selected="selected"' : '';
                      echo '<option '.$cor.'>'.'+'.$row['code'].'</option>';
                      
                    }
                    ?>
                  </select>
                </div>
                <div class="col-xs-5 mobile_phone_code">
                  <input type="text" class="form-control" name="mobile" placeholder="Please enter your mobile no" >
                </div>
              </div>
            </div>
          </div>
        </div> 
        <div class="row">
          <div class="col-xs-12">
            <div class="form-group text-center">
              <button type="submit"  class="btn">SAVE CHANGES</button>
            </div>
          </div>
        </div>
      </div>

      <div class="col-lg-4">
        <div class="rght_my_profil">
          <div class="row my_prof_rght_box my_prof_rght_box1">
            <div class="col-sm-12 text-center">
              <div class="upload_prof_photo">
                <?php if(isset($profile->profile_img_path) && !empty($profile->profile_img_path)){
                  echo '<img src="'.base_url('uploads/profilepics').'/'.$profile->profile_img_path.'" style="height:212px;width:249px;">';
                }else{
                  echo '<span class="fa fa-user">'.'</span>';
                   echo '<h4>Upload Profile Photo</h4>';
                }?>
               
                <input id="profile_img_path" type="file" name ="profile_img_path">
               <h4>Upload Profile Photo</h4>
              </div> 
            </div>
          </div><!-- right_box1 start-->
        </div>
        <div class="rght_my_profil">
        <h3>Favorite Broker</h3>
          <table class="table table-bordered">
          <?php 
            if(isset($favouritcustomer) && !empty($favouritcustomer)){
            foreach($favouritcustomer as $row){
              $broker = $this->Customer_model->getUserById($row->broker_id);
          ?>
            <tr>
              <td><?='<span class="nam_ltr_circl">'.strtoupper(substr($broker->first_name, 0, 1)).''.strtoupper(substr($broker->last_name, 0, 1)).'</span>'?>
              </td>
              <td>
              <a href="<?=base_url('Customer/broker_profile/'.$broker->id)?>">
              <?=$broker->first_name.' '.$broker->last_name?>
              </a>
              </td>
            </tr>
            <?php } 

          }else{
            echo '<tr>
                <td>There is no data</td>
            </tr>';
          }
            ?>
          </table>
        </div>
      </div> 
  </form>

  <div class="row">
    <div class="col-xs-12 deactivat_my_acc_link">
      <div class="form-group ">
        <a href="#">Deactivate My Account</a>
      </div>
    </div>
  </div>
</div>
</div>
</section>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.css" rel="stylesheet"/>
<script type="text/javascript">

function formValidation() {
 var mobile = document.forms["CustomerForm"]["mobile"].value;
 if (mobile.length<8){
  $('.mobile').empty();
  $('.mobile').append('8 character required');
}
var x = document.forms["CustomerForm"]["post_code"].value;

var last_name = document.forms["CustomerForm"]["last_name"].value;
var first_name = document.forms["CustomerForm"]["first_name"].value;
var cage = document.forms["CustomerForm"]["cage"].value;
var household_income = document.forms["CustomerForm"]["household_income"].value;
var state = document.forms["CustomerForm"]["state"].value;
var suburb = document.forms["CustomerForm"]["suburb"].value;*/

/* var y = document.CustomerForm.mobile.value;*/
     /* if(first_name == ""){
        $('.first_name').empty();
        $('.first_name').append('First Name is required');
      }
      if(last_name == ""){
        $('.last_name').empty();
        $('.last_name').append('Last Name is required');
      }
      if(cage == ""){
        $('.cage').empty();
        $('.cage').append('Age is required');
      }
      if(household_income == ""){
        $('.household_income').empty();
        $('.household_income').append('household income is required');
      }
       if(state == ""){
        $('.state').empty();
        $('.state').append('state is required');
      }
      if(suburb == ""){
        $('.suburb').empty();
        $('.suburb').append('suburb is required');
      }
       if (mobile.length>8){
          $('.mobile').empty();
          $('.mobile').append('state is required');
        }*/
        
      }
      </script>