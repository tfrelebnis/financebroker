<!DOCTYPE html>
<html lang="en">
<head>
	<title>Finance</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" type="text/css">
	<script src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
	<link rel="stylesheet" href="<?=base_url('assets/front/css/style.css')?>">
	<script type="text/javascript" src="<?=base_url('assets/front/js/script.js')?>"></script>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/css/bootstrap-select.min.css">
	<!-- Latest compiled and minified JavaScript -->
	
</head>
<body>
<!-- nav -->
<div class="top_nav">
	<div class="container">
		<div class="row top_logo_n_sign_up_logn">
			<div class="col-sm-6">
				<div class="finance_logo_img">
					<a href="#">
						<div class="top_headr_logo"></div>
			      		<!-- <img src="<?=base_url('assets/front/img/logo/logo_jpg.jpg')?>" width="100%"> -->
			      	</a>
			    </div>
		    </div>
		    <div class="col-sm-6 hidden-xs">
	    	 	<ul class="nav navbar-nav navbar-right">
			        <li>
			        <?php 
			        	if($this->session->userdata('name') != null && !empty($this->session->userdata('name'))){
			        		$login = '';
			        	}else{
			        		echo '<a href="#" class="btn" data-toggle="modal" data-target="#custopmer_broker_conf_mdl">
			        		<!-- <span class="glyphicon glyphicon-user"></span> --> <span class="hidden-xs">Sign Up</span>
			        	</a>';
			        	}
			        ?>
			        </li>
			        <li>
			        <?php 
			        	if($this->session->userdata('name') != null && !empty($this->session->userdata('name'))){
			        		echo '<a href="'.$this->session->userdata('profile').'">
						        	<span class="login_top"><span class="fa fa-user"></span> '.$this->session->userdata('name').'</span>
						        </a>';
			        	}else{
			        		echo '<a href="#" data-toggle="modal" data-target="#home_login">
						        	<span class="login_top"><span class="fa fa-lock"></span> Login</span>
						        </a>';
			        	}
			        ?>
			        	
			        </li>
		      	</ul>
		    </div>
		</div>
	</div>
	<nav class="navbar navbar-inverse nav_main">
	  <div class="container-fluid">
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span> 
	      </button>
	      <div class="navbar-brand visible-xs">
	      	<!-- <form class="search_form">
				<input type="text" class="form-control" Placeholder="search here...">
			</form> -->
	      	<!-- <a href="#">
	      		<div class="top_headr_logo"></div>
	      		<img src="<?=base_url('assets/front/img/logo/logo_jpg.jpg')?>" width="100%"> 
	      	</a> -->
	      </div>
	    </div>
	    <div class="collapse navbar-collapse" id="myNavbar">
	      	<ul class="nav navbar-nav mobile_menu_bg main_links">
		        <li class=""><a href="#">How it Works</a></li>
		        <li  class="menu_search_box"><a href="#">Search Now</a>
			    	<!-- <span class="search_box_menu">
			        	<span class="fa fa-search"></span>
				        <input type="Search" placeholder="Search..." class="form-control" />       
				    </span> -->
		        </li> 
		        <li><a href="#"  data-toggle="modal" data-target="#finance_broker_hub">Finance Broker Hub</a></li>
		        <!-- <li ><a href="#">Customer</a></li>
		        <li><a href="#">Finance Brokers</a></li>-->
		        <!-- <li class="hidden-xs">
		        	<form class="search_form">
						<input type="text" class="form-control" Placeholder="search here...">
					</form>
				</li>   -->
	      	</ul>
	      	<ul class="nav navbar-nav navbar-right footer_social ">
				<li><a href="#"><span class="fa fa-facebook"></span></a></li>
				<li><a href="#"><span class="fa fa-twitter"></span></a></li>
				<li><a href="#"><span class="fa fa-linkedin"></span></a></li>
				<li><a href="#"><span class="fa fa-instagram"></span></a></li>
			</ul>
	      	<ul class="nav navbar-nav navbar-right visible-xs">
		        <li>
		        	<a href="#" class="btn" data-toggle="modal" data-target="#custopmer_broker_conf_mdl">
		        		<span class="glyphicon glyphicon-user"></span> Sign Up
		        	</a>
		        </li>
		        <li><a href="#" data-toggle="modal" data-target="#home_login"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
	      	</ul>
	      	
	    </div>
	  </div>
	</nav>

</div>
<!-- nav -->
<?php 
    if(isset($message) && !empty($message)){
        echo '<script type="text/javascript">bootbox.alert({
	    	message: "'.$message.'",
	    	className: "message-boot"
		});
	</script>';
    }
    if(isset($warning) && !empty($warning)){
        echo '<script type="text/javascript">bootbox.alert({
	    	message: "'.$warning.'",
	    	className: "warning-boot"
		});
	</script>';
    }
    if(isset($error) && !empty($error)){
        echo '<script type="text/javascript">bootbox.alert({
	    	message: "'.$error.'",
	    	className: "error-boot"
		});
	</script>';
    }
?>

