<br>
<footer class="footer1">
	<div class="container">
		<div class="row">
			<div class="col-xs-6 col-sm-3">
				<div class="footer_content">
					<h3>Finance Brokers</h3>
					<ul>
						<li><a href="#">Benefits of being part of TFR</a></li>
						<li><a href="#">How our ratings work</a></li>
						<li><a href="#">Code of Conduct   </a></li>
						<li><a href="#">Pricing</a></li>
						<li><a href="#">FAQ</a></li>
					</ul>
				</div>
			</div>
			<div class="col-xs-6 col-sm-3">
				<div class="footer_content">
					<h3>Industry News</h3>
					<ul>
						<li><a href="#">Latest News</a></li>
						<li><a href="#">RBA </a></li>
						<li><a href="#">MFAA</a></li>
						<li><a href="#">FBAA</a></li>
					</ul>
				</div>
			</div>
			<div class="col-xs-6 col-sm-3">
				<div class="footer_content">
					<h3>Tools</h3>
					<ul>
						<li><a href="#">Loans Calculator</a></li>
						<li><a href="#">Stamp Duty Calculator</a></li>
						<li><a href="#">Borrowing Capacity Calculator</a></li>
						<li><a href="#">Budget Calculator</a></li>
					</ul>
				</div>
			</div>
			<div class="col-xs-6 col-sm-3">
				<div class="footer_content">
					<h3>About Us</h3>
					<ul>
						<li><a href="#">Our Journey</a></li>
						<li><a href="#">Meet the team </a></li>
						<li><a href="#">Terms of Service</a></li>
						<li><a href="#">Privacy Policy</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</footer>
<footer class="footer2">
	<div class="container">
		<div class="row">
			<div class="col-sm-4">
				<p><small>Copyright &copy; All rights reserved</small></p>
			</div>
			<div class="col-sm-4">
				<ul class="footer_social">
					<li><a href="#"><span class="fa fa-facebook"></span></a></li>
					<li><a href="#"><span  class="fa fa-twitter"></span></a></li>
					<li><a href="#"><span  class="fa fa-linkedin"></span></a></li>
					<li><a href="#"><span  class="fa fa-instagram"></span></a></li>
				</ul>
			</div>
			<div class="col-sm-4">

			</div>
		</div>
	</div>
</footer>



<!-- =====modal-for====Login==btn======== -->
<div id="home_login" class="modal fade" role="dialog">
	<div class="modal-dialog">
	    <!-- Modal content-->
	    <form class="modal-content form-horizontal" method="post" action="<?=base_url('home/brokerLogin')?>">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h3 class="modal-title text-center">Welcome Back</h3>
	      </div>
	      <div class="modal-body">
	        <div class="form-group">
	            <label for="log-email"  class="control-label col-xs-12">Email</label>
	            <div class="col-xs-12">
	              <input id="log-email"  type="text" class="form-control" placeholder="Please Enter your Email" name="email" required>
	            </div>
          	</div>
          	<div class="form-group">
                <label for="log-pas"  class="control-label col-xs-12">Password</label>
                <div class="col-xs-12">
                  <input id="log-pas"  type="text" class="form-control" placeholder="Please Enter your password" type="password" name="password" required>
                </div>
          	</div>
          	<div class="form-group text-left">
          		<div class="col-xs-12">
          			<a href="#">Forget my password</a>
          		</div>
          	</div>
          	<div class="form-group text-center">
	        	<button type="submit" class="btn">SIGN IN</button>
        	</div>
	      </div>
	    </form>
	</div>
</div>
<!-- ====login===end -->

<!-- =====modal-for====sign-up==btn======== -->
<div id="custopmer_broker_conf_mdl" class="modal fade" role="dialog">
	<div class="modal-dialog">
	    <!-- Modal content-->
	    <form class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title text-enter">Ready to sign up? It's free!</h4>
	      </div>
	      <div class="modal-body custopmer_broker_conf text-enter">
	        <p>Are you a Borrower or Finance Broker?</p>
	        <button type="button" class="btn" data-toggle="modal" data-target="#customer_sign_up">Borrower</button>
	        <button type="button"  class="btn"  data-toggle="modal" data-target="#broker_sign_up">Finance Broker</button>
	      </div>
	    </form>
	</div>
</div>
<!-- =====modal-for====sign-up==btn======== -->
<div id="broker_sign_up" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <form class="modal-content form-horizontal" name="myName" method="post" action="<?=base_url('home')?>" onsubmit="return validateForm()">
	  	<div class="modal-header">
		    <button type="button" class="close" data-dismiss="modal">&times;</button>
		    <h4 class="modal-title text-center"><b>Ready to sign up?</b></h4>
		    <h5 class=" text-center">Please fill in the form to sign up</h5>
	  	</div>	
  		<div class="modal-body">
  			<div class="form-group">
				<label class="control-label col-xs-12" for="last_name">Last Name<span class="red_txt">*</span></label>
				<div class="col-xs-12">
					<input type="text" class="form-control" id="last_name" placeholder="Please enter your last name" name="last_name">
					<span class="form-control-feedback"></span>
					<div class="last_name error-required red_txt"></div>
				</div>
			</div>
		    <div class="form-group">
				<label class="control-label col-xs-12" for="first_name">First Name<span class="red_txt">*</span></label>
				<div class="col-xs-12">
					<input type="text" class="form-control" id="first_name" placeholder="Please enter your first name" name="first_name">
					<span class="form-control-feedback "></span>
					<div class="first_name error-required red_txt"></div>
				</div>
		    </div>
		    <div class="form-group">
				<label class="control-label col-xs-12" for="state">State<span class="red_txt">*</span></label>
				<div class="col-xs-12">
					<select class="form-control" id="state" name="state">
						<option  value="-1">Please select your state</option>
						<?php 
							if(isset($state) && !empty($state)){
								foreach($state as $row){
									echo '<option value="'.$row->id.'">'.$row->name.'</option>';
								}
							}
						?>
					</select>
					<span class="form-control-feedback state"></span>
					<div class="state error-required red_txt"></div>
				</div>
		    </div>
		    <div class="form-group">
				<label class="control-label col-xs-12" for="business_name">Business Name<span class="red_txt">*</span></label>
				<div class="col-xs-12">
					<input type="text" class="form-control" id="business_name" placeholder="Please enter your business name" name="business_name">
					<span class="form-control-feedback "></span>
					<div class="business_name error-required red_txt"></div>
				</div>
		    </div>
		    <div class="form-group">
				<label class="control-label col-xs-12" for="credit_license">Credit Licence Number<span class="red_txt">*</span></label>
				<div class="col-xs-12">
					<input type="text" class="form-control" id="credit_license" placeholder="Please enter your credit licence number" name="credit_license">
					<span class="form-control-feedback "></span>
					<div class="credit_license error-required red_txt"></div>
				</div>
		    </div>
		    <div class="form-group">
				<label class="control-label col-xs-12" for="business_specification">Business Specialisations<span class="red_txt">*</span></label>
				<div class="col-xs-12">
					<select class="form-control" id="business_specification" name="business_specification">
						<option value="-1">Please enter your business speciations</option>
						<option >1</option>
						<option>2</option>
					</select>
					<span class="form-control-feedback "></span>
					<div class="business_specification error-required red_txt"></div>
				</div>
		    </div>
	        <div class="form-group">
				<label class="control-label col-xs-12" for="email_address"> Email address<span class="red_txt">*</span></label>
				<div class="col-xs-12">
					<input type="text" class="form-control" id="email_address" placeholder="Please enter your email address" name="address">
					<span class="form-control-feedback "></span>
					<div class="email_address error-required red_txt"></div>
				</div>
		    </div>
		    <div class="form-group">
				<label class="control-label col-xs-12" for="password">Password<span class="red_txt">*</span></label>
				<div class="col-xs-12">
					<input type="password" class="form-control" id="password" placeholder="Password must be minimum 5 of characters" name="password">
					<span class="form-control-feedback password" id="msg"></span>
					<div class="password error-required red_txt"></div>
				</div>
		    </div>
		    <div class="form-group">
				<label class="control-label col-xs-12" for="rpassword">Retype Password<span class="red_txt">*</span></label>
				<div class="col-xs-12">
					<input type="password" class="form-control" id="rpassword" placeholder="Please retype your password" name="rpassword">
					<span class="form-control-feedback"></span>
				</div>
		    </div>
		    <div class="form-group">
				<label class="control-label col-xs-12">Type the code below<span class="red_txt">*</span></label>
				<div class="col-xs-12">
					<input type="text" class="form-control" id="" placeholder="Enter Captcha" name="">
					<span class="form-control-feedback"></span>
				</div>
		    </div>
		    <div class="form-group">
		    	<div class="captcha_4_broker_sign_up">

		    	</div>
		    </div>
		    <div class="checkbox  text-center">
	      		<label><input type="checkbox" name="remember" id="terms"> By signing up, I agree to the <a href="#">Terms & Conditions</a> and <a href="#">Privacy Policy</a></label>
	      		<span class="form-control-feedback"></span>
		    	<div class="terms error-required red_txt"></div>
				</div>
		    <div class="form-group text-center">
		    	<button class="broker_sign_up_btn btn" id="submit_broker" type="submit">Submit</button>
		    </div>
  		</div>
    </form>
  </div>
</div>
<div id="customer_sign_up" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <form class="modal-content form-horizontal" name="myName" method="post" action="<?=base_url('home')?>" onsubmit="return validateForm()">
	  	<div class="modal-header">
		    <button type="button" class="close" data-dismiss="modal">&times;</button>
		    <h4 class="modal-title text-center"><b>Ready to sign up?</b></h4>
		    <h5 class=" text-center">Please fill in the form to sign up</h5>
	  	</div>	
  		<div class="modal-body">
  			<div class="form-group">
				<label class="control-label col-xs-12" for="last_name">Last Name<span class="red_txt">*</span></label>
				<div class="col-xs-12">
					<input type="text" class="form-control" id="last_name" placeholder="Please enter your last name" name="last_name">
					<span class="form-control-feedback"></span>
					<div class="last_name error-required red_txt"></div>
				</div>
			</div>
		    <div class="form-group">
				<label class="control-label col-xs-12" for="first_name">First Name<span class="red_txt">*</span></label>
				<div class="col-xs-12">
					<input type="text" class="form-control" id="first_name" placeholder="Please enter your first name" name="first_name">
					<span class="form-control-feedback "></span>
					<div class="first_name error-required red_txt"></div>
				</div>
		    </div>
		    <div class="form-group">
				<label class="control-label col-xs-12" for="state">State<span class="red_txt">*</span></label>
				<div class="col-xs-12">
					<select class="form-control" id="state" name="state">
						<option  value="-1">Please select your state</option>
						<?php 
							if(isset($state) && !empty($state)){
								foreach($state as $row){
									echo '<option value="'.$row->id.'">'.$row->name.'</option>';
								}
							}
						?>
					</select>
					<span class="form-control-feedback state"></span>
					<div class="state error-required red_txt"></div>
				</div>
		    </div>
		    <div class="form-group">
				<label class="control-label col-xs-12" for="business_name">Business Name<span class="red_txt">*</span></label>
				<div class="col-xs-12">
					<input type="text" class="form-control" id="business_name" placeholder="Please enter your business name" name="business_name">
					<span class="form-control-feedback "></span>
					<div class="business_name error-required red_txt"></div>
				</div>
		    </div>
		    <div class="form-group">
				<label class="control-label col-xs-12" for="credit_license">Credit Licence Number<span class="red_txt">*</span></label>
				<div class="col-xs-12">
					<input type="text" class="form-control" id="credit_license" placeholder="Please enter your credit licence number" name="credit_license">
					<span class="form-control-feedback "></span>
					<div class="credit_license error-required red_txt"></div>
				</div>
		    </div>
		    <div class="form-group">
				<label class="control-label col-xs-12" for="business_specification">Business Speciations<span class="red_txt">*</span></label>
				<div class="col-xs-12">
					<select class="form-control" id="business_specification" name="business_specification">
						<option value="-1">Please enter your business speciations</option>
						<option >1</option>
						<option>2</option>
					</select>
					<span class="form-control-feedback "></span>
					<div class="business_specification error-required red_txt"></div>
				</div>
		    </div>
	        <div class="form-group">
				<label class="control-label col-xs-12" for="email_address"> Email address<span class="red_txt">*</span></label>
				<div class="col-xs-12">
					<input type="text" class="form-control" id="email_address" placeholder="Please enter your email address" name="address">
					<span class="form-control-feedback "></span>
					<div class="email_address error-required red_txt"></div>
				</div>
		    </div>
		    <div class="form-group">
				<label class="control-label col-xs-12" for="password">Password<span class="red_txt">*</span></label>
				<div class="col-xs-12">
					<input type="password" class="form-control" id="password" placeholder="Password must be minimum 5 of characters" name="password">
					<span class="form-control-feedback password" id="msg"></span>
					<div class="password error-required red_txt"></div>
				</div>
		    </div>
		    <div class="form-group">
				<label class="control-label col-xs-12" for="rpassword">Retype Password<span class="red_txt">*</span></label>
				<div class="col-xs-12">
					<input type="password" class="form-control" id="rpassword" placeholder="Please retype your password" name="rpassword">
					<span class="form-control-feedback"></span>
				</div>
		    </div>
		    <div class="form-group">
				<label class="control-label col-xs-12">Type the code below<span class="red_txt">*</span></label>
				<div class="col-xs-12">
					<input type="text" class="form-control" id="" placeholder="Enter Captcha" name="">
					<span class="form-control-feedback"></span>
				</div>
		    </div>
		    <div class="form-group">
		    	<div class="captcha_4_broker_sign_up">

		    	</div>
		    </div>
		    <div class="checkbox  text-center">
	      		<label><input type="checkbox" name="remember" id="terms"> By signing up, I agree to the <a href="#">Terms & Conditions</a> and <a href="#">Privacy Policy</a></label>
	      		<span class="form-control-feedback"></span>
		    	<div class="terms error-required red_txt"></div>
				</div>
		    <div class="form-group text-center">
		    	<button class="broker_sign_up_btn btn" id="submit_broker" type="submit">Submit</button>
		    </div>
  		</div>
    </form>
  </div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$('#rpassword').change(function (){
			var password = $('#password').val();
			var rpassword = $('#rpassword').val();
			if(password == rpassword){
				$('#rpassword').css("border", "2px solid green");
				$('#password').css("border", "2px solid green");
			}else{
				$('#rpassword').css("border", "2px solid red");
				$('#password').css("border", "2px solid red");
				return false;
			}
		});
		$('#password').keyup(function() {
			var password = $('#password').val();
			var msg = '';
			if(password.length >= '6' && password.length <= '100'){
				msg = '<p style="color:green;">Strong</p>';
			}
			if(password.length >= '3' && password.length <= '6'){
				msg = '<p style="color:yellow;">Medium</p>';
			}
			if(password.length >= '' && password.length <= '3'){
				msg = '<p style="color:red;">Poor</p>';
			}
			$('#msg').empty();
			$('#msg').append(msg);
		});

		$('.form-control').change(function (){
			$('.error-required').empty();
		});

		$('#email_address').change(function (){
			var email = $('#email_address').val();
			$.ajax({
				url: "<?=base_url('Home/validateEmail')?>",
				type: 'POST',
				dataType: 'json',
				data: {email: email},
			})
			.done(function(result) {
				console.log("success");
				console.log(result);
				if(result.message == "fine"){
					$('#email_address').css('border', '2px solid green');
					document.getElementById("submit_broker").removeAttribute("disabled");
					return true;
				}else{
					$('#email_address').css('border', '2px solid red');
					$('.email_address').append(result.message);
					document.getElementById("submit_broker").disabled = true; 
					return false;
				}
			})
			.fail(function(e) {
				console.log("error");
				console.log(e);
			})
			
		});

		$('#business_name').change(function (){
			var business_name = $('#business_name').val();
			$.ajax({
				url: "<?=base_url('Home/validateBusinessName')?>",
				type: 'POST',
				dataType: 'json',
				data: {bname: business_name},
			})
			.done(function(result) {
				console.log("success");
				console.log(result);
				if(result.message == "fine"){
					$('#business_name').css('border', '2px solid green');
					document.getElementById("submit_broker").removeAttribute("disabled");
					return true;
				}else{
					$('#business_name').css('border', '2px solid red');
					$('.business_name').append(result.message);
					document.getElementById("submit_broker").disabled = true; 
					return false;
				}
			})
			.fail(function(e) {
				console.log("error");
				console.log(e);
			})
			
		});
	});

	function validateForm() {
		var last_name = document.forms["myName"]["last_name"].value;
		if (last_name == "") {
		    $('.last_name').empty();
			$('.last_name').append('Last Name is required');
			document.forms["myName"]["last_name"].focus();
			return false;
		}
		var first_name = document.forms["myName"]["first_name"].value;
		if (first_name == "") {
		    $('.first_name').empty();
			$('.first_name').append('First Name is required');
		    document.forms["myName"]["first_name"].focus();
			return false;
		}
		var business_name = document.forms["myName"]["business_name"].value;
		if (business_name == "") {
		    $('.business_name').empty();
			$('.business_name').append('Business Name is required');
		    document.forms["myName"]["business_name"].focus();
			return false;
		}
		var credit_license = document.forms["myName"]["credit_license"].value;
		if (credit_license == "") {
		    $('.credit_license').empty();
			$('.credit_license').append('Credit License is required');
		    document.forms["myName"]["credit_license"].focus();
			return false;
		}
		var business_specification = document.forms["myName"]["business_specification"].value;
		if (business_specification == "") {
		    $('.business_specification').empty();
			$('.business_specification').append('Business Specification is required');
		    document.forms["myName"]["business_specification"].focus();
			return false;
		}
		var email_address = document.forms["myName"]["email_address"].value;
		if (email_address == "") {
		    $('.email_address').empty();
			$('.email_address').append('Email Address is required');
		    document.forms["myName"]["email_address"].focus();
			return false;
		}
		var atpos = email_address.indexOf("@");
	    var dotpos = email_address.lastIndexOf(".");
	    if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email_address.length) {
	        $('.email_address').empty();
			$('.email_address').append('Please enter a valid email address');
		    document.forms["myName"]["email_address"].focus();
			return false;
	    }


		var password = document.forms["myName"]["rpassword"].value;
		if (password == "") {
		    $('.password').empty();
			$('.password').append('Password is required');
		    document.forms["myName"]["rpassword"].focus();
			return false;
		}

		var password = document.forms["myName"]["password"].value;
		if (password == "") {
		    $('.password').empty();
			$('.password').append('Password is required');
		    document.forms["myName"]["password"].focus();
			return false;
		}

		if(document.forms["myName"]["password"].length < 5){
			$('.password').empty();
			$('.password').append('It must be above 5 characters');
		    document.forms["myName"]["password"].focus();
			return false;
		}

		if(document.forms["myName"]["rpassword"].length == password){
			$('.password').empty();
			$('.password').append('Password could not match');
		    document.forms["myName"]["rpassword"].focus();
			return false;
		}

		var terms = document.forms["myName"]["terms"].checked;
		if (terms == false) {
			$('.terms').empty();
			$('.terms').append('You need to agree with our terms and conditions');
		    document.forms["myName"]["terms"].focus();
			return false;
		}
	}
</script>

<style type="text/css" media="screen">
#customer_sign_up .form-horizontal .control-label {
    text-align: left;
}	
#customer_sign_up .modal-dialog {
    max-width: 450px;
}

</style>
</body>
</html>
 

