<br>
<footer class="footer1">
	<div class="container">
		<div class="row">
			<div class="col-xs-6 col-sm-3">
				<div class="footer_content">
					<h3>Borrowers</h3>
					<ul>
						<li><a href="#">Benefits of using a Finance Broker</a></li>
						<li><a href="#">Selecting the right Finance Broker</a></li>
						<li><a href="#">Know your Finance terms </a></li>
						<li><a href="#">FAQ</a></li>
					</ul>
				</div>
			</div>
			<div class="col-xs-6 col-sm-3">
				<div class="footer_content">
					<h3>Industry News</h3>
					<ul>
						<li><a href="#">Latest News</a></li>
						<li><a href="#">RBA </a></li>
						<li><a href="#">MFAA</a></li>
						<li><a href="#">FBAA</a></li>
					</ul>
				</div>
			</div>
			<div class="col-xs-6 col-sm-3">
				<div class="footer_content">
					<h3>Tools</h3>
					<ul>
						<li><a href="#">Loans Calculator</a></li>
						<li><a href="#">Stamp Duty Calculator</a></li>
						<li><a href="#">Borrowing Capacity Calculator</a></li>
						<li><a href="#">Budget Calculator</a></li>
					</ul>
				</div>
			</div>
			<div class="col-xs-6 col-sm-3">
				<div class="footer_content">
					<h3>About Us</h3>
					<ul>
						<li><a href="#">Our Journey</a></li>
						<li><a href="#">Meet the team </a></li>
						<li><a href="#">Terms of Service</a></li>
						<li><a href="#">Privacy Policy</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</footer>
<footer class="footer2">
	<div class="container">
		<div class="row">
			<div class="col-sm-4">
				<p><small>Copyright &copy; All rights reserved</small></p>
			</div>
			<div class="col-sm-4">
				<!-- <ul class="footer_social">
					<li><a href="#"><span class="fa fa-facebook"></span></a></li>
					<li><a href="#"><span  class="fa fa-twitter"></span></a></li>
					<li><a href="#"><span  class="fa fa-linkedin"></span></a></li>
					<li><a href="#"><span  class="fa fa-instagram"></span></a></li>
				</ul> -->
			</div>
			<div class="col-sm-4">

			</div>
		</div>
	</div>
</footer>

<!-- =====modal-for==== Finance Broker Hub ======== -->
<div id="finance_broker_hub" class="modal fade" role="dialog">
	<div class="modal-dialog">
	    <!-- Modal content-->
	    <form class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	      </div>
	      <div class="modal-body  text-center">
	        <p>"Please confirm you are an existing Finance Broker looking to be involved with The Finance Revolution"</p>
	        <a href="#" class="btn">Yes</a>
	        <button type="button"  class="btn"  data-dismiss="modal">No</button>
	      </div>
	    </form>
	</div>
</div>
<!-- =====modal-for====    Finance Broker Hub ======== -->

<!-- =====modal-for==== Finance Broker Hub ======== -->
<div id="cancel_subscription" class="modal fade" role="dialog">
	<div class="modal-dialog">
	    <!-- Modal content-->
	    <form class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	      </div>
	      <div class="modal-body  text-center">
	        <p>Are you sure you want to cancel you subscription and lose access to all customer connections, messages and fields?</p>
	        <a href="<?= base_url('Home/cancel_subscription_by_id/').$this->uri->segment(3) ?>" class="btn">Yes</a>
	        <button type="button"  class="btn"  data-dismiss="modal">No</button>
	      </div>
	    </form>
	</div>
</div>
<!-- =====modal-for====    Finance Broker Hub ======== -->

<!-- =====modal-for====forget_password========== -->
<div id="forget_password" class="modal fade" role="dialog">
	<div class="modal-dialog">
	    <!-- Modal content-->
	    <form class="modal-content form-horizontal">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h3 class="modal-title text-center">Forgot Password</h3>
	      </div>
	      <div class="modal-body">
	        <div class="form-group">
	            <label for="log-email"  class="control-label col-xs-12">Email</label>
	            <div class="col-xs-12">
	              <input id="log-email"  type="text" class="form-control" placeholder="Please enter your email" name="email" required>
	              <span class="col-sm-12 red_txt hidden" id="forgot_txt_span"><small id="forgot_txt_small">field is required</small></span>
          	<div class="form-group text-left">
	            </div>
          	</div>
          		<div class="col-xs-12">
          			<label>This must be the email address you have registered with</label>
          		</div>
          	</div>
          	<div class="form-group text-center">
	        	<button type="button" class="forget_btn" id="forgot_submit" onclick="enable();">Submit</button>
        	</div>
	      </div>
	    </form>
	</div>
</div>
<!-- ====forget_password===end -->


<!-- =====modal-for====Login==btn======== -->
<div id="home_login" class="modal fade" role="dialog">
	<div class="modal-dialog">
	    <!-- Modal content-->
	    <form class="modal-content form-horizontal" method="post" action="<?=base_url('home/brokerLogin')?>">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h3 class="modal-title text-center">Welcome Back</h3>
	      </div>
	      <div class="modal-body">
	        <div class="form-group">
	            <label for="log-email"  class="control-label col-xs-12">Email</label>
	            <div class="col-xs-12">
	              <input id="log-email"  type="text" class="form-control" placeholder="Please Enter your Email" name="email" required>
	            </div>
          	</div>
          	<div class="form-group">
                <label for="log-pas"  class="control-label col-xs-12">Password</label>
                <div class="col-xs-12">
                  <input id="log-pas"  type="password" class="form-control" placeholder="Please Enter your password" type="password" name="password" required>
                </div>
          	</div>
          	<div class="form-group text-left">
          		<div class="col-xs-12">
          			<a href="#" data-toggle="modal" data-dismiss="modal" data-target="#forget_password">Forgot my password</a>
          		</div>
          	</div>
          	<div class="form-group text-center">
	        	<button type="submit" class="btn">SIGN IN</button>
        	</div>
	      </div>
	    </form>
	</div>
</div>
<div id="after_login" class="modal fade" role="dialog">
	<div class="modal-dialog">
	    <!-- Modal content-->
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		        <h3 class="modal-title text-center">Welcome <?=$this->session->userdata('name')?></h3>
		      </div>
		      <div class="modal-body">
		        <div class="form-group text-center">
		        	<a class="btn" href="<?=base_url('Home/logout')?>">Logout</a>
	        	</div>
		      </div>
		</div>
	</div>
</div>

<!-- ====login===end -->

<!-- =====modal-for====sign-up==btn======== -->
<div id="custopmer_broker_conf_mdl" class="modal fade" role="dialog">
	<div class="modal-dialog">
	    <!-- Modal content-->
	    <form class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title text-center">Ready to sign up? It's free!</h4>
	      </div>
	      <div class="modal-body custopmer_broker_conf text-center">
	        <p>Are you a Borrower or Finance Broker?</p>
	        <button type="button" class="btn" data-toggle="modal" data-target="#customer_sign_up">Borrower</button>
	        <button type="button"  class="btn"  data-toggle="modal" data-target="#broker_sign_up">Finance Broker</button>
	      </div>
	    </form>
	</div>
</div>
<!-- =====modal-for====sign-up==btn======== -->
<div id="broker_sign_up" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <form class="modal-content form-horizontal" name="myName" method="post" onsubmit="return validateForm()">
	  	<div class="modal-header">
		    <button type="button" class="close" data-dismiss="modal">&times;</button>
		    <h4 class="modal-title text-center"><b>Ready to sign up?</b></h4>
		    <h5 class=" text-center">Please fill in the form to sign up</h5>
	  	</div>	
  		<div class="modal-body">
  			<div class="form-group">
				<label class="control-label col-xs-12" for="last_name">Last Name<span class="red_txt">*</span></label>
				<div class="col-xs-12">
					<input type="text" class="form-control" id="last_name" placeholder="Please enter your last name" name="last_name">
					<span class="form-control-feedback"></span>
					<div class="last_name error-required red_txt"></div>
				</div>
			</div>
		    <div class="form-group">
				<label class="control-label col-xs-12" for="first_name">First Name<span class="red_txt">*</span></label>
				<div class="col-xs-12">
					<input type="text" class="form-control" id="first_name" placeholder="Please enter your first name" name="first_name">
					<span class="form-control-feedback "></span>
					<div class="first_name error-required red_txt"></div>
				</div>
		    </div>
		    <div class="form-group">
				<label class="control-label col-xs-12" for="state">State<span class="red_txt">*</span></label>
				<div class="col-xs-12">
					<select class="form-control" id="state" name="state">
						<option  value="-1">Please select your state</option>
						<?php 
							if(isset($state) && !empty($state)){
								foreach($state as $row){
									echo '<option value="'.$row->id.'">'.$row->name.'</option>';
								}
							}
						?>
					</select>
					<span class="form-control-feedback state"></span>
					<div class="state error-required red_txt"></div>
				</div>
		    </div>
		    <div class="form-group">
				<label class="control-label col-xs-12" for="business_name">Business Name<span class="red_txt">*</span></label>
				<div class="col-xs-12">
					<input type="text" class="form-control" id="business_name" placeholder="Please enter your business name" name="business_name">
					<span class="form-control-feedback "></span>
					<div class="business_name error-required red_txt"></div>
				</div>
		    </div>
		    <div class="form-group">
				<label class="control-label col-xs-12" for="credit_license">Credit Licence Number<span class="red_txt">*</span></label>
				<div class="col-xs-12">
					<input type="text" class="form-control" id="credit_license" placeholder="Please enter your credit licence number" name="credit_license">
					<span class="form-control-feedback "></span>
					<div class="credit_license error-required red_txt"></div>
				</div>
		    </div>

		    <div class="form-group">
				<label class="control-label col-xs-12" for="business_specification">Business Specialisations<span class="red_txt">*</span></label>
				<div class="col-xs-12">
					<select multiple name="business_specification[]" class="form-control mySelect" id="selectEvents">
					  <option value="1">Trade Fair</option>
					  <option value="2">Party</option>
					  <option value="3">Foo</option>
					  <option value="4">Bar</option>
					</select>
					<span class="form-control-feedback "></span>
					<div class="business_specification error-required red_txt"></div>
				</div>
		    </div>
	        <div class="form-group">
				<label class="control-label col-xs-12" for="email_address"> Email address<span class="red_txt">*</span></label>
				<div class="col-xs-12">
					<input type="text" class="form-control" id="email_address" placeholder="Please enter your email address" name="email_address">
					<span class="form-control-feedback "></span>
					<div class="email_address error-required red_txt"></div>
				</div>
		    </div>
		    <div class="form-group">
				<label class="control-label col-xs-12" for="retype_email_address"> Retype email address<span class="red_txt">*</span></label>
				<div class="col-xs-12">
					<input type="text" class="form-control" id="retype_email_address" placeholder="Please retypr your email address" name="retype_email_address">
					<span class="form-control-feedback "></span>
					<div class="retype_email_address error-required red_txt"></div>
				</div>
		    </div>
		    <div class="form-group">
				<label class="control-label col-xs-12" for="password">Password<span class="red_txt">*</span></label>
				<div class="col-xs-12">
					<input type="password" class="form-control" id="password" placeholder="Password must be minimum 5 of characters" name="password">
                    <div class="password error-required red_txt"></div>
				</div>
		    </div>
		    <div class="form-group">
				<label class="control-label col-xs-12" for="rpassword">Retype Password<span class="red_txt">*</span></label>
				<div class="col-xs-12">
					<input type="password" class="form-control" id="rpassword" placeholder="Please retype your password" name="rpassword">

					<span class="form-control-feedback rpassword" id="msg"></span>
					<div class="password error-required red_txt"></div>
					<span class="form-control-feedback"></span>

				</div>
		    </div>
		    <div class="form-group">
				<label class="control-label col-xs-12">Type the code below<span class="red_txt">*</span></label>
				<input type="hidden" name="word" value="<?=$captcha['word']?>" id="word">
				
				<div class="col-xs-12">
					<div id="recaptcha2"></div>
		    	
				</div>
		    </div>
		    <div class="form-group">
		    	<div class="captcha_4_broker_sign_up">

		    	</div>
		    </div>
		    <div class="checkbox  text-center">
		    	<input type="checkbox" name="remember" class="checkbox checkbox-primary" id="terms">
	      		<label for="terms"> By signing up, I agree to the <a href="#">Terms & Conditions</a> and <a href="#">Privacy Policy</a></label>
	      		<span class="form-control-feedback"></span>
		    	<div class="terms error-required red_txt"></div>
				</div>
		    <div class="form-group text-center">
		    	<button class="broker_sign_up_btn btn" id="submit_broker" type="submit">Submit</button>
		    </div>
  		</div>
    </form>
  </div>
</div>
<div id="customer_sign_up" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <form class="modal-content form-horizontal" name="customerSignup" method="post" action="<?=base_url('Customer/add')?>" onsubmit="return customerValidate()">
	  	<div class="modal-header">
		    <button type="button" class="close" data-dismiss="modal">&times;</button>
		    <h4 class="modal-title text-center"><b>Ready to sign up?</b></h4>
		    <h5 class=" text-center">Please fill in the form to sign up</h5>
	  	</div>	
  		<div class="modal-body">
  			<div class="form-group">
				<label class="control-label col-xs-12" for="clast_name">Last Name<span class="red_txt">*</span></label>
				<div class="col-xs-12">
					<input type="text" class="form-control" id="clast_name" placeholder="Please enter your last name" name="clast_name">
					<span class="form-control-feedback"></span>
					<div class="clast_name error-required red_txt"></div>
				</div>
			</div>
		    <div class="form-group">
				<label class="control-label col-xs-12" for="cfirst_name">First Name<span class="red_txt">*</span></label>
				<div class="col-xs-12">
					<input type="text" class="form-control" id="cfirst_name" placeholder="Please enter your first name" name="cfirst_name">
					<span class="form-control-feedback "></span>
					<div class="cfirst_name error-required red_txt"></div>
				</div>
		    </div>
		    <div class="form-group">
				<label class="control-label col-xs-12" for="cstate">State<span class="red_txt">*</span></label>
				<div class="col-xs-12">
					<select class="form-control" id="cstate" name="cstate">
						<option  value="">Please select your state</option>
						<?php 
							if(isset($state) && !empty($state)){
								foreach($state as $row){
									echo '<option value="'.$row->id.'">'.$row->name.'</option>';
								}
							}
						?>
					</select>
					<span class="form-control-feedback state"></span>
					<div class="cstate error-required red_txt"></div>
				</div>
		    </div>
		    <div class="form-group">
				<label class="control-label col-xs-12" for="csuburb">Suburb<span class="red_txt">*</span></label>
				<div class="col-xs-12">
					<input type="text" class="form-control" id="csuburb" placeholder="Please enter your suburb" name="csuburb">
					<span class="form-control-feedback "></span>
					<div class="csuburb error-required red_txt"></div>
				</div>
		    </div>
		    <div class="form-group">
				<label class="control-label col-xs-12" for="chouse_hold_income">House hold income<span class="red_txt">*</span></label>
				<div class="col-xs-12">
					<input type="text" class="form-control" id="chouse_hold_income" placeholder="Please enter your House hold income" name="chouse_hold_income">
					<span class="form-control-feedback "></span>
					<div class="chouse_hold_income error-required red_txt"></div>
				</div>
		    </div>
		    <div class="form-group">
				<label class="control-label col-xs-12" for="cage">Age<span class="red_txt">*</span></label>
				<div class="col-xs-12">
					<select class="form-control" name="cage" id="cage">
						<option value="">Please select your age</option>
						<?php 
							for($i=18; $i<=65; $i++){
								echo '<option value="'.$i.'">'.$i.'</option>';
							}
						?>
					</select>
					<span class="form-control-feedback "></span>
					<div class="cage error-required red_txt"></div>
				</div>
		    </div>
	        <div class="form-group">
				<label class="control-label col-xs-12" for="cemail_address"> Email address<span class="red_txt">*</span></label>
				<div class="col-xs-12">
					<input type="text" class="form-control" id="cemail_address" placeholder="Please enter your email address" name="cemail_address">
					<span class="form-control-feedback "></span>
					<div class="cemail_address error-required red_txt"></div>
				</div>
		    </div>
		    
		    <div class="form-group">
				<label class="control-label col-xs-12" for="cpassword">Password<span class="red_txt">*</span></label>
				<div class="col-xs-12">
					<input type="password" class="form-control" id="cpassword" placeholder="Password must be minimum 5 of characters" name="cpassword">
					<div class="cpassword error-required red_txt"></div>
				</div>
		    </div>
		    <div class="form-group">
				<label class="control-label col-xs-12" for="crpassword">Retype Password<span class="red_txt">*</span></label>
				<div class="col-xs-12">
					<input type="password" class="form-control" id="crpassword" placeholder="Please retype your password" name="crpassword">
					<span class="form-control-feedback password" id="msg1"></span>
					<span class="form-control-feedback"></span>
				</div>
		    </div>
		    <div class="form-group">
				<label class="control-label col-xs-12">Type the code below<span class="red_txt">*</span></label>
				<input type="hidden" name="word" value="<?=$captcha['word']?>" id="word">
				
				<div class="col-xs-12">
					<div id="recaptcha1"></div>
		    	</div>
		    </div>
		    <div class="form-group">
		    	<div class="captcha_4_broker_sign_up">
		    	</div>
		    </div>

		    <div class="checkbox  text-center">
		    	<input type="checkbox" class="checkbox checkbox-primary" name="remember" id="cterms">
	      		<label for="cterms"> By signing up, I agree to the <a href="#">Terms & Conditions</a> and <a href="#">Privacy Policy</a></label>
	      		<span class="form-control-feedback"></span>
		    	<div class="cterms ercror-required red_txt"></div>
				</div>
		    <div class="form-group text-center">
		    	<button class="broker_sign_up_btn btn" id="submit_broker" type="submit">Submit</button>
		    </div>
  		</div>
    </form>
  </div>
</div>


<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.css" rel="stylesheet"/>
<!-- for finance broker -->
<script type="text/javascript">
	$(document).ready(function() {
		var data = []; // Programatically-generated options array with > 5 options
		var placeholder = "select";
		$(".mySelect").select2({
		    data: data,
		    placeholder: placeholder,
		    allowClear: false,
		    minimumResultsForSearch: 5});	
	});
</script>
<script type="text/javascript">
	$(document).ready(function() {
		/*email_id match again*/
		$('#retype_email_address').change(function (){
			var retype_email_address = $('#retype_email_address').val();
			var email_address = $('#email_address').val();
			if(email_address == retype_email_address){
				$('#retype_email_address').css('border', '2px solid green');
				$('#email_address').css('border', '2px solid green');
			}else{
				$('#retype_email_address').css('border', '2px solid red');
				$('#email_address').css('border', '2px solid red');
			}

		});
		/*email_id match again*/


		$('#rpassword').change(function (){
			var password = $('#password').val();
			var rpassword = $('#rpassword').val();
			if(password == rpassword){
				$('#rpassword').css("border", "2px solid green");
				$('#password').css("border", "2px solid green");
			}else{
				$('#rpassword').css("border", "2px solid red");
				$('#password').css("border", "2px solid red");
				$('.rpassword').empty();
				$('.rpassword').append('Your password didnot match');
				$('.password').empty();
				$('.password').append('your password did not match');
				return false;
			}
		});
		$('#password').keyup(function() {
			var password = $('#password').val();
			var msg = '';
			if(password.length >= '6' && password.length <= '100'){
				msg = '<p style="color:green;">Strong</p>';
			}
			if(password.length >= '3' && password.length <= '6'){
				msg = '<p style="color:yellow;">Medium</p>';
			}
			if(password.length >= '' && password.length <= '3'){
				msg = '<p style="color:red;">Poor</p>';
			}
			$('#msg').empty();
			$('#msg').append(msg);
		});

		$('.form-control').change(function (){
			$('.error-required').empty();
		});

		$('#email_address').change(function (){
			var email = $('#email_address').val();
			var atpos = email.indexOf("@");
		    var dotpos = email.lastIndexOf(".");
		    if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length) {
		        $('#email_address').css('border', '2px solid red');
				$('.email_address').empty();
				$('.email_address').append('Not a valid e-mail address'); 
				return false;
		    }
			$.ajax({
				url: "<?=base_url('Home/validateEmail')?>",
				type: 'POST',
				dataType: 'json',
				data: {email: email},
			})
			.done(function(result) {
				console.log("success");
				console.log(result);
				if(result.message == "fine"){
					$('#email_address').css('border', '2px solid green');
					document.getElementById("submit_broker").removeAttribute("disabled");
					return true;
				}else{
					$('#email_address').css('border', '2px solid red');
					$('.email_address').append(result.message);
					document.getElementById("submit_broker").disabled = true; 
					return false;
				}
			})
			.fail(function(e) {
				console.log("error");
				console.log(e);
			})
			
		});

		$('#business_name').change(function (){
			var business_name = $('#business_name').val();
			$.ajax({
				url: "<?=base_url('Home/validateBusinessName')?>",
				type: 'POST',
				dataType: 'json',
				data: {bname: business_name},
			})
			.done(function(result) {
				console.log("success");
				console.log(result);
				if(result.message == "fine"){
					$('#business_name').css('border', '2px solid green');
					document.getElementById("submit_broker").removeAttribute("disabled");
					return true;
				}else{
					$('#business_name').css('border', '2px solid red');
					$('.business_name').append(result.message);
					document.getElementById("submit_broker").disabled = true; 
					return false;
				}
			})
			.fail(function(e) {
				console.log("error");
				console.log(e);
			})
			
		});
		$('#code').change(function (){
			var word = document.forms["myName"]["word"].value;
			var code = document.forms["myName"]["code"].value;

			if(word == code){
				$('#code').css('border', '2px solid green');
				$('.code').empty();
				return true;
			}else{
				$('#code').css('border', '2px solid red');
				$('.code').empty();
				$('.code').append('Captcha could not matched');
				return false;
			}
		});
	});
	/*function to validate broker form*/
	function validateForm() {
		var last_name = document.forms["myName"]["last_name"].value;
		var first_name = document.forms["myName"]["first_name"].value;
		var business_name = document.forms["myName"]["business_name"].value;
		var credit_license = document.forms["myName"]["credit_license"].value;
		var state = document.forms["myName"]["state"].value;

		//var business_specification = document.forms["myName"]["business_specification"].value;
		//alert(business_specification); return false;
		var email_address = document.forms["myName"]["email_address"].value;
		var password = document.forms["myName"]["password"].value;
		var terms = document.forms["myName"]["terms"].checked;

		var atpos = email_address.indexOf("@");
	    var dotpos = email_address.lastIndexOf(".");

	    

		if(last_name == "" || first_name == "" || terms == false || business_name == "" || credit_license == "" || business_specification == "" || email_address == "" || password == ""){
			
			if(last_name == ""){
				$('.last_name').empty();
				$('.last_name').append('Last Name is required');
			}
			if(first_name == ""){
				$('.first_name').empty();
				$('.first_name').append('First Name is required');
			}
			if(business_name == ""){
				$('.business_name').empty();
				$('.business_name').append('Business Name is required');
			}
			if(credit_license == ""){
				$('.credit_license').empty();
				$('.credit_license').append('Credit License is required');
			}
			if(state == ""){
				$('.state').empty();
				$('.state').append('State is required');
			}
			if(email_address == ""){
				$('.email_address').empty();
				$('.email_address').append('Email Address is required');
			}
			if(password == ""){
				$('.password').empty();
				$('.password').append('Password is required');
			}
			if(terms == ""){
				$('.terms').empty();
				$('.terms').append('You need to agree with our terms and conditions');
			}

			if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email_address.length) {
		        $('#email_address').css('border', '2px solid red');
				$('.email_address').empty();
				$('.email_address').append('Not a valid e-mail address');
				return false; 
	    	}
			return false;
		}
	}
	/*function to validate broker form*/
</script>
<!-- for finance broker -->
<!-- for customerSignup -->
<script type="text/javascript">
	$(document).ready(function() {
		$('#crpassword').change(function (){
			var cpassword = $('#cpassword').val();
			var crpassword = $('#crpassword').val();
			if(cpassword == crpassword){
				$('#crpassword').css("border", "2px solid green");
				$('#cpassword').css("border", "2px solid green");
			}else{
				$('#crpassword').css("border", "2px solid red");
				$('#cpassword').css("border", "2px solid red");
				$('.cpassword').empty();
				$('.cpassword').append('Your password didnot match');
				return false;
			}
		});
		$('#cpassword').keyup(function() {
			var cpassword = $('#cpassword').val();
			var msg1 = '';
			if(cpassword.length >= '6' && cpassword.length <= '100'){
				msg1 = '<p style="color:green;">Strong</p>';
			}
			if(cpassword.length >= '3' && cpassword.length <= '6'){
				msg1 = '<p style="color:yellow;">Medium</p>';
			}
			if(cpassword.length >= '' && cpassword.length <= '3'){
				msg1 = '<p style="color:red;">Poor</p>';
			}
			$('#msg1').empty();
			$('#msg1').append(msg1);
		});
		$('#cemail_address').change(function (){
			var email = $('#cemail_address').val();
			var atpos = email.indexOf("@");
		    var dotpos = email.lastIndexOf(".");
		    if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length) {
		        $('#cemail_address').css('border', '2px solid red');
				$('.cemail_address').empty();
				$('.cemail_address').append('Not a valid e-mail address'); 
				return false;
		    }
			$.ajax({
				url: "<?=base_url('Home/validateEmail')?>",
				type: 'POST',
				dataType: 'json',
				data: {email: email},
			})
			.done(function(result) {
				console.log("success");
				console.log(result);
				if(result.message == "fine"){
					
					$('#cemail_address').css('border', '2px solid green');
					document.getElementById("submit_broker").removeAttribute("disabled");
					return true;
				}else{
					$('#cemail_address').css('border', '2px solid red');
					$('.cemail_address').append(result.message);
					document.getElementById("submit_broker").disabled = true; 
					return false;
				}
			})
			.fail(function(e) {

				console.log("error");
				console.log(e);
			})
			
		});
	});
	
	function customerValidate(){
		var clast_name = document.forms["customerSignup"]["clast_name"].value;
		var cfirst_name = document.forms["customerSignup"]["cfirst_name"].value;
		var cstate = document.forms["customerSignup"]["cstate"].value;
		var csuburb = document.forms["customerSignup"]["csuburb"].value;
		var chouse_hold_income = document.forms["customerSignup"]["chouse_hold_income"].value;
		var cage = document.forms["customerSignup"]["cage"].value;
		var cemail_address = document.forms["customerSignup"]["cemail_address"].value;
		var cpassword = document.forms["customerSignup"]["cpassword"].value;
		var crpassword = document.forms["customerSignup"]["crpassword"].value;
		var cterms = document.forms["customerSignup"]["cterms"].checked;
		
		if(clast_name == '' || cfirst_name == '' || cterms == false || cstate == '' || csuburb == '' || chouse_hold_income == '' || cage == '' || cemail_address == '' || cpassword == ''){
			var atpos = cemail_address.indexOf("@");
		    var dotpos = cemail_address.lastIndexOf(".");
		    
			if(clast_name == ''){
				$('.clast_name').empty();
				$('.clast_name').append('Last name is required');
			}
			if(cfirst_name == ''){
				$('.cfirst_name').empty();
				$('.cfirst_name').append('First name is required');
			}
			if(cstate == ''){
				$('.cstate').empty();
				$('.cstate').append('State is required');
			}
			if(csuburb == ''){
				$('.csuburb').empty();
				$('.csuburb').append('Suburb is required');
			}
			if(chouse_hold_income == ''){
				$('.chouse_hold_income').empty();
				$('.chouse_hold_income').append('House hold income is required');
			}
			if(cage == ''){
				$('.cage').empty();
				$('.cage').append('Age is required');
			}
			if(cpassword == ''){
				$('.cpassword').empty();
				$('.cpassword').append('Password is required');
			}
			if(cterms == ''){
				$('.cterms').empty();
				$('.cterms').append('You need to agree with our terms and conditions');
			}
			if(cemail_address == ''){
				$('.cemail_address').empty();
				$('.cemail_address').append('Email-Address is required');
			}
			if (atpos<1 || dotpos<atpos+2 || dotpos+2>=cemail_address.length) {
		        $('#cemail_address').css('border', '2px solid red');
				$('.cemail_address').empty();
				$('.cemail_address').append('Not a valid e-mail address'); 
				return false;
		    }
			return false;
		}
		
	}
</script>
<!-- for customerSignup -->

<style type="text/css" media="screen">
#customer_sign_up .form-horizontal .control-label {
    text-align: left;
}	
#customer_sign_up .modal-dialog {
    max-width: 450px;
}
.select2{
	width:100% !important;
}
.like{
	cursor: pointer;
}

</style>
<script> var base_url = '<?php echo base_url(); ?>';</script>
<script type="text/javascript" src="<?php echo base_url()?>assets/front/js/customer_profile.js" ></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/front/js/deactivate_acc.js" ></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/front/js/change_password.js" ></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/front/js/forgot_password.js" ></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/front/js/broker_dashboard.js" ></script>
<script src="https://www.google.com/recaptcha/api.js?onload=myCallBack&render=explicit" async defer></script>
    <script>
      var recaptcha1;
      var recaptcha2;
      var myCallBack = function() {
        //Render the recaptcha1 on the element with ID "recaptcha1"
        recaptcha1 = grecaptcha.render('recaptcha1', {
          'sitekey' : '6LcdkyUUAAAAAOQcGnMzO6OzYQL3Ge2wiK6MvPsN', //Replace this with your Site key
          'theme' : 'light'
        });
         
        //Render the recaptcha2 on the element with ID "recaptcha2"
        recaptcha2 = grecaptcha.render('recaptcha2', {
          'sitekey' : '6LcdkyUUAAAAAOQcGnMzO6OzYQL3Ge2wiK6MvPsN', //Replace this with your Site key
          'theme' : 'light'
        });
      };
    </script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/bootstrap-select.min.js"></script>
	
	<script type="text/javascript">
		$(document).ready(function(){
			$('.selectpicker').selectpicker();
		});
	</script>
	<!-- start here -->

<script type="text/javascript">

$(document).ready(function () {
    $(".forget_btn").click(function () {
        $(".forget_btn").attr("disabled", true);
        return true;
    });

    

});


</script>

<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/594c22ba50fd5105d0c8248e/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
</body>
</html>