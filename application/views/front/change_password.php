<?php $uri = $this->uri->segment(3); ?>
<section class="change_pass_page">
  	<div class="container">
	    <div class="row">
	      	<div class='col-sm-12'>
		        <br>
		        	<h3 class="page_brdr_titl" >Change Password </h3>
		        <br>
	      	</div>
	    </div>
  	</div>

    <div class="container">
    	<div class="row">
      		<div class='col-sm-12'>
      			<div class="change_pass_form">
	      			<form class="form-horizontal" action="<?= base_url('Home/change_password_action/').$uri?>" method="POST">
	      				<div class="form-group">
	      					<label for="ch_ps_old" class="control-label col-sm-4">Old Password<span class="red_txt">*</span></label>
	      					<div class="col-sm-8">
	      						<input type="password" name="oldpassword" class="form-control ch_ps_old" id="<?= $uri?>" placeholder="Please enter your old password">
	      						<span class="col-sm-12 red_txt hidden" id="ch_ps_old_span"><small>Old Password is Wrong! Plz Re-enter</small></span>
	      					</div>
	      				</div>
	      				<div class="form-group">
	      					<label for="ch_ps_new" class="control-label col-sm-4">New Password<span class="red_txt">*</span></label>
	      					<div class="col-sm-8">
	      						<input type="password" name="newpassword" class="form-control" id="ch_ps_new" placeholder="Password must be minimum of 6 characters">
	      						<span class="col-sm-12 red_txt hidden" id="ch_ps_new_span"><small id="ch_ps_new_small">Password Wrong! Plz Re-enter</small></span>
	      					</div>
	      				</div>
	      				<div class="form-group">
	      					<label for="ch_ps_retyp" class="control-label col-sm-4">Retype Password<span class="red_txt">*</span></label>
	      					<div class="col-sm-8">
	      						<input type="password" name="cnfpassword" class="form-control" id="ch_ps_retyp" placeholder="Please retype your password">
	      						<span class="col-sm-12 red_txt hidden" id="ch_ps_retyp_span"><small id="ch_ps_retyp_small">your retype password did not match</small></span>
	      					</div>
	      				</div>
	      				<div class="form-group">
	      					<div class="col-sm-offset-4 col-sm-8">
	      						<input type="submit" id="form_submit" class="btn" value="Save Changes">
	      					</div>
	      				</div>
	      			</form>
	      		</div>
      		</div>
	    </div>
  	</div>
</section>