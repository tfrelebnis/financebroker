<?php $usr_id = $this->uri->segment(3);?>
<?php //print_r($pack); exit(); ?>
<section class="change_subscription_page">
	<div class="container">
		<div class="row">
			<div class='col-sm-12'>
				<br>
				<h3 class="page_brdr_titl" >Change Subscription</h3>
				<br>
			</div>
		</div>
	</div>
	<div class="container ">
		<div class="row change_subscription_row">
			<div class='col-sm-3'>
				<div class="not_visible_sontnt left_ch_subscription text-center">
					<br>
					<?php if(isset($pack) && !empty($pack)){ 
						$spt = $pack[0]->subscribed_packege_type;
						$pid = $pack[0]->package_id;
						?>
						<p class="fa fa-check "></p>
						<p >Your current subscription.</p>
						<h3><?= $pack[0]->package_name; ?></h3>
						<div class="ch_month_box">
							<h4><?= $pack[0]->subscribed_packege_type == '12' ? 'MONTHLY' : 'ANNUAL' ?></h4>
							<h3>$<?= $spt == '12' ? $pack[0]->monthly_fee : $pack[0]->annualy_fee ?>/month</h3>
							<p><small>Inclusive of GST</small></p>
						</div>
						<?php }else{?>
						<p class="fa fa-close red_txt"></p>
						<p >You Didn't Subscribe Any Package</p>
						<?php $spt='';$pid = ''; } ?>
						<br>
					</div>
				</div>
				<?php if(isset($all) && !empty($all)){ ?>
				<div class='col-sm-9'>
					<div class="right_ch_subscription text-center">
						<br>
						<p>You may upgrade your subscription to:</p>
						<div class="feature_n_pkg_box">
							<div class="pkg_cntnt">
								<h3>NORMAL PACKAGE</h3>
								<P>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</P>
							</div>
							<div class="pkg_cntnt text-center">
								<div class="pkg_month">
									<h5>MONTHLY</h5>
									<h3>$<?= $all[1]->monthly_fee ?>/month</h3>
									<p><small>inclusive of GST</small></p>
									<?php if($spt == '12' && $pid == '1'){ ?>
									<h5 class="green_txt" id="n_m_text">CURRENT SUBSCRIPTION</h5>
									<?php }else if($spt == '1' && $pid == '1' || $spt == '1' && $pid == '2'){ ?>
									<h5 class="red_txt" id="n_m_text">Subscription must be cancelled first before changing to this package</h5>
									<?php }else{ ?>
									<a href="<?= base_url('Home/subscribe_package/').'12'.'/'.$usr_id.'/'.$all[1]->id.'/'.$all[0]->monthly_fee ?>"  class="btn" id="n_a_btn">SUBSCRIBE</a>
									<?php } ?>
								</div>
								<div class="pkg_annual cancl_change">
									<h5>ANNUAL</h5>
									<h3>$<?= $all[1]->annualy_fee ?>/year</h3>
									<p><small>inclusive of GST</small></p>
									<?php if($spt == '1' && $pid == '1'){ ?>
									<h5 class="green_txt" id="n_a_text">CURRENT SUBSCRIPTION</h5>
									<?php }else if($spt == '1' && $pid == '2'){ ?>
									<h5 class="red_txt" id="n_m_text">Subscription must be cancelled first before changing to this package</h5>
									<?php }else{ ?>
									<a href="<?= base_url('Home/subscribe_package/').'1'.'/'.$usr_id.'/'.$all[1]->id.'/'.$all[1]->annualy_fee ?>"  class="btn" id="n_a_btn">SUBSCRIBE</a>
									<?php } ?>
								</div>
							</div>
						</div>
						<div class="feature_n_pkg_box">
							<div class="pkg_cntnt">
								<h3>FEATURED PACKAGE</h3>
								<P>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</P>
							</div>
							<div class="pkg_cntnt text-center cancl_change">
								<div class="pkg_month">
									<h5>MONTHLY</h5>
									<h3>$<?= $all[0]->monthly_fee ?>/month</h3>
									<p><small>inclusive of GST</small></p>
									<?php if($spt == '12' && $pid == '2'){ ?>
									<h5 class="green_txt" id="f_m_txt">CURRENT SUBSCRIPTION</h5>
									<?php }else if($spt == '1' && $pid == '2' || $spt == '1' && $pid == '1'){ ?>
									<h5 class="red_txt" id="n_m_text">Subscription must be cancelled first before changing to this package</h5>
									<?php }else{ ?>
									<a href="<?= base_url('Home/subscribe_package/').'12'.'/'.$usr_id.'/'.$all[0]->id.'/'.$all[0]->monthly_fee ?>" class="btn" id="f_m_btn">SUBSCRIBE</a>
									<?php } ?>
								</div>
								<div class="pkg_annual cancl_change">
									<h5>ANNUAL</h5>
									<h3>$<?= $all[0]->annualy_fee ?>/year</h3>
									<p><small>inclusive of GST</small></p>
									<?php if($spt == '1' && $pid == '2'){ ?>
									<h5 class="green_txt" id="f_a_txt">CURRENT SUBSCRIPTION</h5>
									<?php }else{ ?>
									<a href="<?= base_url('Home/subscribe_package/').'1'.'/'.$usr_id.'/'.$all[0]->id.'/'.$all[0]->annualy_fee ?>" class="btn" id="f_a_btn">SUBSCRIBE</a>
									<?php } ?>
								</div>
							</div>
							<br>
						</div>
					</div>
					<?php }else{ ?>
					<div class='col-sm-9'>
						<div class="right_ch_subscription text-center">
							<br>There Is Not Any Package </br>
						</div>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</section>