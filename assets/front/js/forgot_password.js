$('#forgot_submit').on('click', function() {
	var error = 0;
	var val = $('#log-email').val();

	if(val == ''){
		$('#log-email').focus();
		$('#forgot_txt_span').removeClass('hidden');
		return false;
	}else{
		function validateEmail(email) {
			var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
			return emailReg.test( email );
		}
		if(!validateEmail(val)){
			$('#forgot_txt_span').removeClass('hidden');
			$('#forgot_txt_small').html('Please enter valid email !');
			return false;
		}else{
			$.ajax({
				type: 'POST',
				async: false,
				url: base_url+'Home/ajax_validate_email/'+val,                   
				success: function(result){
					// alert(result); return false;
					if(result != 1){
						$('#forgot_txt_span').removeClass('hidden');
						$('#forgot_txt_small').html('The email address you have entered is not registered !');
						return false;
					}else{
						$('#forgot_txt_span').removeClass('hidden');
						$('#forgot_txt_span').removeClass('red_txt');
						$('#forgot_txt_span').addClass('green_txt');
						$('#forgot_txt_small').html('Your Password Has Sent On Your Registered email!');
						return true;
					}
				}, 
				error: function(e){
					console.log(e);
				}
			});
		}
	}
});