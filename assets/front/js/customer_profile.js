$(document).ready(function(e){
	 $('.fav').on('change', function() {
		var fav_val =  $(this).val();
		var obj = $.parseJSON(fav_val);
		 $.ajax({
                    type: 'POST',
                    async: false,
                    url: base_url+'Home/ajax_marks_as_favourite_change/'+obj.fav_id+'/'+obj.cid+'/'+obj.bid,                   
                    success: function(result){
                        if(result == 1){
                        	window.location.href = base_url+'Home/customer_profile/'+obj.cid;
                        }else{
                        	window.location.href = base_url+'Home/customer_profile/'+obj.cid;
                        }
                    }, 
                    error: function(e){
                        console.log(e);
                    }
            });
	 });
});