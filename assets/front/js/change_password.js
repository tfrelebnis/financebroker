$(document).ready(function(e){
  $('.ch_ps_old').on('blur', function() {
      var val =  $(this).val();
      var user_id = $(this).attr('id');
      $.ajax({
        type: 'POST',
        async: false,
        url: base_url+'Home/old_password_validate/'+val+'/'+user_id,                   
        success: function(result){
            if(result != 1){
                $('#ch_ps_old_span').removeClass('hidden');
                $('#form_submit').prop("disabled",true);
            }else{
                $('#ch_ps_old_span').addClass('hidden');
                $('#form_submit').prop("disabled",false);
            }
        }, 
        error: function(e){
            console.log(e);
        }
    });
  });

  $('#ch_ps_new').on('blur', function() {
    if($('#ch_ps_new').val().length>=6){
        var reg = /^[a-zA-Z0-9]{8,}$/;
        var result = reg.test($('#ch_ps_new').val());
        if(result == false){
            $('#ch_ps_new_span').removeClass('hidden');
            $('#ch_ps_new_span').removeClass('red_txt');
            $('#ch_ps_new_span').removeClass('blue_txt');
            $('#ch_ps_new_span').removeClass('yelo_txt');
            $('#ch_ps_new_span').addClass('green_txt');
            $('#ch_ps_new_small').html('Strong');
        }else{
            $('#ch_ps_new_span').removeClass('hidden');
            $('#ch_ps_new_span').removeClass('red_txt');
            $('#ch_ps_new_span').removeClass('green_txt');
            $('#ch_ps_new_span').removeClass('blue_txt');
            $('#ch_ps_new_span').addClass('yelo_txt');
            $('#ch_ps_new_small').html('Medium');
        }
    }else{
        $('#ch_ps_new_span').removeClass('hidden');
        $('#ch_ps_new_span').removeClass('red_txt');
        $('#ch_ps_new_span').removeClass('green_txt');
        $('#ch_ps_new_span').removeClass('yelo_txt');
        $('#ch_ps_new_span').addClass('blue_txt');
        $('#ch_ps_new_small').html('Weak');
    }
});
  $('#form_submit').on('click', function() {
    var error = 0;
    if($('.ch_ps_old').val() == ''){
        $('.ch_ps_old').focus();
        error = 1;
    }else if($('#ch_ps_new').val() == ''){
        $('#ch_ps_new').focus();
        error = 1;
    }else if($('#ch_ps_retyp').val() == ''){
        $('#ch_ps_retyp').focus();
        error = 1;
    }else {
        if($('#ch_ps_new').val() != $('#ch_ps_retyp').val()){
            $('#ch_ps_retyp').focus();
            $('#ch_ps_retyp_span').removeClass('hidden');
            error = 1;
        }
    }
    if(error){
        return false;
    }else{
        return true;
    }

});
});